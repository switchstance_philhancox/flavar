<?php
use Migrations\AbstractMigration;

class RecipesMigration4 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $recipe_steps_table = $this->table('recipe_steps');
        $recipe_steps_table
            ->changeColumn('timer_time', 'string', ['length' => '8', 'null' => true])
            ->update();
    }

    public function down()
    {
        $recipe_steps_table = $this->table('recipe_steps');
        $recipe_steps_table
            ->changeColumn('timer_time', 'string', ['length' => '5'])
            ->update();

    }
}
