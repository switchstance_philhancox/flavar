<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Recipe $recipe
 */
?>

<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <span class="c-grey fw-normal"><?= __('Recipes') ?></span><br><?= $recipe->recipe_title ?>
            </h2>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-10">
        <div class="row flex-row">
            <div class="col-md-6">
                <div class="widget">
                    <div class="widget-header">
                        <h3><?= __('Ingredients') ?>
                            <div class="pull-right">
                                <a class="btn btn-secondary btn-square btn-sm" href="<?= $this->Url->build(['controller' => 'RecipeIngredients', 'action' => 'edit', $recipe->recipe_id]) ?>">
                                    <?= __('Edit Ingredients') ?>
                                </a>
                            </div>
                        </h3>
                    </div>
                    <div class="widget-body">
                        <?php if ($recipe->recipe_ingredients) : ?>
                        <ul class="no-bullet">
                            <?php foreach ($recipe->recipe_ingredients as $recipe_ingredient) : ?>
                                <li><?= $this->Util->getIngredientLine(
                                        $recipe->default_number_of_servings,
                                        $recipe_ingredient->quantity,
                                        $recipe_ingredient->quantity_per_additional_serving,
                                        $recipe_ingredient->quantity_type->quantity_type_name,
                                        $recipe_ingredient->ingredient_name
                                    ) ?></li>
                            <?php endforeach; ?>
                        </ul>
                        <?php else : ?>
                            <p class="c-red"><?= __('There have been no ingredients configured with this recipe.') ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="widget">
                    <div class="widget-header">
                        <h3><?= __('Utensils') ?>
                            <div class="pull-right">
                                <a class="btn btn-secondary btn-square btn-sm" href="<?= $this->Url->build(['controller' => 'RecipeUtensils', 'action' => 'edit', $recipe->recipe_id]) ?>">
                                    <?= __('Edit Utensils') ?>
                                </a>
                            </div>
                        </h3>
                    </div>
                    <div class="widget-body">
                        <?php if ($recipe->recipe_utensils) :
                            $utensil_list = $this->Util->getRecipeUtensilList($recipe->recipe_utensils) ?>
                        <ul class="no-bullet">
                            <?php foreach ($utensil_list as $item) : ?>
                                <li><?= sprintf(__('%d x %s'), $item['quantity'], $item['name']) ?></li>
                            <?php endforeach; ?>
                        </ul>
                        <?php else : ?>
                            <p class="c-red"><?= __('There have been no utensils configured with this recipe.') ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="widget-header">
                        <h3><?= __('Method') ?>
                            <div class="pull-right">
                                <a class="btn btn-secondary btn-square btn-sm" href="<?= $this->Url->build(['controller' => 'RecipeSteps', 'action' => 'add', $recipe->recipe_id]) ?>">
                                    <?= __('Add Step') ?>
                                </a>
                            </div>
                        </h3>
                    </div>
                    <div class="widget-body">
                        <?php if ($recipe->recipe_steps) : ?>
                            <table class="table table-condensed table-bordered sortable-table">
                                <thead>
                                <tr>
                                    <th><?= __('Re-Order') ?></th>
                                    <th><?= __('Description') ?></th>
                                    <th><?= __('Time') ?></th>
                                    <th><?= __('Utensil') ?></th>
                                    <th><?= __('MEP?') ?></th>
                                    <th><?= __('Blocker?') ?></th>
                                    <th><?= __('Video?') ?></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($recipe->recipe_steps as $key => $recipe_step) : ?>
                                <tr id="recipe-steps-<?= $recipe_step->recipe_step_id ?>" class="entity-row-<?= $key ?>">
                                    <td class="text-center reorder-icon-cell"><span class="fa fa-bars"></span></td>
                                    <td>
                                        <p><?= nl2br($recipe_step->step_description) ?></p>
                                        <?php if ($recipe_step->tip_description) : ?>
                                        <p><small class="c-grey"><?= __('Tip:') ?>  <?= $recipe_step->tip_description ?></small></p>
                                        <?php endif; ?>
                                    </td>
                                    <td><p class="text-center"><?= $recipe_step->has_timer ? ($recipe_step->timer_time ?? '-') : '-' ?></p></td>
                                    <td>
                                        <?php if ($recipe_step->recipe_utensil && $recipe_step->recipe_utensil->utensil->image) : ?>
                                            <img src="<?= $this->Url->build('/', true) . $recipe_step->recipe_utensil->utensil->image->image_url ?>" alt="<?= $recipe_step->recipe_utensil->utensil->image->alt_text ?>" class="img-fluid" style="width: 30px;">
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                        <?= $this->Util->getBooleanIcon($recipe_step->is_mise_en_place) ?>
                                    </td>
                                    <td class="text-center">
                                        <?= $this->Util->getBooleanIcon($recipe_step->must_complete_time) ?>
                                    </td>
                                    <td class="text-center">
                                        <?= $this->Util->getBooleanIcon($recipe_step->video_link) ?>
                                    </td>
                                    <td width="150px">
                                        <a class="btn btn-sm btn-secondary btn-square" href="<?= $this->Url->build(['controller' => 'RecipeSteps', 'action' => 'edit', $recipe_step->recipe_step_id]) ?>">
                                            <?= __('Edit') ?>
                                        </a>
                                        <button class="btn btn-sm btn-danger btn-square delete-entity" data-key="<?= $key ?>" data-id="<?= $recipe_step->recipe_step_id?>" data-delete-url="<?= $this->Url->build(['controller' => 'RecipeSteps', 'action' => 'delete', '_ext' => 'json']); ?>"><?= __('Delete') ?></button>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php else : ?>
                            <p class="c-red"><?= __('There have been no steps configured with this recipe.') ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="widget-header">
                        <h3><?= __('Plating Up') ?>
                            <div class="pull-right">
                                <a class="btn btn-secondary btn-square btn-sm" href="<?= $this->Url->build(['controller' => 'RecipePlatingDescriptions', 'action' => 'edit', $recipe->recipe_id]) ?>">
                                    <?= __('Edit') ?>
                                </a>
                            </div>
                        </h3>
                    </div>
                    <div class="widget-body">
                        <?php if ($recipe->recipe_plating_description) : ?>
                            <?php if ($recipe->recipe_plating_description->image) : ?>
                                <img src="<?= $this->Url->build('/', true) . $recipe->recipe_plating_description->image->image_url ?>" alt="<?= $recipe->recipe_plating_description->image->alt_text ?>" class="img-fluid pb-4">
                            <?php endif; ?>
                            <p><?= nl2br($recipe->recipe_plating_description->plating_description) ?></p>
                        <?php else : ?>
                            <p class="c-red"><?= __('The plating description has not been configured for this recipe.') ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="widget-header">
                        <h3><?= __('Gallery') ?>
                            <div class="pull-right">
                                <a class="btn btn-secondary btn-square btn-sm" href="<?= $this->Url->build(['controller' => 'RecipeImages', 'action' => 'add', $recipe->recipe_id]) ?>">
                                    <?= __('Add Image') ?>
                                </a>
                            </div>
                        </h3>
                    </div>
                    <div class="widget-body">
                        <?php if ($recipe->recipe_images) : ?>
                            <div class="row">
                            <?php foreach ($recipe->recipe_images as $key => $recipe_image) : ?>
                                <div class="col-md-3 entity-row-delete-gallery-image-<?= $key ?>">
                                    <img src="<?= $this->Url->build('/', true) . $recipe_image->image->image_url ?>" alt="<?= $recipe_image->image->alt_text ?>" class="img-fluid">
                                    <div class="text-center">
                                        <button class="btn btn-sm btn-square btn-danger mt-3 delete-entity" data-key="delete-gallery-image-<?= $key ?>" data-id="<?= $recipe_image->recipe_image_id ?>" data-delete-url="<?= $this->Url->build(['controller' => 'RecipeImages', 'action' => 'delete', '_ext' => 'json']); ?>"><?= __('Remove Image') ?></button>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            </div>
                        <?php else : ?>
                            <p class="c-red"><?= __('The gallery for this recipe is empty.') ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2 bg-grey border">
        <div class="text-center pt-4">
        <a class="btn btn-secondary btn-square mb-4" href="<?= $this->Url->build(['controller' => 'Recipes', 'action' => 'edit', $recipe->recipe_id]) ?>">
            <?= __('Edit Recipe Details') ?>
        </a>
        </div>
        <?php if ($recipe->image) : ?>
                <img src="<?= $this->Url->build('/', true) . $recipe->image->image_url ?>" alt="<?= $recipe->image->alt_text ?>" class="img-fluid pb-3">
        <?php endif; ?>
        <p>
            <?= __('Category') ?><br>
            <strong><?= $recipe->category->category_name ?></strong>
        </p>
        <?php if ($recipe->total_time_estimate) : ?>
        <p>
            <?= __('Estimated Time') ?><br>
            <strong><?= $recipe->total_time_estimate ?></strong>
        </p>
        <?php endif; ?>
        <p>
            <?= __('Author') ?><br>
            <strong><?= $recipe->admin->display_name ?></strong>
        </p>
        <p>
            <?= __('Status') ?><br>
            <strong><?= $this->Util->returnStatus($recipe->is_live) ?></strong>
        </p>
        <p>
            <?= __('Published') ?><br>
            <strong><?= $recipe->created->format('H:ia d/m/Y') ?></strong>
        </p>
        <p>
            <?= __('Difficulty') ?><br>
            <strong><?= $this->Util->returnDifficulty($recipe->difficulty_level)?></strong>
        </p>
        <p>
            <?= __('Default number of servings') ?><br>
            <strong><?= $recipe->default_number_of_servings ?></strong>
        </p>
        <p>
            <?= __('Subscriber Access') ?><br>
            <strong><?= $this->Util->returnSubscriberLevel($recipe->subscriber_level) ?></strong>
        </p>
        <?php if (count($recipe->recipe_tags) > 0) : ?>
        <p>
            <?= __('Tags') ?><br>
            <?php foreach ($recipe->recipe_tags as $recipe_tag) : ?>
                <span class="tag"><?= $recipe_tag->tag->tag_name ?></span>
            <?php endforeach; ?>
        </p>
        <?php endif; ?>
        <?php if ($recipe->description) : ?>
        <p>
            <?= __('Description') ?><br>
            <strong><?= $recipe->description ?></strong>
        </p>
        <?php endif; ?>
        <?php if ($recipe->ar_model) : ?>
        <p>
            <?= __('AR Model') ?><br>
            <strong style="word-break: break-all;"><?= $recipe->ar_model->ar_model_url .' ('.$this->Util->formatFileSize($recipe->ar_model->ar_model_file_size).')' ?></strong>
        </p>
        <?php endif; ?>
    </div>
</div>