<?php
/**
 * @var \App\View\AppView $this
 * @var App\Model\Entity\Recipe[]|\Cake\Collection\CollectionInterface $recipes */
?>

<!-- Begin Page Header-->
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title"><?= __('Recipes') ?></h2>
            <div>
                <a class="btn btn-square btn-outline-primary" href="<?= $this->Url->build(['controller' => 'Recipes', 'action' => 'add']) ?>">
                    <?= __('Add New Recipe') ?>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header -->


<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <!--
            <div class="widget-header bordered no-actions d-flex align-items-center">
            </div>
            -->
            <div class="widget-body">
                <?php if (count($recipes) > 0) : ?>
                    <div class="table-responsive">
                        <div id="sorting-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div id="sorting-table_filter" class="dataTables_filter">
                                        <form method="get" action="#">
                                            <label><?= __('Search:') ?><input type="search" class="form-control form-control-sm" placeholder="" name="search_term" aria-controls="sorting-table" value="<?= $search_term ?>"></label>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <table class="table mb-0 dataTable table-hover">
                                        <thead>
                                        <tr>

                                            <?php
                                            if ($this->request->getQuery('sort') == 'recipe_title') :
                                                if ($this->request->getQuery('direction') == 'asc') :
                                                    $sort_class = 'sorting_asc';
                                                elseif ($this->request->getQuery('direction') == 'desc') :
                                                    $sort_class = 'sorting_desc';
                                                else :
                                                    $sort_class = 'both';
                                                endif;
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                            ?>
                                            <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('recipe_title') ?></th>

                                            <?php
                                            if ($this->request->getQuery('sort') == 'is_live') :
                                                if ($this->request->getQuery('direction') == 'asc') :
                                                    $sort_class = 'sorting_asc';
                                                elseif ($this->request->getQuery('direction') == 'desc') :
                                                    $sort_class = 'sorting_desc';
                                                else :
                                                    $sort_class = 'both';
                                                endif;
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                            ?>
                                            <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('is_live') ?></th>

                                            <?php
                                            if ($this->request->getQuery('sort') == 'subscriber_level') :
                                                if ($this->request->getQuery('direction') == 'asc') :
                                                    $sort_class = 'sorting_asc';
                                                elseif ($this->request->getQuery('direction') == 'desc') :
                                                    $sort_class = 'sorting_desc';
                                                else :
                                                    $sort_class = 'both';
                                                endif;
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                            ?>
                                            <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('subscriber_level') ?></th>

                                            <?php
                                            if ($this->request->getQuery('sort') == 'difficulty_level') :
                                                if ($this->request->getQuery('direction') == 'asc') :
                                                    $sort_class = 'sorting_asc';
                                                elseif ($this->request->getQuery('direction') == 'desc') :
                                                    $sort_class = 'sorting_desc';
                                                else :
                                                    $sort_class = 'both';
                                                endif;
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                            ?>
                                            <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('difficulty_level') ?></th>

                                            <?php
                                            if ($this->request->getQuery('sort') == 'category_id') :
                                                if ($this->request->getQuery('direction') == 'asc') :
                                                    $sort_class = 'sorting_asc';
                                                elseif ($this->request->getQuery('direction') == 'desc') :
                                                    $sort_class = 'sorting_desc';
                                                else :
                                                    $sort_class = 'both';
                                                endif;
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                            ?>
                                            <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('category_id') ?></th>

                                            <th scope="col" class="actions"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($recipes as $key => $recipe): ?>
                                            <tr class="entity-row-<?= $key ?>">
                                                <td><a href="<?= $this->Url->build(['controller' => 'Recipes', 'action' => 'view', $recipe->recipe_id]) ?>"><?= h($recipe->recipe_title) ?></a></td>
                                                <td class="text-center"><?= $this->Util->getBooleanIcon(h($recipe->is_live)) ?></td>
                                                <td><?= $this->Util->returnSubscriberLevel($recipe->subscriber_level) ?></td>
                                                <td><?= $this->Util->returnDifficulty($recipe->difficulty_level) ?></td>
                                                <td><?= $recipe->has('category') ? $recipe->category->category_name : '-' ?></td>
                                                <td class="actions">
                                                    <?= $this->Html->link(__('View'), ['controller' => 'Recipes', 'action' => 'view', $recipe->recipe_id], ['class' => 'btn btn-primary btn-square btn-sm']) ?>
                                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Recipes', 'action' => 'edit', $recipe->recipe_id], ['class' => 'btn btn-info btn-square btn-sm']) ?>
                                                    <btn class="delete-entity btn btn-sm btn-square btn-danger" data-key="<?= $key ?>" data-csrf="<?= $this->request->getParam('_csrfToken') ?>" data-id="<?= $recipe->recipe_id ?>" data-delete-url="<?= $this->Url->build(['controller' => 'Recipes', 'action' => 'delete', '_ext' => 'json']); ?>"><?= __('Delete') ?></btn>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 col-md-5">
                                    <div class="dataTables_info" role="status" aria-live="polite">
                                        <?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-7">
                                    <div class="dataTables_paginate paging_simple_numbers">
                                        <ul class="pagination">
                                            <?= $this->Paginator->first() ?>
                                            <?= $this->Paginator->prev() ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next() ?>
                                            <?= $this->Paginator->last() ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php else : ?>
                    <h3 class="text-center"><?= __('There are no records yet. Click here to add one.') ?></h3>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>