<?php
use Migrations\AbstractMigration;

class AdminsMigration1 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $admins_table = $this->table('admins', ['id' => false, 'primary_key' => 'admin_id']);
        $admins_table
            ->addColumn('admin_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
            ->addColumn('admin_name', 'string', ['limit' => 255])
            ->addColumn('display_name', 'string', ['limit' => 255])
            ->addColumn('email_address', 'string', ['limit' => 255])
            ->addColumn('admin_password', 'string', ['limit' => 255])
            ->addColumn('created', 'datetime')
            ->addColumn('modified', 'datetime')
            ->create();
    }
}
