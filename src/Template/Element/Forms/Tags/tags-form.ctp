<?php
/**
 * @var \App\View\AppView $this
 * @var string $method
 * @var Cake\Datasource\EntityInterface $tag */
?>

<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h2><?= $method == 'add' ? __('Add New Tag') : __('Edit Tag') ?></h2>
            </div>
            <div class="widget-body">
                <?= $this->Form->create($tag); ?>

                <?= $this->Form->control('tag_id') ?>
                <?= $this->Form->control('tag_name') ?>

                <div class="text-right">
                    <input class="btn btn-primary btn-square" type="submit" value="<?= __('Save Tag') ?>">
                </div>

                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>