<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RecipePlatingDescriptionsFixture
 *
 */
class RecipePlatingDescriptionsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'recipe_plating_description_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'recipe_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'plating_image_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'plating_description' => ['type' => 'string', 'length' => 2000, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'recipe_id' => ['type' => 'index', 'columns' => ['recipe_id'], 'length' => []],
            'plating_image_id' => ['type' => 'index', 'columns' => ['plating_image_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['recipe_plating_description_id'], 'length' => []],
            'recipe_plating_descriptions_ibfk_1' => ['type' => 'foreign', 'columns' => ['recipe_id'], 'references' => ['recipes', 'recipe_id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'recipe_plating_descriptions_ibfk_2' => ['type' => 'foreign', 'columns' => ['plating_image_id'], 'references' => ['images', 'image_id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'recipe_plating_description_id' => 1,
                'recipe_id' => 1,
                'plating_image_id' => 1,
                'plating_description' => 'Lorem ipsum dolor sit amet'
            ],
        ];
        parent::init();
    }
}
