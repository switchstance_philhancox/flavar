<?php
/*********************************************************
 *
 * If you prefer, the below should be placed in an element
 * file as described above so it can be shared between
 * add/edit view.
 *
 * Then delete the form below up until "end of element"
 *
 *********************************************************/
?>

<?php
/**
 * @var \App\View\AppView $this
 * @var string $method
 * @var App\Model\Entity\QuantityType $quantity_type */
?>

<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h2><?= $method == 'add' ? __('Add New Quantity Type') : __('Edit Quantity Type') ?></h2>
            </div>
            <div class="widget-body">
                <?= $this->Form->create($quantity_type); ?>

                <?= $this->Form->control('quantity_type_id') ?>
                <?= $this->Form->control('quantity_type_name') ?>

                <div class="text-right">
                    <input class="btn btn-primary btn-square" type="submit" value="<?= __('Save Record') ?>">
                </div>

                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>