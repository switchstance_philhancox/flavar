<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= isset($title) ? $title : $this->Util->getMetaTitle($this->request) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>vendor/elisyam/css/base/bootstrap.min.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>vendor/elisyam/css/base/elisyam-1.5.min.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>vendor/elisyam/css/datatables/datatables.min.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>vendor/bootstrap-sweetalert/sweetalert.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>vendor/summernote/dist/summernote.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>vendor/codemirror/codemirror.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>vendor/sweetalert/css/sweetalert.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>vendor/bootstrap-select/css/bootstrap-select.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>assets/css/animate/animate.min.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>css/admin.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css"
          integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <?= $this->Html->meta('icon', $this->Url->build('/', true) . 'img/flavar-favicon.png'); ?>
</head>