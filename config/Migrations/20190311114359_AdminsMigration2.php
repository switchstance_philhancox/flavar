<?php
use Migrations\AbstractMigration;

class AdminsMigration2 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $admins_table = $this->table('admins');
        $admins_table
            ->addColumn('password_reset_code', 'string', ['limit' => 255, 'null' => true, 'after' => 'admin_password'])
            ->addColumn('password_reset_expiry_date', 'datetime', ['null' => true, 'after' => 'password_reset_code'])
            ->update();
    }
}
