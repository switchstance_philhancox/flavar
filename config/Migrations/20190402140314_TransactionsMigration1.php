<?php
use Migrations\AbstractMigration;

class TransactionsMigration1 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('transactions', ['id' => false, 'primary_key' => 'transaction_id']);
        $table
            ->addColumn('transaction_id', 'integer', ['signed' => false, 'limit' => 11, 'identity' => true])
            ->addColumn('store_transaction_id', 'string', ['length' => '100'])
            ->addColumn('user_id', 'integer', ['limit' => 11, 'signed' => false])
            ->addForeignKey('user_id', 'users', 'user_id', ['update' => 'CASCADE', 'delete' => 'CASCADE'])
            ->addColumn('is_apple', 'boolean', ['default' => false])
            ->addColumn('receipt_data', 'text')
            ->addColumn('purchase_date', 'datetime')
            ->addColumn('expiry_date', 'datetime')
            ->create();
    }
}
