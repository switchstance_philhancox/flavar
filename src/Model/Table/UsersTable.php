<?php
namespace App\Model\Table;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('user_id');
        $this->setPrimaryKey('user_id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('user_id')
            ->allowEmptyString('user_id', 'create');

        $validator
            ->scalar('user_name')
            ->maxLength('user_name', 255)
            ->requirePresence('user_name', 'create', 'Please enter name')
            ->allowEmptyString('user_name', false, 'Please enter name');

        $validator
            ->scalar('display_name')
            ->maxLength('display_name', 255)
            ->requirePresence('display_name', 'create', 'Please enter display name')
            ->allowEmptyString('display_name', true);

        $validator
            ->scalar('email_address')
            ->maxLength('email_address', 255)
            ->requirePresence('email_address', 'create', 'Please enter valid email address')
            ->allowEmptyString('email_address', false, 'Please enter valid email address');

        $validator
            ->scalar('user_password')
            ->maxLength('user_password', 255)
            ->requirePresence('user_password', 'create', 'Please enter password')
            ->allowEmptyString('user_password', false, 'Please enter password');

        return $validator;
    }

    public function validationExtended(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->requirePresence('user_password2', 'create', 'Please repeat the password');

        $validator
            ->add('user_password2', [
                'length' => [
                    'rule'    => ['minLength', 6],
                    'message' => 'The password have to be at least 6 characters',
                ],
            ])
            ->add('user_password2', [
                'compare' => [
                    'rule'    => ['compareWith', 'user_password'],
                    'message' => 'Passwords are not equal',
                ],
            ])
            ->allowEmptyString('user_password2', false);

        return $validator;
    }

    public function validationPassword(Validator $validator)
    {
        $validator
            ->add('user_password', [
                'length' => [
                    'rule'    => ['minLength', 6],
                    'message' => 'The password have to be at least 6 characters',
                ],
            ])
            ->allowEmptyString('user_password', false);


        $validator
            ->add('user_password2', [
                'length' => [
                    'rule'    => ['minLength', 6],
                    'message' => 'The password have to be at least 6 characters',
                ],
            ])
            ->add('user_password2', [
                'compare' => [
                    'rule'    => ['compareWith', 'user_password'],
                    'message' => 'Passwords are not equal',
                ],
            ])
            ->allowEmptyString('user_password2', false);

        return $validator;
    }

    public function validationUpdatingOwnPassword(Validator $validator)
    {

        $validator
            ->add('old_passwd', 'custom', [
                'rule'    => function ($value, $context)
                {
                    $user = $this->get($context['data']['id']);
                    if ($user)
                    {
                        if ((new DefaultPasswordHasher())->check($value, $user->user_password))
                        {
                            return true;
                        }
                    }

                    return false;
                },
                'message' => 'The old password does not match the current password!',
            ])
            ->allowEmptyString('old_passwd', false);


        $validator
            ->add('user_password', [
                'length' => [
                    'rule'    => ['minLength', 6],
                    'message' => 'The password have to be at least 6 characters!',
                ],
            ])
            ->allowEmptyString('user_password', false);


        $validator
            ->add('user_password2', [
                'length' => [
                    'rule'    => ['minLength', 6],
                    'message' => 'The password have to be at least 6 characters!',
                ],
            ])
            ->add('user_password2', [
                'compare' => [
                    'rule'    => ['compareWith', 'user_password'],
                    'message' => 'Passwords are not equal',
                ],
            ])
            ->allowEmptyString('user_password2', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->addCreate(function ($entity, $options)
        {
            return !$entity->email_address || !$this->exists(['Users.email_address' => $entity->email_address, 'Users.is_deleted' => false]);
        }, 'uniqueUserEmail', [
            'errorField' => 'email',
            'message'    => 'User with this email address already exists.',
        ]);


        $rules->addUpdate(function ($entity, $options)
        {
            return !$entity->email_address || !$this->exists(['Users.user_id !=' => $entity->user_id, 'Users.email_address' => $entity->email_address, 'Users.is_deleted' => false]);
        }, 'uniqueUserEmail', [
            'errorField' => 'email',
            'message'    => 'User with this email address already exists.',
        ]);

        return $rules;
    }
}
