Hello,<br><br>

new message has been sent using contact form in mobile application. See details below.<br><br>

<strong>User</strong>: <?= $user->full_name; ?> (#<?= $user->user_id; ?>)<br>
<strong>User email</strong>: <?= $user->email; ?><br>
<strong>Message</strong>: <?= h($message); ?>
