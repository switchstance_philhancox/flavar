<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Recipe $recipe
 * @var \App\Model\Entity\RecipeUtensil[] $recipe_utensils
 * @var \App\Model\Entity\RecipeUtensil $new_recipe_utensil
 */
?>

<div class="row">
    <div class="page-header pb-0">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <span class="c-grey fw-normal"><?= __('Recipes') ?></span><br>
                <a href="<?= $this->Url->build(['controller' => 'Recipes', 'action' => 'view', $recipe->recipe_id]) ?>">
                    <?= $recipe->recipe_title ?></a> > <?= __('Utensils') ?>
            </h2>
        </div>
    </div>
</div>

<div class="widget">
    <div class="widget-body">
        <form method="post" action="<?= $this->Url->build(['controller' => 'RecipeUtensils', 'action' => 'edit', $recipe->recipe_id]) ?>">
            <table class="table table-condensed table-bordered">
                <thead>
                <tr>
                    <th><?= __('Utensil') ?></th>
                    <th><?= __('Internal Name') ?><br><small><?= __('For use in identifying the utensils when creating a method, particularly if there are duplicate utensils being used,') ?></small></th>
                    <th><?= __('') ?></th>
                </tr>
                </thead>
                <tbody id="recipe-utensils-container">
                <?= $this->element('Arr' . DS . 'recipe-utensils-row', ['key' => '{{counter}}', 'recipe_utensil' => $new_recipe_utensil]) ?>
                <?php foreach ($recipe_utensils as $key => $recipe_utensil) : ?>
                    <?= $this->element('Arr' . DS . 'recipe-utensils-row', ['key' => $key, 'recipe_utensil' => $recipe_utensil]) ?>
                <?php endforeach; ?>
                </tbody><tfoot>
                <tr>
                    <td colspan="6">
                        <a class="btn btn-secondary btn-square btn-sm arr-add-row"
                           data-container-id="recipe-utensils-container" data-minimum-rows="2">
                            <?= __('Add another utensil') ?>
                        </a>
                    </td>
                </tr>
                </tfoot>
            </table>

            <div class="row">
                <div class="col-md-12">
                    <input type="submit" value="<?= __('Save utensils') ?>" class="pull-right btn btn-lg btn-primary btn-square">
                </div>
            </div>
        </form>
    </div>
</div>
