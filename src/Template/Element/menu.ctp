<!-- Begin Navigation -->
<div class="horizontal-menu">
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-light navbar-expand-lg main-menu ">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li><a href="<?= $this->Url->build(['controller' => 'Dashboard', 'action' => 'index']) ?>"><?= __('Recipes') ?></a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" id="dashboard" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= __('Config') ?></a>
                            <ul class="dropdown-menu" aria-labelledby="dashboard">
                                <li><a href="<?= $this->Url->build(['controller' => 'Utensils', 'action' => 'index']) ?>"><?= __('Utensils') ?></a></li>
                                <li><a href="<?= $this->Url->build(['controller' => 'Categories', 'action' => 'index']) ?>"><?= __('Categories') ?></a></li>
                                <li><a href="<?= $this->Url->build(['controller' => 'Tags', 'action' => 'index']) ?>"><?= __('Tags') ?></a></li>
                                <li><a href="<?= $this->Url->build(['controller' => 'QuantityTypes', 'action' => 'index']) ?>"><?= __('Quantity Types') ?></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>