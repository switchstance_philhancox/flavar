<?php
use Migrations\AbstractMigration;

class UsersMigration2 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table
            ->addColumn('password_reset_code', 'string', ['limit' => 32, 'null' => true, 'after' => 'user_password'])
            ->addColumn('password_reset_code_expiration_date', 'datetime', ['null' => true, 'after' => 'password_reset_code'])
            ->update();
    }
}
