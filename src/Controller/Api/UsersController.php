<?php

namespace App\Controller\Api;

use App\Controller\Component\UtilComponent;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Mailer\Email;

/**
 * Users Controller
 *
 * @property \App\Model\Table\TransactionsTable $Transactions
 * @property \App\Controller\Component\ReceiptsValidatorComponent $ReceiptsValidator
 * @property \App\Model\Table\UsersTable           $Users
 */
class UsersController extends AppController
{

    const PURCHASE_STATUS_SUBSCRIPTION_EXPIRED = 6778003;
    const PURCHASE_STATUS_INVALID_PAYLOAD = 6778001;

	public function initialize()
	{
		parent::initialize();
        $this->loadComponent('ReceiptsValidator');
	}

	public function info()
    {
        echo json_encode([
            'name' => 'Flavar API',
            'version' => '1.0.0'
        ]);
        die;
    }

	public function register()
	{
		$user = $this->Users->newEntity();
		if ($this->request->is('post'))
		{
		    $user_data = $this->request->getData();
		    if(empty($user_data['user_name'])) {
                $user_data['display_name'] = '';
            } else {
		        $name_array = explode(' ', $user_data['user_name']);
                $user_data['display_name'] = $name_array[0];
            }
		    $user_data['user_password'] = $this->request->getData('password', '');
			unset($user_data['password']);
            $user_data['user_password2'] = $this->request->getData('password2', '');
			unset($user_data['password2']);
            $user_data['is_live'] = true;
            $user_data['is_deleted'] = false;

			$user = $this->Users->patchEntity($user, $user_data, ['validate' => 'extended']);
			if ($this->Users->save($user))
			{
			    $this->RestAuth->generateAuthToken($user);
                $user->subscription_expiry = null;

                echo json_encode([
                    'user' => $user
                ]);
				die;
			}
			else
			{
				echo $this->getJsonErrorResponse(400, 'Registration failed with following errors. '.$this->formatValidationErrors($user->getErrors()));
				die;
			}
		} else {
			echo $this->getJsonErrorResponse(400, 'Invalid request type');
			die;
		}
	}

	public function login()
	{
		if ($this->request->is('post'))
		{
			$user = $this->RestAuth->authenticate();

			if ($user)
			{
                $this->loadModel('Transactions');
                $latest_transaction = $this->Transactions->find()
                    ->where([
                        'Transactions.user_id' => $user->user_id
                    ])
                    ->order(['Transactions.expiry_date DESC'])
                    ->first();



                if(!empty($latest_transaction) && $latest_transaction->expiry_date) {
                    $subscription_expiry_date = $latest_transaction->expiry_date->i18nFormat(Time::UNIX_TIMESTAMP_FORMAT) * 1000;
                } else {
                    $subscription_expiry_date = null;
                }

                $user->subscription_expiry = $subscription_expiry_date;

				echo json_encode([
					'user' => $user
				]);
				die;
			}

			echo $this->getJsonErrorResponse(400, 'Your email or password is incorrect');
			die;
		}
		else
		{
			echo $this->getJsonErrorResponse(400, 'Invalid request type');
			die;
		}
	}

	public function changePassword()
    {
        if ($this->request->is('post'))
        {
            $old_password = $this->request->getData('old_password', '');
            $dfh = new DefaultPasswordHasher();

            try
            {
                $user = $this->Users->get($this->currentUserId);
            }
            catch (\Exception $e)
            {
                echo $this->getJsonErrorResponse(500, 'Error occurred while trying to change user password (REF: USNF1)');
                die;
            }


            if(!$dfh->check($old_password, $user->user_password))
            {
                echo $this->getJsonErrorResponse(400, 'Provided password is incorrect');
                die;
            }

            $user = $this->Users->patchEntity($user, ['user_password' => $this->request->getData('password', '')]);
            if($this->Users->save($user))
            {
                echo json_encode([
                    'data' => [
                        'success' => true
                    ]
                ]);
                die;
            }
            else
            {
                echo $this->getJsonErrorResponse(400, $this->formatValidationErrors($user->getErrors()));
                die;
            }
        } else {
            echo $this->getJsonErrorResponse(400, 'Invalid request type');
            die;
        }
    }

    public function updateDetails()
    {
        if ($this->request->is('post'))
        {
            try
            {
                $user = $this->Users->get($this->currentUserId);
            }
            catch (\Exception $e)
            {
                echo $this->getJsonErrorResponse(500, 'Error occurred while trying to change user details (REF: USNF1)');
                die;
            }

            $data = [
                'user_name' => $this->request->getData('user_name', ''),
                'email_address' => $this->request->getData('email_address', ''),
            ];

            $user = $this->Users->patchEntity($user, $data);
            if($this->Users->save($user))
            {
                echo json_encode([
                    'data' => [
                        'success' => true
                    ]
                ]);
                die;
            }
            else
            {
                echo $this->getJsonErrorResponse(400, $this->formatValidationErrors($user->getErrors()));
                die;
            }
        } else {
            echo $this->getJsonErrorResponse(400, 'Invalid request type');
            die;
        }
    }


    public function sendPasswordResetCode()
    {
        if ($this->request->is('post'))
        {
            $email = $this->request->getData('email', '');
            $user = $this->Users->find()
                ->where([
                    'Users.email_address' => $email,
                    'Users.is_deleted' => false
                ])->first();

            if(empty($user))
            {
                echo $this->getJsonErrorResponse(400, 'Account for given email address doesn\'t exist');
                die;
            }

            $code = $this->Util->generateRandomString(16);
            $expiration_date = new \DateTime();
            $expiration_date->add(new \DateInterval('P1D'));
            $user = $this->Users->patchEntity($user, [
                'password_reset_code' => $code,
                'password_reset_code_expiration_date' => $expiration_date->format('Y-m-d H:i:s')
            ]);

            if($this->Users->save($user))
            {
                $email = new Email('default');
                $email->setFrom([UtilComponent::APPLICATION_EMAIL => UtilComponent::APPLICATION_NAME])
                    ->setTo($user->email_address)
                    ->setSubject('['.UtilComponent::APPLICATION_NAME.'] Password reset code for your account')
                    ->setTemplate('password')
                    ->setEmailFormat('both');

                $email->setViewVars([
                    'user' => $user,
                    'code' => $code
                ]);

                try {
                    $email->send();
                    echo json_encode([
                        'data' => [
                            'success' => true
                        ]
                    ]);
                    die;
                } catch(\Exception $e) {
                    $this->log($e);
                    echo $this->getJsonErrorResponse(500, 'Sending password reset code failed. Please try again later.');
                    die;
                }
            }
            else
            {
                echo $this->getJsonErrorResponse(500, 'Error occurred while setting password reset code. Please try again later');
                die;
            }
        } else {
            echo $this->getJsonErrorResponse(400, 'Invalid request type');
            die;
        }
    }

    public function resetPassword()
    {
        if ($this->request->is('post'))
        {
            $email = $this->request->getData('email', '');
            $code = $this->request->getData('reset_code', '');

            $user = $this->Users->find()
                ->where([
                    'Users.email_address' => $email,
                    'Users.is_deleted' => false
                ])->first();

            if(empty($user))
            {
                echo $this->getJsonErrorResponse(400, 'Account for given email address doesn\'t exist');
                die;
            }

            if($user->password_reset_code == null)
            {
                echo $this->getJsonErrorResponse(400, 'Password reset has not been requested for this account');
                die;
            }

            if($user->password_reset_code != $code)
            {
                echo $this->getJsonErrorResponse(400, 'Provided password reset code is not valid');
                die;
            }

            $now = new \DateTime();
            $expiration_date = \DateTime::createFromFormat('Y-m-d H:i:s', $user->password_reset_code_expiration_date->format('Y-m-d H:i:s'));

            if($expiration_date < $now)
            {
                echo $this->getJsonErrorResponse(400, 'Provided password reset code has expired');
                die;
            }

            $user = $this->Users->patchEntity($user, [
                'user_password' => $this->request->getData('password', $this->Util->generateRandomString(16)),
                'password_reset_code' => null,
                'password_reset_code_expiration_date' => null
            ]);

            if($this->Users->save($user))
            {
                echo json_encode([
                    'data' => [
                        'success' => true
                    ]
                ]);
                die;
            }
            else
            {
                echo $this->getJsonErrorResponse(400, $this->formatValidationErrors($user->getErrors()));
                die;
            }
        } else {
            echo $this->getJsonErrorResponse(400, 'Invalid request type');
            die;
        }
    }

	public function validateToken()
	{
	    $isValid = $this->RestAuth->isAuthenticated();
        $subscription_expiry_date = null;

	    if($isValid) {
	        $this->loadModel('Transactions');
	        $latest_transaction = $this->Transactions->find()
                ->where([
                    'Transactions.user_id' => $this->RestAuth->getAuthenticatedUserId()
                ])
                ->order(['Transactions.expiry_date DESC'])
                ->first();

	        if(!empty($latest_transaction) && $latest_transaction->expiry_date) {
                $subscription_expiry_date = $latest_transaction->expiry_date->i18nFormat(Time::UNIX_TIMESTAMP_FORMAT) * 1000;
            }
        }

		echo json_encode([
			'data' => [
                'isValid' => $isValid,
                'subscription_expiry' => $subscription_expiry_date
            ]
		]);
		die;
	}

	public function validateReceipt()
    {
        if($this->request->is('post'))
        {
            $transaction_data = $this->request->getData('transaction');
            $is_apple = $this->request->getData('is_apple');

            if(empty($transaction_data))
            {
                echo $this->getJsonErrorResponse(400, 'Request is missing compulsory parameters');
                die;
            }

            //debug($transaction_data);die;

            if($is_apple)
            {

                if(empty($transaction_data['transaction']['appStoreReceipt']))
                {
                    echo $this->getJsonErrorResponse(400, 'Receipt is missing compulsory data');
                    die;
                }

                $output = $this->ReceiptsValidator->setAppleSharedSecret(Configure::read('AppStore.SharedSecret'))
                    ->setEndpointUrl(Configure::read('AppStore.ValidationEndpoint'))
                    ->setAppleReceiptData($transaction_data['transaction']['appStoreReceipt'])
                    ->validateAppleReceipt();

                if($output['ok'] && isset($output['data']['transaction']) && $output['data']['transaction']) {
                    $this->loadModel('Transactions');
                    $this->Transactions->storeAppleTransaction($this->currentUserId, $output['data']['transaction'], $transaction_data['transaction']['appStoreReceipt']);
                }

                echo json_encode($output);
                die;
            }
            else
            {
                if(empty($transaction_data['transaction']['purchaseToken']))
                {
                    echo $this->getJsonErrorResponse(400, 'Receipt is missing compulsory data');
                    die;
                }

                $output = $this->ReceiptsValidator->setAndroidBundleName(Configure::read('PlayStore.BundleName'))
                    ->setAndroidProductId(Configure::read('PlayStore.ProductId'))
                    ->setAuthConfigPath(ROOT . DS . 'gkeys' . DS . Configure::read('PlayStore.CredentialsFile'))
                    ->setGoogleTransactionData($transaction_data['transaction'])
                    ->validateGoogleReceipt();

                if($output['ok'] && isset($output['data']['transaction']) && $output['data']['transaction']) {
                    $this->loadModel('Transactions');
                    $this->Transactions->storeGoogleTransaction($this->currentUserId, $output['data']['transaction'], $transaction_data['transaction']['purchaseToken']);
                }

                echo json_encode($output);
                die;
            }
        }
        else
        {
            echo $this->getJsonErrorResponse(400, 'Invalid request type');
            die;
        }
    }
}
