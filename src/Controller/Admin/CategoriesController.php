<?php
namespace App\Controller\Admin;

use App\Model\Entity\Category;

/**
 * Categories Controller
 *
 * @property \App\Model\Table\CategoriesTable $Categories */
class CategoriesController extends AppController
{
	
	public function index()
	{
        $search_term = $this->request->getQuery('search_term');

        $where = [];
        if ($search_term)
        {
            $where = [
                'OR' => [
                    'category_name LIKE' => '%' . $search_term . '%',
                ]
            ];
        }

        $categories_query = $this->Categories->find()->where($where);

        $categories = $this->paginate($categories_query);

        $this->set([
            'categories' => $categories,
            'search_term' => $search_term,
        ]);
        $this->set('_serialize', ['categories']);
	}

    /**
    * @return \Cake\Http\Response|null
    */
	public function add()
	{
		$category = $this->Categories->newEntity();
		if ($this->request->is('post'))
		{
            $data = $this->Categories->prepareData($this->request->getData());
			$category = $this->Categories->patchEntity($category, $data);
			if ($this->Categories->save($category))
			{
				$this->Flash->success(__('The category has been saved.'));
				return $this->redirect(['action' => 'index']);
			}
			else
			{
				$this->Flash->error(__('The category could not be saved. Please, try again.'));
			}
		}

		
        $this->set([
            'category' => $category,
        ]);

	}


	    /**
    * @param null $category_id
    *
    * @return \Cake\Http\Response|null
    */
	public function edit($category_id = null)
	{
        $category = $this->Categories->find()->where(['category_id' => $category_id])->first();

        if (!$category)
        {
            $this->Flash->error('The $category could not be found. Please, try again.');
            return $this->redirect(['action' => 'index']);
        }
		 
		if ($this->request->is(['patch', 'post', 'put']))
		{
            $data = $this->Categories->prepareData($this->request->getData());
			$category = $this->Categories->patchEntity($category, $data);
			if ($this->Categories->save($category))
			{
				$this->Flash->success(__('The category has been saved.'));
				return $this->redirect(['action' => 'index']);
			}
			else
			{
                $this->log($category->getErrors());
				$this->Flash->error(__('The category could not be saved. Please, try again.'));
			}
		}
		
        $this->set([
            'category' => $category,
        ]);
        $this->set('_serialize', ['categories']);
	}

    /**
     * @return \Cake\Http\Response|null
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);

        if ($this->request->is('json'))
        {
            $category_id = $this->request->getData('id');

            /** @var Category $category */
            $category = $this->Categories->find()->where(['category_id' => $category_id])->first();

            if (!$category)
            {
                $output = [
                    'status' => false,
                    'message' => __('The category could not be found. Please try again.'),
                ];
            }
            else
            {
                if (!$this->Categories->delete($category))
                {
                    $this->log($category->getErrors());
                    $output = [
                        'status' => false,
                        'message' => __('There was a problem trying to delete the category. Please try again.'),
                    ];
                }
                else
                {
                    $output = [
                        'status' => true,
                        'message' => __('The category was successfully deleted.'),
                    ];
                }
            }

            $this->set(compact('output'));
            $this->set('_serialise', ['output']);
        }
        else
        {
            return $this->redirect('index');
        }
    }
}
