<?php
/**
 * @var integer $key
 * @var \App\Model\Entity\RecipeIngredient $recipe_ingredient
 * @var \App\Model\Entity\QuantityType[] $quantity_types
 * @var \App\Model\Entity\Recipe $recipe
 */
?>

<tr class="arr-row<?= $key === '{{counter}}' ? ' d-none' : '' ?>" data-key="<?= $key ?>">
    <input type="hidden" name="recipe_ingredients[<?= $key ?>][recipe_ingredient_id]" value="<?= $recipe_ingredient->recipe_ingredient_id ?>">
    <td>
        <input type="number" step="0.01" min="0" class="form-control" name="recipe_ingredients[<?= $key ?>][quantity]" value="<?= $recipe_ingredient->quantity ?>">
    </td>
    <td>
        <select class="form-control quantity-type-selector" data-qty-span="#qty-add-serving-<?= $key ?>" name="recipe_ingredients[<?= $key ?>][quantity_type_id]">
            <?php foreach ($quantity_types as $quantity_type) : ?>
                <option value="<?= $quantity_type->quantity_type_id ?>"<?= $quantity_type->quantity_type_id == $recipe_ingredient->quantity_type_id ? ' selected="selected"' : '' ?>><?= $quantity_type->quantity_type_name ?></option>
            <?php endforeach; ?>
        </select>
    </td>
    <td>
        <input type="text" class="form-control" name="recipe_ingredients[<?= $key ?>][ingredient_name]" value="<?= $recipe_ingredient->ingredient_name ?>">
    </td>
    <?php if ($recipe->can_servings_be_adjusted) : ?>
    <td>
        <input type="number" step="0.01" min="0" class="form-control" name="recipe_ingredients[<?= $key ?>][quantity_per_additional_serving]" value="<?= $recipe_ingredient->quantity ?? '0' ?>">
    </td>
    <td>
        <span class="qty-add-serving" id="qty-add-serving-<?= $key ?>"><?= $recipe_ingredient->quantity_type ? $recipe_ingredient->quantity_type->quantity_type_name : '-' ?></span>
    </td>
    <?php else : ?>
        <input type="hidden" name="recipe_ingredients[<?= $key ?>][quantity_per_additional_serving]" value="0">
    <?php endif; ?>
    <td>
        <a href="#" class="btn btn-danger btn-sm btn-square arr-remove-row c-white<?= $key == 0 ? ' d-none' : '' ?>" data-minimum-rows="2" data-container-id="recipe-ingredients-container"><?= __('Remove') ?></a>
    </td>
</tr>