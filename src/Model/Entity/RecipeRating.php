<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RecipeRating Entity
 *
 * @property int $recipe_rating_id
 * @property int $recipe_id
 * @property int $rating
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Recipe $recipe
 * @property \App\Model\Entity\User $user
 */
class RecipeRating extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'recipe_id' => true,
        'rating' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'recipe' => true,
        'user' => true
    ];
}
