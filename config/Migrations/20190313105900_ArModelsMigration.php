<?php
use Migrations\AbstractMigration;

class ArModelsMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('ar_models', ['id' => false, 'primary_key' => 'ar_model_id']);
        $table
            ->addColumn('ar_model_id', 'integer', ['signed' => false, 'limit' => 11, 'identity' => true])
            ->addColumn('ar_model_file_name', 'string', ['limit' => 255])
            ->addColumn('ar_model_zip_file_name', 'string', ['limit' => 255])
            ->addColumn('ar_model_url', 'string', ['limit' => 255])
            ->addColumn('ar_model_file_size', 'integer', ['limit' => 11, 'signed' => false])
            ->addColumn('created', 'datetime')
            ->addColumn('modified', 'datetime')
            ->create();
    }
}
