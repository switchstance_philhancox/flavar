<?php
/**
 * @var \App\View\AppView $this
 * @var string $method
 * @var App\Model\Entity\Utensil $utensil */
?>

    <div class="row">
        <div class="col-xl-12">
            <!-- Default -->
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h2><?= $method == 'add' ? __('Add New Utensil') : __('Edit Utensil') ?></h2>
                </div>
                <div class="widget-body">
                    <?= $this->Form->create($utensil, ['type' => 'file']); ?>

                    <?= $this->Form->control('utensil_id') ?>
                    <?= $this->Form->control('utensil_name') ?>

                    <div class="form-group row d-flex mb-5 required">
                        <label class="col-lg-4 form-control-label d-flex justify-content-lg-end"
                               for="content"><?= __('Image') ?></label>
                        <input type="hidden" name="image_id"
                               value="<?= isset($utensil->image) ? $utensil->image->image_id : null ?>">

                        <div class="col-md-6 col-sm-12">
                            <?php if ($utensil->image !== null): ?>
                                <div class="fileinput fileinput-new">
                                    <div>
                                        <div class="fileinput-preview thumbnail pr-2 pb-2">
                                            <img src="<?= $this->Url->build('/', true) . $utensil->image->image_url ?>" style="max-height: 100px;">
                                        </div>
                                    </div>
                                    <a href="#" class="btn btn-danger removeImage waves-effect"
                                       data-remove="image_file"><?= __('Remove') ?></a>
                                </div>
                                <input type="hidden" name="remove_image_file" id="remove_image_file">
                                <br>
                            <?php endif; ?>

                            <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-primary btn-file m-r-10">
                                <span class="fileinput-new"><?= $method == 'add' ? __('Select image') : __('Select new image') ?></span>
                                <input type="file" name="image_file" class="file">
                            </span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <input class="btn btn-primary btn-square" type="submit" value="<?= __('Save Utensil') ?>">
                    </div>

                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>