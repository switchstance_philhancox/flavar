<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RecipeRatingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RecipeRatingsTable Test Case
 */
class RecipeRatingsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RecipeRatingsTable
     */
    public $RecipeRatings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.RecipeRatings',
        'app.Recipes',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RecipeRatings') ? [] : ['className' => RecipeRatingsTable::class];
        $this->RecipeRatings = TableRegistry::getTableLocator()->get('RecipeRatings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RecipeRatings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
