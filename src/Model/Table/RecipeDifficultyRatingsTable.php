<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RecipeDifficultyRatings Model
 *
 * @property \App\Model\Table\RecipesTable|\Cake\ORM\Association\BelongsTo $Recipes
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\RecipeDifficultyRating get($primaryKey, $options = [])
 * @method \App\Model\Entity\RecipeDifficultyRating newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RecipeDifficultyRating[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RecipeDifficultyRating|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RecipeDifficultyRating|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RecipeDifficultyRating patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RecipeDifficultyRating[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RecipeDifficultyRating findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RecipeDifficultyRatingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('recipe_difficulty_ratings');
        $this->setDisplayField('recipe_difficulty_rating_id');
        $this->setPrimaryKey('recipe_difficulty_rating_id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Recipes', [
            'foreignKey' => 'recipe_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('recipe_difficulty_rating_id')
            ->allowEmptyString('recipe_difficulty_rating_id', 'create');

        $validator
            ->integer('difficult_rating')
            ->requirePresence('difficult_rating', 'create')
            ->allowEmptyString('difficult_rating', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['recipe_id'], 'Recipes'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
