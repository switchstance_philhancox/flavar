<?php
namespace App\Controller\Admin;

use App\Model\Entity\RecipeStep;
use App\Model\Entity\RecipeStepUtensil;
use App\Model\Table\RecipesTable;
use App\Model\Table\RecipeStepUtensilsTable;
use App\Model\Table\RecipeUtensilsTable;

/**
 * RecipeSteps Controller
 *
 * @property RecipesTable $Recipes
 * @property \App\Model\Table\RecipeStepsTable $RecipeSteps
 * @property RecipeUtensilsTable $RecipeUtensils
 */
class RecipeStepsController extends AppController
{
    /**
     * @param $recipe_id
     *
     * @return \Cake\Http\Response|null
     */
	public function add($recipe_id)
	{
        $this->loadModel('Recipes');

        $recipe = $this->Recipes->find()->where(['recipe_id' => $recipe_id])->first();

        if (!$recipe)
        {
            $this->Flash->error('You cannot add ingredients to a recipe that doesn\'t exist.');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }

		$recipe_step = $this->RecipeSteps->newEntity();
		if ($this->request->is('post'))
		{
            $data = $this->request->getData();
            /** @var RecipeStep $last_recipe_step */
            $last_recipe_step = $this->RecipeSteps->find()->where(['recipe_id' => $recipe_id])->orderDesc('order_number')->first();

            $data['order_number'] = $last_recipe_step ? ($last_recipe_step->order_number + 1) : 1;
			$recipe_step = $this->RecipeSteps->patchEntity($recipe_step, $data);
			if ($this->RecipeSteps->save($recipe_step))
			{
				$this->Flash->success(__('The recipe step has been saved.'));
				return $this->redirect(['controller' => 'Recipes', 'action' => 'view', $recipe_id]);
			}
			else
			{
				$this->Flash->error(__('The recipe step could not be saved. Please, try again.'));
				$this->log($recipe_step->getErrors());
			}
		}

        $this->loadModel('RecipeUtensils');

		$utensils = $this->RecipeUtensils->find()->where(['recipe_id' => $recipe_id])->all();

        $this->set([
            'recipe' => $recipe,
            'recipe_step' => $recipe_step,
            'utensils' => $utensils,
        ]);
	}


	/**
    * @param null $recipe_step_id
    *
    * @return \Cake\Http\Response|null
    */
	public function edit($recipe_step_id = null)
	{
        $recipe_step = $this->RecipeSteps->find()->where(['recipe_step_id' => $recipe_step_id])->contain(['Recipes', 'RecipeUtensils' => ['Utensils']])->first();

        if (!$recipe_step)
        {
            $this->Flash->error('The recipe step could not be found. Please, try again.');
            return $this->redirect(['action' => 'index']);
        }
		 
		if ($this->request->is(['patch', 'post', 'put']))
		{
            $data = $this->request->getData();

			$recipe_step = $this->RecipeSteps->patchEntity($recipe_step, $data);

			if ($this->RecipeSteps->save($recipe_step))
			{
				$this->Flash->success(__('The recipe step has been saved.'));
                return $this->redirect(['controller' => 'Recipes', 'action' => 'view', $recipe_step->recipe_id]);
			}
			else
			{
                $this->log($recipe_step->getErrors());
				$this->Flash->error(__('The recipe step could not be saved. Please, try again.'));
			}
		}

        $this->loadModel('RecipeUtensils');

        $utensils = $this->RecipeUtensils->find()->where(['recipe_id' => $recipe_step->recipe_id])->all();
		
        $this->set([
            'recipe' => $recipe_step->recipe,
            'recipe_step' => $recipe_step,
            'utensils' => $utensils,
        ]);
        $this->set('_serialize', ['recipeSteps']);
	}

    /**
     * @return \Cake\Http\Response|null
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);

        if ($this->request->is('json'))
        {
            $recipe_step_id = $this->request->getData('id');

            /** @var RecipeStep $recipe_step */
            $recipe_step = $this->RecipeSteps->find()->where(['recipe_step_id' => $recipe_step_id])->first();

            if (!$recipe_step)
            {
                $output = [
                    'status' => false,
                    'message' => __('The step could not be found. Please try again.'),
                ];
            }
            else
            {
                if (!$this->RecipeSteps->delete($recipe_step))
                {
                    $this->log($recipe_step->getErrors());
                    $output = [
                        'status' => false,
                        'message' => __('There was a problem trying to delete the step. Please try again.'),
                    ];
                }
                else
                {
                    $output = [
                        'status' => true,
                        'message' => __('The step was successfully deleted.'),
                    ];
                }
            }

            $this->set(compact('output'));
            $this->set('_serialise', ['output']);
        }
        else
        {
            return $this->redirect('index');
        }
    }

    public function reorderSteps()
    {
        $this->request->allowMethod(['post', 'delete']);

        if ($this->request->is('json'))
        {
            $data = $this->request->getData();

            if (isset($data['recipe-steps']))
            {
                foreach ($data['recipe-steps'] as $order_number => $recipe_step_id)
                {
                    /** @var RecipeStep $recipe_step */
                    $recipe_step = $this->RecipeSteps->find()->where(['recipe_step_id' => $recipe_step_id])->first();

                    $recipe_step->order_number = ($order_number + 1);

                    if (!$this->RecipeSteps->save($recipe_step))
                    {
                        $this->log($recipe_step->getErrors());
                    }
                }
            }
        }
        else
        {
            $this->Flash->error('Nope.');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }

    }

}
