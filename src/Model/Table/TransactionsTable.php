<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Transactions Model
 *
 * @property \App\Model\Table\StoreTransactionsTable|\Cake\ORM\Association\BelongsTo $StoreTransactions
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Transaction get($primaryKey, $options = [])
 * @method \App\Model\Entity\Transaction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Transaction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Transaction|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Transaction|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Transaction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Transaction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Transaction findOrCreate($search, callable $callback = null, $options = [])
 */
class TransactionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('transactions');
        $this->setDisplayField('transaction_id');
        $this->setPrimaryKey('transaction_id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('transaction_id')
            ->allowEmptyString('transaction_id', 'create');

        $validator
            ->boolean('is_apple')
            ->requirePresence('is_apple', 'create')
            ->allowEmptyString('is_apple', false);

        $validator
            ->scalar('receipt_data')
            ->requirePresence('receipt_data', 'create')
            ->allowEmptyString('receipt_data', true);

        $validator
            ->dateTime('purchase_date')
            ->requirePresence('purchase_date', 'create')
            ->allowEmptyDateTime('purchase_date', false);

        $validator
            ->dateTime('expiry_date')
            ->requirePresence('expiry_date', 'create')
            ->allowEmptyDateTime('expiry_date', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function storeAppleTransaction($user_id, $transaction_data, $encoded_receipt) {
        $q = $this->find()
            ->where([
                'Transactions.store_transaction_id' => $transaction_data['transaction_id'],
                'Transactions.user_id' => $user_id
            ]);

        if($q->count() > 0) {
            return;
        }

        $receipt_data = strlen($encoded_receipt) <= 65535 ? $encoded_receipt : '';
        $purchase_date = new \DateTime('@'.round($transaction_data['purchase_date_ms']/1000));
        $expiry_date = new \DateTime('@'.round($transaction_data['expires_date_ms']/1000));

        $transaction = $this->newEntity([
            'store_transaction_id' => $transaction_data['transaction_id'],
            'user_id' => $user_id,
            'is_apple' => true,
            'receipt_data' => $receipt_data,
            'purchase_date' => $purchase_date->format('Y-m-d H:i:s'),
            'expiry_date' => $expiry_date->format('Y-m-d H:i:s')
        ]);

        return $this->save($transaction);
    }

    public function storeGoogleTransaction($user_id, $transaction_data, $purchaseToken) {
        $q = $this->find()
            ->where([
                'Transactions.store_transaction_id' => $transaction_data['orderId'],
                'Transactions.user_id' => $user_id
            ]);

        if($q->count() > 0) {
            return;
        }

        $receipt_data = strlen($purchaseToken) <= 65535 ? $purchaseToken : '';
        $purchase_date = new \DateTime('@'.round($transaction_data['startTimeMillis']/1000));
        $expiry_date = new \DateTime('@'.round($transaction_data['expiryTimeMillis']/1000));

        $transaction = $this->newEntity([
            'store_transaction_id' => $transaction_data['orderId'],
            'user_id' => $user_id,
            'is_apple' => false,
            'receipt_data' => $receipt_data,
            'purchase_date' => $purchase_date->format('Y-m-d H:i:s'),
            'expiry_date' => $expiry_date->format('Y-m-d H:i:s')
        ]);

        return $this->save($transaction);
    }
}
