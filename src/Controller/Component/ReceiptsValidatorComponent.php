<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

/**
 *
 * Class ReceiptsValidatorComponent
 * @package App\Controller\Component
 */
class ReceiptsValidatorComponent extends Component {

    const CODE_INVALID_PAYLOAD = 6778001;
    const CODE_CONNECTION_ERROR = 6778002;
    const CODE_PURCHASE_EXPIRED  = 6778003;

    const RESULT_APPSTORE_CANNOT_READ = 21000;
    const RESULT_DATA_MALFORMED = 21002;
    const RESULT_RECEIPT_NOT_AUTHENTICATED = 21003;
    const RESULT_SHARED_SECRET_NOT_MATCH = 21004;
    const RESULT_RECEIPT_SERVER_UNAVAILABLE = 21005;
    const RESULT_RECEIPT_VALID_BUT_SUB_EXPIRED = 21006;
    const RESULT_SANDBOX_RECEIPT_SENT_TO_PRODUCTION = 21007;
    const RESULT_PRODUCTION_RECEIPT_SENT_TO_SANDBOX = 21008;
    const RESULT_RECEIPT_WITHOUT_PURCHASE = 21010;

    private $appleSharedSecret;
    private $endpointUrl;
    private $appleReceiptData;

    private $androidBundleName;
    private $androidProductId;
    private $authConfigPath;
    private $googleTransactionData;


    public function setAppleSharedSecret($secret) {
        $this->appleSharedSecret = $secret;
        return $this;
    }

    public function setEndpointUrl($url) {
        $this->endpointUrl = $url;
        return $this;
    }

    public function setAppleReceiptData($data) {
        $this->appleReceiptData = $data;
        return $this;
    }

    public function setAndroidBundleName($bundle_name) {
        $this->androidBundleName = $bundle_name;
        return $this;
    }

    public function setAndroidProductId($product_id) {
        $this->androidProductId = $product_id;
        return $this;
    }

    public function setAuthConfigPath($config_path) {
        $this->authConfigPath = $config_path;
        return $this;
    }

    public function setGoogleTransactionData($transaction_data) {
        $this->googleTransactionData = $transaction_data;
        return $this;
    }

    public function validateAppleReceipt() {
        if(!$this->appleSharedSecret) {
            throw new \Exception('Apple shared secret is not set!');
        }

        if(!$this->endpointUrl) {
            throw new \Exception('Endpoint url is not set!');
        }

        if(!$this->appleReceiptData) {
            throw new \Exception('Apple receipt data is not set!');
        }

        $body = \Unirest\Request\Body::json([
            'receipt-data' => $this->appleReceiptData,
            'password' => $this->appleSharedSecret
        ]);
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Content-Length' => strlen($body)
        ];

        \Unirest\Request::jsonOpts(true);

        $response = \Unirest\Request::post($this->endpointUrl, $headers, $body);

        if($response->code != 200) {
            return [
                'ok' => false,
                'code' => self::CODE_CONNECTION_ERROR,
                'data' => [
                    'error' => [
                        'code' => self::CODE_CONNECTION_ERROR,
                        'message' => 'Apple validation server returned HTTP code '.$response->code
                    ]
                ]
            ];
        }

        //debug($response->body);die;

        $receipt_data = $response->body;
        if($receipt_data['status'] != 0) {
            $code = self::CODE_INVALID_PAYLOAD;
            $error_message = '';
            switch($receipt_data['status']) {
                case self::RESULT_RECEIPT_VALID_BUT_SUB_EXPIRED:
                    $code = self::CODE_PURCHASE_EXPIRED;
                    $error_message = 'Subscription expired';
                    break;
                case self::RESULT_APPSTORE_CANNOT_READ:
                    $error_message = 'The App Store could not read receipt data';
                    break;
                case self::RESULT_DATA_MALFORMED:
                    $error_message = 'The data in the receipt is malformed or missing';
                    break;
                case self::RESULT_RECEIPT_NOT_AUTHENTICATED:
                    $error_message = 'The receipt could not be authenticated';
                    break;
                case self::RESULT_SHARED_SECRET_NOT_MATCH:
                    $error_message = 'The shared secret doesn\'t match';
                    break;
                case self::RESULT_RECEIPT_SERVER_UNAVAILABLE:
                    $error_message = 'The receipt server is not currently available';
                    break;
                case self::RESULT_SANDBOX_RECEIPT_SENT_TO_PRODUCTION:
                    $error_message = 'This receipt is from the test environment, but it was sent to the production environment for verification';
                    break;
                case self::RESULT_PRODUCTION_RECEIPT_SENT_TO_SANDBOX:
                    $error_message = 'This receipt is from the production environment, but it was sent to the test environment for verification';
                    break;
                case self::RESULT_RECEIPT_WITHOUT_PURCHASE:
                    $error_message = 'This receipt could not be authorized';
                    break;
            }

            return [
                'ok' => false,
                'code' => $code,
                'data' => [
                    'code' => $code,
                    'error' => [
                        'message' => $error_message
                    ]
                ]
            ];
        }

        $isValid = true;
        $latest_transaction = null;
        $latest_receipt = isset($receipt_data['latest_receipt']) && $receipt_data['latest_receipt'] ? $receipt_data['latest_receipt'] : null;

        if(isset($receipt_data['receipt']['in_app']) && $receipt_data['receipt']['in_app']) {
            $max_expiry_date_ms = 0;
            foreach($receipt_data['receipt']['in_app'] as $purchase) {
                if($purchase['expires_date_ms'] > $max_expiry_date_ms) {
                    $latest_transaction = $purchase;
                    $max_expiry_date_ms = $purchase['expires_date_ms'];
                }
            }

            $latest_transaction['type'] = 'ios-appstore';
        }

        $code = 0;
        $data = [
            'transaction' => $latest_transaction,
            'latest_receipt' => $latest_receipt
        ];

        if($latest_transaction && ($max_expiry_date_ms/1000) <= time()) {
            $isValid = false;
            $code = self::CODE_PURCHASE_EXPIRED;
            $data['error'] = [
                'message' => 'Transaction has expired '.$latest_transaction['expires_date']
            ];
        }

        $data['code'] = $code;

        return [
            'ok' => $isValid,
            'code' => $code,
            'data' => $data
        ];
    }

    public function validateGoogleReceipt() {
        if(!$this->authConfigPath) {
            throw new \Exception('Google Auth Config Path is not set!');
        }

        if(!$this->androidBundleName) {
            throw new \Exception('Android Bundle Name is not set!');
        }

        if(!$this->androidProductId) {
            throw new \Exception('Android Product Id is not set!');
        }

        if(!$this->googleTransactionData || empty($this->googleTransactionData['purchaseToken'])) {
            throw new \Exception('Google transaction data is not set or doesn\'t contain purchase token!');
        }

        $googleClient = new \Google_Client();
        $googleClient->setScopes([\Google_Service_AndroidPublisher::ANDROIDPUBLISHER]);
        $googleClient->setApplicationName('FlavarReceiptsValidator');
        $googleClient->setAuthConfig($this->authConfigPath);

        $googleAndroidPublisher = new \Google_Service_AndroidPublisher($googleClient);
        $validator = new \ReceiptValidator\GooglePlay\Validator($googleAndroidPublisher);

        try
        {
            $response = $validator->setPackageName($this->androidBundleName)
                ->setProductId($this->androidProductId)
                ->setPurchaseToken($this->googleTransactionData['purchaseToken'])
                ->validateSubscription();


            $isValid = true;
            $code = 0;
            $data = [
                'transaction' => (array) $response->getRawResponse()
            ];
            $data['transaction']['type'] = 'android-appstore';

            $expiry_timestamp = round($response->getExpiryTimeMillis() / 1000);
            if($expiry_timestamp <= time()) {
                $expiry_date = new \DateTime('@'.$expiry_timestamp);
                $isValid = false;
                $code = self::CODE_PURCHASE_EXPIRED;
                $data['error'] = [
                    'message' => 'Transaction has expired '.$expiry_date->format('Y-m-d H:i:s e')
                ];
            }


            $data['code'] = $code;
            return [
                'ok' => $isValid,
                'code' => $code,
                'data' => $data
            ];
        }
        catch (\Exception $e)
        {
            $this->log('[AndroidReceiptsValidation] Cannot validate receipt: '.$e->getMessage());
            return [
                'ok' => false,
                'code' => self::CODE_INVALID_PAYLOAD,
                'data' => [
                    'code' => self::CODE_INVALID_PAYLOAD,
                    'error' => [
                        'message' => $e->getMessage()
                    ]
                ]
            ];
        }
    }
}