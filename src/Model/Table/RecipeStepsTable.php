<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RecipeSteps Model
 *
 * @property \App\Model\Table\RecipesTable|\Cake\ORM\Association\BelongsTo $Recipes
 * @property \App\Model\Table\UtensilsTable|\Cake\ORM\Association\BelongsTo $Utensils
 *
 * @method \App\Model\Entity\RecipeStep get($primaryKey, $options = [])
 * @method \App\Model\Entity\RecipeStep newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RecipeStep[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RecipeStep|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RecipeStep|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RecipeStep patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RecipeStep[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RecipeStep findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RecipeStepsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('recipe_steps');
        $this->setDisplayField('recipe_step_id');
        $this->setPrimaryKey('recipe_step_id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Recipes', [
            'foreignKey' => 'recipe_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Utensils', [
            'foreignKey' => 'utensil_id',
        ]);
        $this->belongsTo('RecipeUtensils', [
            'foreignKey' => 'recipe_utensil_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('recipe_step_id')
            ->allowEmptyString('recipe_step_id', 'create');

        $validator
            ->integer('order_number')
            ->allowEmptyString('order_number');

        $validator
            ->scalar('step_description')
            ->maxLength('step_description', 2500)
            ->requirePresence('step_description', 'create')
            ->allowEmptyString('step_description', false);

        $validator
            ->boolean('has_timer')
            ->requirePresence('has_timer', 'create')
            ->allowEmptyString('has_timer', false);

        $validator
            ->scalar('timer_time')
            ->maxLength('timer_time', 8)
            ->requirePresence('timer_time', 'create')
            ->allowEmptyString('timer_time', true);

        $validator
            ->boolean('must_complete_time')
            ->requirePresence('must_complete_time', 'create')
            ->allowEmptyString('must_complete_time', false);

        $validator
            ->boolean('is_mise_en_place')
            ->requirePresence('is_mise_en_place', 'create')
            ->allowEmptyString('is_mise_en_place', false);

        $validator
            ->scalar('video_link')
            ->maxLength('video_link', 2500)
            ->allowEmptyString('video_link');

        $validator
            ->scalar('tip_description')
            ->maxLength('tip_description', 2500)
            ->allowEmptyString('tip_description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['recipe_id'], 'Recipes'));
        $rules->add($rules->existsIn(['utensil_id'], 'Utensils'));

        return $rules;
    }
}
