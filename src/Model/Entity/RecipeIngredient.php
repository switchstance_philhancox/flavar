<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RecipeIngredient Entity
 *
 * @property int $recipe_ingredient_id
 * @property int $recipe_id
 * @property float $quantity
 * @property integer $quantity_type_id
 * @property string $ingredient_name
 * @property float $quantity_per_additional_serving
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Recipe $recipe
 * @property QuantityType $quantity_type
 */
class RecipeIngredient extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];
}
