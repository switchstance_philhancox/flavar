<?php
/**
 * @var \App\View\AppView $this
 * @var App\Model\Entity\Utensil[]|\Cake\Collection\CollectionInterface $utensils */
?>

<!-- Begin Page Header-->
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title"><?= __('Utensils') ?></h2>
            <div>
                <a class="btn btn-square btn-outline-primary" href="<?= $this->Url->build(['controller' => 'Utensils', 'action' => 'add']) ?>">
                    <?= __('Add New Utensil') ?>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header -->


<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <!--
            <div class="widget-header bordered no-actions d-flex align-items-center">
            </div>
            -->
            <div class="widget-body">
                <?php if (count($utensils) > 0) : ?>
                <div class="table-responsive">
                    <div id="sorting-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div id="sorting-table_filter" class="dataTables_filter">
                                    <form method="get" action="#">
                                        <label><?= __('Search:') ?><input type="search" class="form-control form-control-sm" placeholder="" name="search_term" aria-controls="sorting-table" value="<?= $search_term ?>"></label>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <table class="table mb-0 dataTable table-hover">
                                    <thead>
                                    <tr>
                                        <?php
                                        if ($this->request->getQuery('sort') == 'utensil_name') :
                                            if ($this->request->getQuery('direction') == 'asc') :
                                                $sort_class = 'sorting_asc';
                                            elseif ($this->request->getQuery('direction') == 'desc') :
                                                $sort_class = 'sorting_desc';
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                        else :
                                            $sort_class = 'both';
                                        endif;
                                        ?>
                                        <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('utensil_name') ?></th>
                                        
                                        <?php
                                        if ($this->request->getQuery('sort') == 'image_id') :
                                            if ($this->request->getQuery('direction') == 'asc') :
                                                $sort_class = 'sorting_asc';
                                            elseif ($this->request->getQuery('direction') == 'desc') :
                                                $sort_class = 'sorting_desc';
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                        else :
                                            $sort_class = 'both';
                                        endif;
                                        ?>
                                        <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('image_id') ?></th>

                                        <th scope="col" class="actions"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($utensils as $key => $utensil): ?>
                                    <tr class="entity-row-<?= $key ?>">
                                        <td><?= h($utensil->utensil_name) ?></td>
                                        <td><?= $utensil->has('image') ? '<img src="' . $this->Url->build('/', true) . $utensil->image->image_url . '" alt="' . $utensil->image->alt_text . '" style="max-height: 40px;">' : '' ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $utensil->utensil_id], ['class' => 'btn btn-info btn-square btn-sm']) ?>
                                            <btn class="delete-entity btn btn-sm btn-square btn-danger" data-key="<?= $key ?>" data-csrf="<?= $this->request->getParam('_csrfToken') ?>" data-id="<?= $utensil->utensil_id ?>" data-delete-url="<?= $this->Url->build(['action' => 'delete', '_ext' => 'json']); ?>"><?= __('Delete') ?></btn>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                <div class="dataTables_info" role="status" aria-live="polite">
                                    <?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_paginate paging_simple_numbers">
                                    <ul class="pagination">
                                        <?= $this->Paginator->first() ?>
                                        <?= $this->Paginator->prev() ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next() ?>
                                        <?= $this->Paginator->last() ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php else : ?>
                    <h3 class="text-center"><?= __('There are no records yet. Click here to add one.') ?></h3>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>