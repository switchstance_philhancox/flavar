<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RecipeUtensilsFixture
 *
 */
class RecipeUtensilsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'recipe_utensil_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'recipe_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'quantity' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'utensil_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'recipe_id' => ['type' => 'index', 'columns' => ['recipe_id'], 'length' => []],
            'utensil_id' => ['type' => 'index', 'columns' => ['utensil_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['recipe_utensil_id'], 'length' => []],
            'recipe_utensils_ibfk_1' => ['type' => 'foreign', 'columns' => ['recipe_id'], 'references' => ['recipes', 'recipe_id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'recipe_utensils_ibfk_2' => ['type' => 'foreign', 'columns' => ['utensil_id'], 'references' => ['utensils', 'utensil_id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'recipe_utensil_id' => 1,
                'recipe_id' => 1,
                'quantity' => 1,
                'utensil_id' => 1
            ],
        ];
        parent::init();
    }
}
