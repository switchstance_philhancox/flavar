<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RecipeStepsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RecipeStepsTable Test Case
 */
class RecipeStepsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RecipeStepsTable
     */
    public $RecipeSteps;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.RecipeSteps',
        'app.Recipes',
        'app.Utensils'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RecipeSteps') ? [] : ['className' => RecipeStepsTable::class];
        $this->RecipeSteps = TableRegistry::getTableLocator()->get('RecipeSteps', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RecipeSteps);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
