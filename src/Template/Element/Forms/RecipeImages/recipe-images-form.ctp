<?php
/**
 * @var \App\View\AppView $this
 * @var string $method
 * @var App\Model\Entity\RecipeImage $recipe_image
 * @var \App\Model\Entity\Recipe $recipe
 */
?>

<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h2><?= $method == 'add' ? __('Add New Recipe Image') : __('Edit Recipe Image') ?></h2>
            </div>
            <div class="widget-body">
                <?= $this->Form->create($recipe_image, ['type' => 'file']); ?>

                <?= $this->Form->control('recipe_image_id') ?>
                <?= $this->Form->control('recipe_id', ['type' => 'hidden', 'value' => $recipe->recipe_id]) ?>

                <div class="form-group row d-flex mb-5 required">
                    <label class="col-lg-4 form-control-label d-flex justify-content-lg-end"
                           for="content"><?= __('Image') ?></label>

                    <div class="col-md-6 col-sm-12">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-primary btn-file m-r-10">
                                <span class="fileinput-new"><?= $method == 'add' ? __('Select image') : __('Select new image') ?></span>
                                <input type="file" name="image_file" class="file">
                            </span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                        </div>
                    </div>
                </div>


                <div class="text-right">
                    <input class="btn btn-primary btn-square" type="submit" value="<?= __('Save Image') ?>">
                </div>

                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>