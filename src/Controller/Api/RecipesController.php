<?php

namespace App\Controller\Api;

use App\Model\Entity\RecipeDifficultyRating;
use Cake\ORM\Query;

/**
 * Recipes Controller
 *
 * @property \App\Model\Table\RecipesTable           $Recipes
 */
class RecipesController extends AppController
{

    private $itemsPerPage = 6;

    public function items($category_id = null)
    {
        if(!$this->request->is('post'))
        {
            echo $this->getJsonErrorResponse(400, 'Invalid request type');
            die;
        }

        if(!$category_id)
        {
            echo $this->getJsonErrorResponse(400, 'Please specify recipes category');
            die;
        }

        $search_term = $this->request->getData('search');
        $page_number = intval($this->request->getData('page'));
        if(!$page_number)
        {
            $page_number = 1;
        }

        $conditions = [
            'Recipes.category_id' => $category_id,
            'Recipes.is_live' => true,
            'Recipes.is_deleted' => false
        ];

        if($search_term)
        {
            $conditions['OR'] = [
                [
                    'Recipes.recipe_title LIKE' => '%'.$search_term.'%'
                ],
                [
                    'Recipes.description LIKE' => '%'.$search_term.'%'
                ]
            ];
        }

        $recipes = $this->Recipes->find()
            ->select([
                'Recipes.recipe_id',
                'Recipes.recipe_title',
                'Recipes.subscriber_level',
                'Recipes.difficulty_level'
            ])
            ->where($conditions)
            ->contain([
                'Images' => function (Query $q) {
                    return $q->select([
                        'Images.image_url'
                    ]);
                }
            ])
            ->order(['Recipes.recipe_id ASC'])
            ->limit($this->itemsPerPage)
            ->page($page_number);

        echo json_encode([
            'recipes' => $recipes
        ]);
        die;
    }

    public function search()
    {
        if(!$this->request->is('post'))
        {
            echo $this->getJsonErrorResponse(400, 'Invalid request type');
            die;
        }

        $category_ids = $this->request->getData('category_ids');

        if(!$category_ids)
        {
            echo $this->getJsonErrorResponse(400, 'Please specify categories');
            die;
        }

        $search_term = $this->request->getData('search');
        $conditions = [
            'Recipes.is_live' => true,
            'Recipes.is_deleted' => false
        ];

        if($search_term)
        {
            $conditions['OR'] = [
                [
                    'Recipes.recipe_title LIKE' => '%'.$search_term.'%'
                ],
                [
                    'Recipes.description LIKE' => '%'.$search_term.'%'
                ]
            ];
        }

        $recipes = [];
        foreach($category_ids as $category_id)
        {
            $conditions['Recipes.category_id'] = $category_id;
            $results = $this->Recipes->find()
                ->select([
                    'Recipes.recipe_id',
                    'Recipes.recipe_title',
                    'Recipes.subscriber_level',
                    'Recipes.difficulty_level'
                ])
                ->where($conditions)
                ->contain([
                    'Images' => function (Query $q) {
                        return $q->select([
                            'Images.image_url'
                        ]);
                    }
                ])
                ->order(['Recipes.recipe_id ASC'])
                ->limit($this->itemsPerPage)
                ->page(1)
                ->toArray();

            if(!empty($results)) {
                $recipes[$category_id] = $results;
            }
        }

        echo json_encode([
            'recipes' => $recipes
        ]);
        die;
    }

    public function viewModel($recipe_id = null)
    {
        $recipe = $this->Recipes->find()
            ->select([
                'Recipes.recipe_id',
                'Recipes.ar_model_id'
            ])
            ->where([
                'Recipes.recipe_id' => $recipe_id,
                'Recipes.is_live' => true,
                'Recipes.is_deleted' => false
            ])
            ->contain([
                'ArModels' => function (Query $q) {
                    return $q->select([
                        'ArModels.ar_model_id',
                        'ArModels.ar_model_file_name',
                        'ArModels.ar_model_zip_file_name',
                        'ArModels.ar_model_url',
                        'ArModels.ar_model_file_size'
                    ]);
                }
            ])
            ->first();

        if(empty($recipe))
        {
            echo $this->getJsonErrorResponse(400, 'Such recipe does not exist');
            die;
        }

        if(empty($recipe->ar_model))
        {
            echo $this->getJsonErrorResponse(400, '3D model for this recipe cannot be found');
            die;
        }

        echo json_encode([
            'recipe_model' => $recipe->ar_model
        ]);
        die;
    }

    public function view($recipe_id = null)
    {
        $recipe = $this->Recipes->find()
            ->select([
                'Recipes.recipe_id',
                'Recipes.recipe_title',
                'Recipes.subscriber_level',
                'Recipes.description',
                'Recipes.ar_model_id',
                'Recipes.featured_image_id',
                'Recipes.difficulty_level',
                'Recipes.default_number_of_servings'
            ])
            ->where([
                'Recipes.recipe_id' => $recipe_id,
                'Recipes.is_live' => true,
                'Recipes.is_deleted' => false
            ])
            ->contain([
                'Images' => function (Query $q) {
                    return $q->select([
                        'Images.image_url'
                    ]);
                },
                'RecipeImages' => function (Query $q) {
                    return $q->select([
                        'RecipeImages.recipe_id',
                        'RecipeImages.image_id',
                        'Images.image_url'
                    ])->contain(['Images']);
                },
                'RecipeIngredients' => function (Query $q) {
                    return $q->select([
                        'RecipeIngredients.recipe_id',
                        'RecipeIngredients.quantity',
                        'RecipeIngredients.ingredient_name',
                        'QuantityTypes.quantity_type_name'
                    ])->contain(['QuantityTypes']);
                },
                'RecipeUtensils' => function (Query $q) {
                    return $q->select([
                        'RecipeUtensils.recipe_id',
                        'RecipeUtensils.utensil_id',
                        'RecipeUtensils.quantity',
                        'Utensils.utensil_name'
                    ])->contain(['Utensils']);
                },
                'RecipeTags' => function (Query $q) {
                    return $q->select([
                        'RecipeTags.recipe_id',
                        'RecipeTags.tag_id',
                        'Tags.tag_name'
                    ])->contain(['Tags']);
                },
                'RecipeSteps' => function (Query $q) {
                    return $q->select([
                        'RecipeSteps.recipe_id',
                        'RecipeSteps.recipe_step_id'
                    ])->where(['RecipeSteps.is_mise_en_place' => true]);
                }
            ])
            ->first();

        if(empty($recipe))
        {
            echo $this->getJsonErrorResponse(400, 'Such recipe does not exist');
            die;
        }

        //debug($recipe);die;

        if($recipe->recipe_steps)
        {
            $recipe->hasMiseEnPlaceSteps = true;
        }
        else
        {
            $recipe->hasMiseEnPlaceSteps = false;
        }
        unset($recipe->recipe_steps);

        if($recipe->recipe_utensils)
        {
            $recipe_utensil_list = [];

            foreach ($recipe->recipe_utensils as $recipe_utensil)
            {
                $recipe_utensil_list[$recipe_utensil->utensil_id] = [
                    'quantity' => isset($recipe_utensil_list[$recipe_utensil->utensil_id]) ? ($recipe_utensil_list[$recipe_utensil->utensil_id]['quantity'] + 1) : 1,
                    'name' => $recipe_utensil->utensil->utensil_name,
                ];
            }

            $recipe->recipe_utensils = array_values($recipe_utensil_list);
        }

        if($recipe->recipe_tags)
        {
            $tag_ids = [];
            foreach($recipe->recipe_tags as $tag)
            {
                $tag_ids[] = $tag->tag_id;
            }

            $recipe->related_recipes = $this->Recipes->find()
                ->select([
                    'Recipes.recipe_id',
                    'Recipes.recipe_title',
                    'Recipes.subscriber_level',
                    'Recipes.difficulty_level'
                ])
                ->where([
                    'Recipes.recipe_id != ' => $recipe_id,
                    'Recipes.is_live' => true,
                    'Recipes.is_deleted' => false
                ])
                ->matching('RecipeTags', function ($q) use ($tag_ids) {
                    return $q->where(['RecipeTags.tag_id IN' => $tag_ids]);
                })
                ->contain([
                    'Images' => function (Query $q) {
                        return $q->select([
                            'Images.image_url'
                        ]);
                    }
                ])
                ->distinct(['Recipes.recipe_id'])
                ->order(['Recipes.recipe_id ASC'])
                ->limit($this->itemsPerPage);
        }

        echo json_encode([
            'recipe' => $recipe
        ]);
        die;
    }


    public function getCookingInfo($recipe_id = null)
    {
        $user_id = $this->currentUserId;
        $recipe = $this->Recipes->find()
            ->select([
                'Recipes.recipe_id',
                'Recipes.subscriber_level',
                'Recipes.ar_model_id'
            ])
            ->where([
                'Recipes.recipe_id' => $recipe_id,
                'Recipes.is_live' => true,
                'Recipes.is_deleted' => false
            ])
            ->contain([
                'RecipeSteps' => function (Query $q) {
                    return $q->select([
                        'RecipeSteps.recipe_step_id',
                        'RecipeSteps.recipe_id',
                        'RecipeSteps.order_number',
                        'RecipeSteps.step_description',
                        'RecipeSteps.has_timer',
                        'RecipeSteps.timer_time',
                        'RecipeSteps.must_complete_time',
                        'RecipeSteps.is_mise_en_place',
                        'RecipeSteps.recipe_utensil_id',
                        'RecipeSteps.video_link',
                        'RecipeSteps.tip_description'
                    ])->contain([
                        'RecipeUtensils' => function (Query $q) {
                            return $q
                                ->select([
                                    'RecipeUtensils.recipe_utensil_id',
                                    'RecipeUtensils.utensil_id',
                                    'RecipeUtensils.internal_name',
                                    'Utensils.utensil_name',
                                    'Images.image_url'
                                ])
                                ->contain(['Utensils.Images']);
                        }
                    ])
                    ->order(['RecipeSteps.order_number ASC']);
                },
                'RecipePlatingDescriptions' => function (Query $q) {
                    return $q->select([
                        'RecipePlatingDescriptions.plating_description',
                        'Images.image_url'
                    ])->contain(['Images']);
                },
                'RecipeDifficultyRatings' => function (Query $q) use ($user_id) {
                    return $q->where(['RecipeDifficultyRatings.user_id' => $user_id]);
                },
                'RecipeRatings' => function (Query $q) use ($user_id) {
                    return $q->where(['RecipeRatings.user_id' => $user_id]);
                }
            ])
            ->first();

        if(empty($recipe))
        {
            echo $this->getJsonErrorResponse(400, 'Such recipe does not exist');
            die;
        }

        //debug($recipe);die;

        echo json_encode([
            'recipe' => $recipe
        ]);
        die;
    }

    public function rateDifficulty($id = null)
    {

        if ($this->request->is('post'))
        {
            if(!$id)
            {
                echo $this->getJsonErrorResponse(400, 'Please specify recipe identifier');
                die;
            }

            $difficult_rating = intval($this->request->getData('difficult_rating'));

            if(!in_array($difficult_rating, [RecipeDifficultyRating::RATING_EASY, RecipeDifficultyRating::RATING_MEDIUM, RecipeDifficultyRating::RATING_DIFFICULT])) {
                echo $this->getJsonErrorResponse(400, 'Please provide valid value for difficult_rating parameter');
                die;
            }

            $user_id = $this->currentUserId;
            $recipe = $this->Recipes->find()
                ->select([
                    'Recipes.recipe_id'
                ])
                ->where([
                    'Recipes.recipe_id' => $id
                ])
                ->contain(
                    [
                        'RecipeDifficultyRatings' => function (Query $q) use ($user_id) {
                            return $q->where(['RecipeDifficultyRatings.user_id' => $user_id]);
                        }
                    ]
                )->first();

            if(empty($recipe)) {
                echo $this->getJsonErrorResponse(400, 'Such recipe does not exist');
                die;
            }

            if($recipe->recipe_difficulty_ratings) {
                echo $this->getJsonErrorResponse(400, 'Recipe difficulty can be rated just once');
                die;
            }

            $recipe_difficulty_rating = $this->Recipes->RecipeDifficultyRatings->newEntity([
                'user_id' => $this->currentUserId,
                'recipe_id' => $id,
                'difficult_rating' => $difficult_rating
            ]);

            if($this->Recipes->RecipeDifficultyRatings->save($recipe_difficulty_rating))
            {
                echo json_encode([
                    'success' => true
                ]);
                die;
            }
            else
            {
                echo $this->getJsonErrorResponse(400, $this->formatValidationErrors($recipe_difficulty_rating->getErrors()));
                die;
            }
        }
        else
        {
            echo $this->getJsonErrorResponse(400, 'Invalid request type');
            die;
        }
    }

    public function rate($id = null)
    {
        if(!$id)
        {
            echo $this->getJsonErrorResponse(400, 'Please specify recipe identifier');
            die;
        }

        $user_id = $this->currentUserId;
        $recipe = $this->Recipes->find()
            ->select([
                'Recipes.recipe_id',
                'Recipes.recipe_title'
            ])
            ->where([
                'Recipes.recipe_id' => $id
            ])
            ->contain(
                [
                    'RecipeRatings' => function (Query $q) use ($user_id) {
                        return $q->where(['RecipeRatings.user_id' => $user_id]);
                    }
                ]
            )->first();

        if(empty($recipe)) {
            echo $this->getJsonErrorResponse(400, 'Such recipe does not exist');
            die;
        }

        if($recipe->recipe_ratings) {
            echo $this->getJsonErrorResponse(400, 'We\'re sorry, recipe can be rated just once!');
            die;
        }


        if ($this->request->is('post'))
        {
            $rating = intval($this->request->getData('difficult_rating'));

            if($rating < 1 || $rating > 5) {
                echo $this->getJsonErrorResponse(400, 'Please provide valid value for rating parameter');
                die;
            }

            $recipe_rating = $this->Recipes->RecipeRatings->newEntity([
                'user_id' => $this->currentUserId,
                'recipe_id' => $id,
                'rating' => $rating
            ]);

            if($this->Recipes->RecipeRatings->save($recipe_rating))
            {
                echo json_encode([
                    'success' => true
                ]);
                die;
            }
            else
            {
                echo $this->getJsonErrorResponse(400, $this->formatValidationErrors($recipe_rating->getErrors()));
                die;
            }
        }
        else
        {
            echo json_encode([
                'recipe' => $recipe
            ]);
            die;
        }
    }

}
