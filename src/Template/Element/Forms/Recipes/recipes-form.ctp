<?php
use App\Model\Entity\Recipe;
/**
 * @var \App\View\AppView $this
 * @var string $method
 * @var App\Model\Entity\Recipe $recipe
 * @var \App\Model\Entity\Tag[] $tags
 */
?>

<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h2><?= $method == 'add' ? __('Add New Recipe') : __('Edit Recipe') ?></h2>
            </div>
            <div class="widget-body">
                <?= $this->Form->create($recipe, ['type' => 'file']); ?>

                <?= $this->Form->control('recipe_id') ?>
                <?= $this->Form->control('recipe_title') ?>
                <?= $this->Form->control('description', ['type' => 'textarea']) ?>
                <?= $this->Form->control('total_time_estimate', ['label' => __('Estimated Time')]) ?>

                <div class="form-group row d-flex align-items-center mb-5 required">
                    <label class="col-lg-4 form-control-label d-flex justify-content-lg-end" for="subscriber-level"><?= __('Status') ?></label>
                    <?php if ($method == 'add' || count($recipe->recipe_steps) == 0) : ?>
                    <div class="col-lg-8">
                        <?= __('<strong>Draft</strong> (A recipe can not be put live until it has at least one step.)') ?>
                    </div>
                    <?= $this->Form->control('is_live', ['type' => 'hidden', 'value' => '0']) ?>
                    <?php else : ?>
                    <div class="col-lg-1">
                        <div class="custom-control custom-radio styled-radio mb-3">
                            <input value="1" class="custom-control-input" type="radio" name="is_live" id="is-live-true" required="required"<?= $recipe->is_live ? ' checked="checked"' : '' ?>>
                            <label class="custom-control-descfeedback" for="is-live-true"><?= __('Live') ?></label>
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="custom-control custom-radio styled-radio mb-3">
                            <input value="0" class="custom-control-input" type="radio" name="is_live" id="is-live-false" required="required"<?= !$recipe->is_live ? ' checked="checked"' : '' ?>>
                            <label class="custom-control-descfeedback" for="is-live-false"><?= __('Draft') ?></label>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>

                <div class="form-group row d-flex align-items-center mb-5 required">
                    <label class="col-lg-4 form-control-label d-flex justify-content-lg-end" for="subscriber-level"><?= __('Subscriber Level') ?></label>
                    <div class="col-lg-1">
                        <div class="custom-control custom-radio styled-radio mb-3">
                            <input value="<?= Recipe::SUBSCRIBER_LEVEL_FREE ?>" class="custom-control-input" type="radio" name="subscriber_level" id="subscriber-level-free" required="required"<?= $recipe->subscriber_level == Recipe::SUBSCRIBER_LEVEL_FREE ? ' checked="checked"' : '' ?>>
                            <label class="custom-control-descfeedback" for="subscriber-level-free"><?= $this->Util->returnSubscriberLevel(Recipe::SUBSCRIBER_LEVEL_FREE) ?></label>
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="custom-control custom-radio styled-radio mb-3">
                            <input value="<?= Recipe::SUBSCRIBER_LEVEL_PREMIUM ?>" class="custom-control-input" type="radio" name="subscriber_level" id="subscriber-level-premium" required="required"<?= $recipe->subscriber_level == Recipe::SUBSCRIBER_LEVEL_PREMIUM ? ' checked="checked"' : '' ?>>
                            <label class="custom-control-descfeedback" for="subscriber-level-premium"><?= $this->Util->returnSubscriberLevel(Recipe::SUBSCRIBER_LEVEL_PREMIUM) ?></label>
                        </div>
                    </div>
                </div>

                <div class="form-group row d-flex mb-5 required">
                    <label class="col-lg-4 form-control-label d-flex justify-content-lg-end"
                           for="content"><?= __('AR Model') ?></label>

                    <div class="col-md-6 col-sm-12">
                        <?php if ($recipe->ar_model != null): ?>
                            <div class="fileinput fileinput-new">
                                <div>
                                    <div class="fileinput-preview thumbnail pr-2 pb-2">
                                        <p><?= $recipe->ar_model->ar_model_url .' ('.$this->Util->formatFileSize($recipe->ar_model->ar_model_file_size).')' ?></p>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-danger removeImage waves-effect"
                                   data-remove="model_file"><?= __('Remove') ?></a>
                            </div>
                            <input type="hidden" name="remove_model_file" id="remove_model_file">
                            <br>
                        <?php endif; ?>

                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-primary btn-file m-r-10">
                                <span class="fileinput-new"><?= $method == 'add' ? __('Select model') : __('Select new model') ?></span>
                                <input type="file" name="model_file" class="file">
                            </span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                        </div>
                    </div>
                </div>

                <div class="form-group row d-flex mb-5 required">
                    <label class="col-lg-4 form-control-label d-flex justify-content-lg-end"
                           for="content"><?= __('Featured Image') ?></label>
                    <input type="hidden" name="featured_image_id"
                           value="<?= isset($recipe->image) ? $recipe->image->image_id : null ?>">

                    <div class="col-md-6 col-sm-12">
                        <?php if ($recipe->image !== null): ?>
                            <div class="fileinput fileinput-new">
                                <div>
                                    <div class="fileinput-preview thumbnail pr-2 pb-2">
                                        <img src="<?= $this->Url->build('/', true) . $recipe->image->image_url ?>" style="max-height: 100px;">
                                    </div>
                                </div>
                                <a href="#" class="btn btn-danger removeImage waves-effect"
                                   data-remove="image_file"><?= __('Remove') ?></a>
                            </div>
                            <input type="hidden" name="remove_image_file" id="remove_image_file">
                            <br>
                        <?php endif; ?>

                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-primary btn-file m-r-10">
                                <span class="fileinput-new"><?= $method == 'add' ? __('Select image') : __('Select new image') ?></span>
                                <input type="file" name="image_file" class="file">
                            </span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                        </div>
                    </div>
                </div>

                <div class="form-group row d-flex align-items-center mb-5 required">
                    <label class="col-lg-4 form-control-label d-flex justify-content-lg-end" for="subscriber-level"><?= __('Subscriber Level') ?></label>
                    <div class="col-lg-1">
                        <div class="custom-control custom-radio styled-radio mb-3">
                            <input value="<?= Recipe::DIFFICULTY_LEVEL_EASY ?>" class="custom-control-input" type="radio" name="difficulty_level" id="difficulty-easy" required="required"<?= $recipe->difficulty_level == Recipe::DIFFICULTY_LEVEL_EASY ? ' checked="checked"' : '' ?>>
                            <label class="custom-control-descfeedback" for="difficulty-easy"><?= $this->Util->returnDifficulty(Recipe::DIFFICULTY_LEVEL_EASY) ?></label>
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="custom-control custom-radio styled-radio mb-3">
                            <input value="<?= Recipe::DIFFICULTY_LEVEL_MEDIUM ?>" class="custom-control-input" type="radio" name="difficulty_level" id="difficulty-medium" required="required"<?= $recipe->difficulty_level == Recipe::DIFFICULTY_LEVEL_MEDIUM ? ' checked="checked"' : '' ?>>
                            <label class="custom-control-descfeedback" for="difficulty-medium"><?= $this->Util->returnDifficulty(Recipe::DIFFICULTY_LEVEL_MEDIUM) ?></label>
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="custom-control custom-radio styled-radio mb-3">
                            <input value="<?= Recipe::DIFFICULTY_LEVEL_HARD ?>" class="custom-control-input" type="radio" name="difficulty_level" id="difficulty-hard" required="required"<?= $recipe->difficulty_level == Recipe::DIFFICULTY_LEVEL_HARD ? ' checked="checked"' : '' ?>>
                            <label class="custom-control-descfeedback" for="difficulty-hard"><?= $this->Util->returnDifficulty(Recipe::DIFFICULTY_LEVEL_HARD) ?></label>
                        </div>
                    </div>
                </div>

                <?= $this->Form->control('default_number_of_servings', ['min' => 1]) ?>

                <div class="form-group row d-flex align-items-center mb-5 required">
                    <label class="col-lg-4 form-control-label d-flex justify-content-lg-end" for="can-servings-be-adjusted"><?= __('Can servings be adjusted?') ?></label>
                    <div class="col-lg-1">
                        <div class="custom-control custom-radio styled-radio mb-3">
                            <input value="1" class="custom-control-input" type="radio" name="can_servings_be_adjusted" id="can-servings-be-adjusted-yes" required="required"<?= $recipe->can_servings_be_adjusted || $method == 'add' ? ' checked="checked"' : '' ?>>
                            <label class="custom-control-descfeedback" for="can-servings-be-adjusted-yes"><?= __('Yes') ?></label>
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="custom-control custom-radio styled-radio mb-3">
                            <input value="0" class="custom-control-input" type="radio" name="can_servings_be_adjusted" id="can-servings-be-adjusted-no" required="required"<?= !$recipe->can_servings_be_adjusted ? ' checked="checked"' : '' ?>>
                            <label class="custom-control-descfeedback" for="can-servings-be-adjusted-no"><?= __('No') ?></label>
                        </div>
                    </div>
                </div>

                <?= $this->Form->control('category_id') ?>

                <div class="form-group row d-flex align-items-center mb-5 required">
                    <label class="col-lg-4 form-control-label d-flex justify-content-lg-end" for="recipe_tags"><?= __('Tags') ?></label>
                    <div class="col-lg-8">
                        <?php $recipe_tags = [];
                        if ($recipe->recipe_tags && count($recipe->recipe_tags) > 0) :
                            foreach ($recipe->recipe_tags as $recipe_tag) :
                                $recipe_tags[] = $recipe_tag->tag_id;
                            endforeach;
                        endif; ?>
                        <select class="form-control selectpicker" name="tags[]" id="recipe_tags" multiple="multiple">
                            <?php foreach ($tags as $tag) : ?>
                                <option value="<?= $tag->tag_id ?>"<?= in_array($tag->tag_id, $recipe_tags) ? ' selected="selected"' : '' ?>><?= $tag->tag_name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>



                <div class="text-right">
                    <input class="btn btn-primary btn-square" type="submit" value="<?= __('Save Recipe') ?>">
                </div>

                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>