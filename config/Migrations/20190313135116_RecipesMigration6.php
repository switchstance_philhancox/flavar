<?php
use Migrations\AbstractMigration;

class RecipesMigration6 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('recipes');
        $table
            ->addColumn('ar_model_id', 'integer', ['limit' => 11, 'signed' => false, 'null' => true, 'after' => 'description'])
            ->addForeignKey('ar_model_id', 'ar_models', 'ar_model_id', ['update' => 'CASCADE', 'delete' => 'CASCADE'])
            ->update();
    }
}
