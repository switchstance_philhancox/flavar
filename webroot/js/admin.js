$(function () {
    if ($('[data-toggle="tooltip"]').length > 0) {
        $('[data-toggle="tooltip"]').tooltip();
    }

    $(document.body).on('click', '.removeImage', function (e) {
        e.preventDefault();
        var delete_id = $(this).attr('data-remove');
        $('#remove_' + delete_id).val(1);
        $(this).parent('.fileinput').hide();
    });

    $(document.body).on('change', '.quantity-type-selector', function() {
        let qtySpan = $(this).data('qty-span');
        $(qtySpan).text($(this).find('option:selected').text());
    });

    $('.has-timer-select').on('change', function() {
        if ($(this).val() === '1') {
            $('.time-container').removeClass('d-none');
            $('#utensil-id').prop('required', 'required');
        } else {
            $('.time-container').addClass('d-none');
            $('#timer-time').val('');
            $('#utensil-id').prop('required', false);
        }
    });

    $('#timer-time').mask('00:50:50', {'translation': {5: {pattern: /[0-5*]/}}});

    $('.sortable-table tbody').sortable({
        update: function (event, ui) {
            var data = $(this).sortable('serialize');
            console.log(data);

            $.ajax({
                data: data,
                type: 'POST',
                url: '/admin/recipe-steps/reorder-steps.json'
            });
        }
    });

    $('.delete-entity').on('click', function() {
        var delete_url = $(this).data('delete-url');
        var item_id = $(this).data('id');
        var key = $(this).data('key');
        swal({
            title: 'Are you sure?',
            text: 'Are you sure you want to delete this?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes',
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            buttons: {
                cancel: {
                    text: 'Cancel',
                    value: null,
                    visible: false,
                    className: 'btn btn-square btn-sm btn-secondary',
                    closeModal: true,
                },
                confirm: {
                    text: 'Yes',
                    value: true,
                    visible: true,
                    className: 'btn btn-square btn-sm btn-primary',
                    closeModal: true
                }
            }
        }, function (isConfirm) {
            if (!isConfirm) return;
            $.ajax({
                url: delete_url,
                type: 'POST',
                data: {
                    id: item_id
                },
                dataType: 'html',
                success: function (data) {
                    data = JSON.parse(data);
                    if(data.output.status === true) {
                        swal('Success!', 'The item was successfully deleted.', 'success');
                        $('.entity-row-' + key).remove();
                        console.log('entity-row-' + key);
                    } else  {
                        swal('Sorry...!', 'There was a problem. Please try again.', 'error');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal('Sorry...', 'There was a problem, please try again.', 'error');
                }
            });
        });
    });

    $(document.body).on('change', '.recipe-utensil-selector', function() {
       var selectedItem = $(this).find(':selected');
       var selectedValueName = selectedItem.data('utensil-name') === '' ? '' : selectedItem.data('utensil-name');
       var selectedValue = selectedItem.val();
       var target = $(this).data('target');
       var thisUtensilCount = 0;
        $('.recipe-utensil-selector').each(function(i, obj) {
            if (selectedValue === $(this).val()) {
                thisUtensilCount++;
            }
        });
       $(target).val(selectedValueName + ' ' + thisUtensilCount);
    });

    $('#recipe-step-utensil-remove-label').on('click', function() {
        $('input[name="recipe_utensil_id"]').val('');
        $('#recipe-utensil-step-name').text('No utensil selected');
        $('#recipe-step-utensil-action-label').text('Add Utensil');
        $('#recipe-step-utensil-remove-label').addClass('d-none');
        $('#recipe-step-utensil-select').val('');
    });

    $('.add-utensil-to-step').on('click', function() {
        var selectedNewUtensil = $('#recipe-step-utensil-select option:selected');

        var utensilId, utensilName = '';

        if (selectedNewUtensil.val() !== '') {
            utensilId = selectedNewUtensil.val();
            utensilName = selectedNewUtensil.data('utensil-name');
        }

        $('input[name="recipe_utensil_id"]').val(utensilId);
        $('#recipe-utensil-step-name').text(utensilName).css('color', '#333');
        $('#recipe-step-utensil-action-label').text('Edit Utensil');
        $('#recipe-step-utensil-remove-label').removeClass('d-none');
        $('#select-utensil-modal').modal('toggle');
    });
});