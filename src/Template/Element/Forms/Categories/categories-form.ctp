<?php
/**
 * @var \App\View\AppView $this
 * @var string $method
 * @var App\Model\Entity\Category $category */
?>

    <div class="row">
        <div class="col-xl-12">
            <!-- Default -->
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h2><?= $method == 'add' ? __('Add New Category') : __('Edit Category') ?></h2>
                </div>
                <div class="widget-body">
                    <?= $this->Form->create($category); ?>

                    <?= $this->Form->control('category_id') ?>
                    <?= $this->Form->control('category_name') ?>

                    <div class="text-right">
                        <input class="btn btn-primary btn-square" type="submit" value="<?= __('Save Category') ?>">
                    </div>

                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>

