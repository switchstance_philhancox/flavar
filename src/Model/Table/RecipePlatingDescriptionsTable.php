<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RecipePlatingDescriptions Model
 *
 * @property \App\Model\Table\RecipesTable|\Cake\ORM\Association\BelongsTo $Recipes
 * @property \App\Model\Table\ImagesTable|\Cake\ORM\Association\BelongsTo $Images
 *
 * @method \App\Model\Entity\RecipePlatingDescription get($primaryKey, $options = [])
 * @method \App\Model\Entity\RecipePlatingDescription newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RecipePlatingDescription[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RecipePlatingDescription|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RecipePlatingDescription|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RecipePlatingDescription patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RecipePlatingDescription[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RecipePlatingDescription findOrCreate($search, callable $callback = null, $options = [])
 */
class RecipePlatingDescriptionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('recipe_plating_descriptions');
        $this->setDisplayField('recipe_plating_description_id');
        $this->setPrimaryKey('recipe_plating_description_id');

        $this->belongsTo('Recipes', [
            'foreignKey' => 'recipe_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Images', [
            'foreignKey' => 'plating_image_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('recipe_plating_description_id')
            ->allowEmptyString('recipe_plating_description_id', 'create');

        $validator
            ->scalar('plating_description')
            ->maxLength('plating_description', 2000)
            ->allowEmptyString('plating_description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['recipe_id'], 'Recipes'));
        $rules->add($rules->existsIn(['plating_image_id'], 'Images'));

        return $rules;
    }
}
