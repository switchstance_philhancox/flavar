<?php
/**
 * @var integer $key
 * @var \App\Model\Entity\RecipeUtensil $recipe_utensil
 * @var \App\Model\Entity\Utensil[] $utensils
 */
?>

<tr class="arr-row<?= $key === '{{counter}}' ? ' d-none' : '' ?>" data-key="<?= $key ?>">
    <input type="hidden" name="recipe_utensils[<?= $key ?>][recipe_utensil_id]" value="<?= $recipe_utensil->recipe_utensil_id ?>">
    <td>
        <select class="form-control recipe-utensil-selector" name="recipe_utensils[<?= $key ?>][utensil_id]" data-target="#recipe-utensil-internal-name-<?= $key ?>">
            <option value=""><?= __('-- Select a utensil --') ?></option>
            <?php foreach ($utensils as $utensil) : ?>
                <option data-utensil-name="<?= $utensil->utensil_name ?>" value="<?= $utensil->utensil_id ?>"<?= $utensil->utensil_id == $recipe_utensil->utensil_id ? ' selected="selected"' : '' ?>><?= $utensil->utensil_name ?></option>
            <?php endforeach; ?>
        </select>
    </td>
    <td>
        <input type="hidden" name="recipe_utensils[<?= $key ?>][quantity]" value="1">
        <input id="recipe-utensil-internal-name-<?= $key ?>" type="text" class="form-control" name="recipe_utensils[<?= $key ?>][internal_name]" value="<?= $recipe_utensil->internal_name ?? '' ?>">
    </td>
    <td>
        <a href="#" class="btn btn-danger btn-sm btn-square arr-remove-row c-white<?= $key == 0 ? ' d-none' : '' ?>" data-minimum-rows="2" data-container-id="recipe-utensils-container"><?= __('Remove') ?></a>
    </td>
</tr>