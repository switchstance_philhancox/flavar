<?php
namespace App\Controller\Admin;

use App\Model\Entity\Recipe;
use App\Model\Entity\RecipeImage;
use App\Model\Table\RecipeImagesTable;
use App\Model\Table\RecipesTable;

/**
 * @property RecipesTable $Recipes
 * @property RecipeImagesTable $RecipeImages
 *
 * Class RecipeImagesController
 * @package App\Controller\Admin
 */
class RecipeImagesController extends AppController
{
    public function add($recipe_id)
    {
        $this->loadModel('Recipes');

        /** @var Recipe $recipe */
        $recipe = $this->Recipes->find()->where(['recipe_id' => $recipe_id])->first();

        if (!$recipe)
        {
            $this->Flash->error('You cannot add an image to a recipe that doesn\'t exist.');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }

        $recipe_image = $this->RecipeImages->newEntity();

        if ($this->request->is('post'))
        {
            $error = true;
            $data = $this->request->getData();

            if ($data['image_file']['name'] != null)
            {
                $image_id = $this->File->uploadFile($data['image_file'], 'uploads' . DS . 'recipes', ['admin_id' => $this->admin_id]);

                if (is_int($image_id))
                {
                    $error = false;
                    $data['image_id'] = $image_id;
                }
            }

            if ($error === false)
            {
                $recipe_image = $this->RecipeImages->patchEntity($recipe_image, $data);

                if (!$this->RecipeImages->save($recipe_image))
                {
                    $this->Flash->error('There was a problem saving the image. Please try again.');
                    $this->log($recipe_image->getErrors());
                }
                else
                {
                    $this->Flash->success('The image was successfully added to the recipe.');
                    return $this->redirect(['controller' => 'Recipes', 'action' => 'view', $recipe->recipe_id]);
                }
            }
        }

        $this->set([
            'recipe' => $recipe,
            'recipe_image' => $recipe_image,
        ]);
    }

    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);

        if ($this->request->is('json'))
        {
            $recipe_image_id = $this->request->getData('id');

            /** @var RecipeImage $recipe_image */
            $recipe_image = $this->RecipeImages->find()->where(['recipe_image_id' => $recipe_image_id])->first();

            if (!$recipe_image)
            {
                $output = [
                    'status' => false,
                    'message' => __('The image could not be found. Please try again.'),
                ];
            }
            else
            {
                if (!$this->RecipeImages->delete($recipe_image))
                {
                    $this->log($recipe_image->getErrors());
                    $output = [
                        'status' => false,
                        'message' => __('There was a problem trying to delete the image. Please try again.'),
                    ];
                }
                else
                {
                    $output = [
                        'status' => true,
                        'message' => __('The image was successfully removed from this gallery.'),
                    ];
                }
            }

            $this->set(compact('output'));
            $this->set('_serialise', ['output']);
        }
        else
        {
            return $this->redirect('index');
        }
    }
}