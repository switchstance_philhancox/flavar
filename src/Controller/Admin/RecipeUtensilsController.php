<?php
namespace App\Controller\Admin;

use App\Model\Table\RecipesTable;
use App\Model\Table\UtensilsTable;

/**
 * RecipeUtensils Controller
 *
 * @property \App\Model\Table\RecipeUtensilsTable $RecipeUtensils
 * @property RecipesTable $Recipes
 * @property UtensilsTable $Utensils
 */
class RecipeUtensilsController extends AppController
{
    /**
     * @param null $recipe_id
     *
     * @return \Cake\Http\Response|null
     */
    public function edit($recipe_id = null)
    {
        $this->loadModel('Recipes');
        $this->loadModel('Utensils');

        $recipe = $this->Recipes->find()->where(['recipe_id' => $recipe_id])->first();

        if (!$recipe)
        {
            $this->Flash->error('You cannot add utensils to a recipe that doesn\'t exist.');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }

        $recipe_utensils = $this->RecipeUtensils->find()->where(['recipe_id' => $recipe_id])->contain(['Utensils'])->all();

        if ($this->request->is(['patch', 'post', 'put']))
        {
            $data = $this->request->getData();

            unset($data['recipe_utensils']['{{counter}}']);

            $recipe = $this->Recipes->patchEntity($recipe, $data, ['associated' => 'RecipeUtensils']);

            if (!$this->Recipes->save($recipe, ['associated' => 'RecipeUtensils']))
            {
                $this->log($recipe->getErrors());
                $this->Flash->error(__('There was a problem saving the utensils lists. Please try again.'));
            }
            else
            {
                $this->Flash->success(__('The utensils list was successfully saved.'));
                return $this->redirect(['controller' => 'Recipes', 'action' => 'view', $recipe_id]);
            }
        }

        if (count($recipe_utensils) == 0)
        {
            $recipe_utensils = [];
            $recipe_utensils[] = $this->RecipeUtensils->newEntity();
        }

        $new_recipe_utensil = $this->RecipeUtensils->newEntity();

        $utensils = $this->Utensils->find()->all();

        $this->set([
            'new_recipe_utensil' => $new_recipe_utensil,
            'recipe' => $recipe,
            'recipe_utensils' => $recipe_utensils,
            'utensils' => $utensils,
        ]);

        $this->set('_serialize', ['recipe_ingredients']);
    }
}
