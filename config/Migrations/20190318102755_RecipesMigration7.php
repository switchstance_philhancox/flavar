<?php
use Migrations\AbstractMigration;

class RecipesMigration7 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $recipes_table = $this->table('recipes');
        $recipes_table
            ->addColumn('total_time_estimate', 'string', ['length' => 255, 'null' => true, 'after' => 'description'])
            ->update();
    }
}
