<%
/**
* OVERRIDE Controller bake template file
*
* Allows templating of Controllers generated from bake. Overrides the normal
* template file as our views are slightly different but consistent
* enough to warrant a new template file
*/
use Cake\Utility\Inflector;

$defaultModel = $name;
%>
<?php
namespace <%= $namespace %>\Controller<%= $prefix %>;

/**
 * <%= $name %> Controller
 *
 * @property \<%= $namespace %>\Model\Table\<%= $defaultModel %>Table $<%= $defaultModel %>
<%
foreach ($components as $component):
$classInfo = $this->Bake->classInfo($component, 'Controller/Component', 'Component');
%>
 *
 * @property <%= $classInfo['fqn'] %> $<%= $classInfo['name'] %>
<% endforeach; %>
 */
class <%= $name %>Controller extends AppController
{
	<%
	echo $this->Bake->arrayProperty('helpers', $helpers, ['indent' => false]);
	echo $this->Bake->arrayProperty('components', $components, ['indent' => false]);
	foreach($actions as $action) {
		echo $this->element('Controller/' . $action);
	}
	%>
}
