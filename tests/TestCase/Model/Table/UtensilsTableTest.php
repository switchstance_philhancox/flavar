<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UtensilsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UtensilsTable Test Case
 */
class UtensilsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UtensilsTable
     */
    public $Utensils;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Utensils',
        'app.Images'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Utensils') ? [] : ['className' => UtensilsTable::class];
        $this->Utensils = TableRegistry::getTableLocator()->get('Utensils', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Utensils);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
