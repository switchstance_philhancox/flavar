<?php
namespace App\View\Helper;

use Cake\View\Helper\FormHelper;

class BootstrapFormHelper extends FormHelper
{
    public function control($fieldName, array $options = [])
    {
        if ($this->getView()->getRequest()->is('post') && !$this->isFieldError($fieldName))
        {
            isset($options['class']) ? $options['class'] .= ' form-control is-valid' : $options['class'] = 'form-control';
        }
        else
        {
            isset($options['class']) ? $options['class'] .= ' form-control' : $options['class'] = 'form-control';
        }

        return parent::control($fieldName, $options);
    }
}