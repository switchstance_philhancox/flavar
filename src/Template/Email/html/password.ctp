Hello <?= $user->forename; ?>!<br><br>

You've just requested password reset code that will allow you to reset the password to your Flavar account. Here is the code you should paste into the app: <strong><?= $code; ?></strong>.
Please note the code is valid only for next 24 hours.<br><br>
If you haven't requested password reset code then please ignore this email.<br><br>

Kind regards,<br>
Flavar Team