<?php
namespace App\Controller\Admin;

use App\Controller\Component\UtilComponent;
use App\Model\Entity\Admin;
use App\Model\Table\AdminsTable;
use App\Model\Table\RecipesTable;
use Cake\I18n\Date;
use Cake\Mailer\Email;
use Cake\Routing\Router;

/**
 * @property AdminsTable $Admins
 * @property RecipesTable $Recipes
 *
 * Class DashboardController
 * @package App\Controller\Admin
 */
class DashboardController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['goToWorkspace', 'forgotPassword', 'resetPassword']);
    }

    public function index()
    {
        $this->loadModel('Recipes');

        $search_term = $this->request->getQuery('search_term');

        $where = ['is_deleted IS FALSE'];
        if ($search_term)
        {
            $where = [
                'OR' => [
                    'recipe_title LIKE' => '%' . $search_term . '%',
                ]
            ];
        }

        $recipes_query = $this->Recipes->find()->where($where)->contain(['Categories'])->orderDesc('recipe_id');

        $recipes = $this->paginate($recipes_query);

        $this->set([
            'recipes' => $recipes,
            'search_term' => $search_term,
        ]);
        $this->set('_serialize', ['recipes']);
    }

    public function login()
    {
        $this->viewBuilder()->setLayout('login');
        if ($this->request->is('post'))
        {
            $user = $this->Auth->identify();
            if ($user)
            {
                $data = $this->request->getData();
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl(isset($data['redirect']) ? $data['redirect'] : null));
            }
            else
            {
                $this->Flash->error(__('E-mail address or password is incorrect'));
            }
        }
        $this->set([
            'title' => 'Log in | ' . UtilComponent::APPLICATION_NAME,
        ]);
    }

    public function logout()
    {
        $this->request->getSession()->destroy();
        return $this->redirect($this->Auth->logout());
    }

    public function forgotPassword()
    {
        $this->viewBuilder()->setLayout('login');

        if ($this->request->is('post'))
        {
            $data = $this->request->getData();

            if ($data['email'] == '')
            {
                $this->Flash->error('Please enter an email address');
            }

            $this->loadModel('Admins');

            /** @var Admin $admin */
            $admin = $this->Admins->find()->where(['email_address' => $data['email']])->first();

            if ($admin)
            {
                $today = new Date();
                $tomorrow = $today->modify('Add 24 hours');

                $reset_password_hash = $this->Util->generateRandomString(16);
                $reset_password_expiry_date = $tomorrow->format('Y-m-d H:i:s');

                $admin->password_reset_code = $reset_password_hash;
                $admin->password_reset_expiry_date = $reset_password_expiry_date;

                $password_reset_link = Router::url(['controller' => 'Dashboard', 'action' => 'resetPassword', $reset_password_hash], true);

                if ($this->Admins->save($admin))
                {
                    $email = new Email();

                    $error = false;

                    $content = '<style type="text/css">p { font-family: Arial, Helvetica, sans-serif;}</style>'
                        .   '<img src="' . Router::url('/', true) . 'img/flavar-logo.png" alt="' . UtilComponent::APPLICATION_NAME . '">'
                        .   '<p>You have requested a password reset.</p>'
                        .   '<p>Please click the link below or copy and paste it into your web browsers.</p>'
                        .   '<p><a href="' . $password_reset_link . '">' . $password_reset_link . '</a></p>'
                        .   '<p>This link will expire in 24 hours.</p>'
                        .   '<p>If you didn\'t request a password reset, please ignore this e-mail.</p>'
                        .   '<p><small>This e-mail address isn\'t monitored so please don\'t reply.</small></p>';


                    // Send email
                    if (!$email
                        ->setSubject(sprintf(__('Reset your password to %s'), UtilComponent::APPLICATION_NAME))
                        ->setEmailFormat('both')
                        ->setTo($data['email'])
                        ->setFrom(UtilComponent::APPLICATION_EMAIL)
                        ->send($content)
                    )
                    {
                        $error = true;
                    }

                    if ($error)
                    {
                        $this->Flash->error('There was a problem sending the password reset e-mail. Please try again.');
                    }
                    else
                    {
                        $this->Flash->success('A link to reset your password has been sent to your e-mail address. The link will expire in 24 hours.');
                    }
                }
                else
                {
                    $this->Flash->error('There was a problem trying to reset your password. Please try again.');
                }
            }
            else
            {
                $this->Flash->error('There isn\'t an admin with that e-mail address. Please enter a valid e-mail.');
            }
        }

        $this->set([
            'title' => sprintf(__('Reset your password | %s'), UtilComponent::APPLICATION_NAME),
        ]);

    }

    /**
     * @param $reset_password_code
     */
    public function resetPassword($reset_password_code)
    {
        $this->loadModel('Admins');
        $this->viewBuilder()->setLayout('login');

        $error = false;

        if ($this->request->is('post'))
        {
            $data = $this->request->getData();

            $new_password = $data['new_password'];
            $confirm_password = $data['confirm_password'];

            /** @var Admin $admin */
            $admin = $this->Admins->find()->where(['password_reset_code' => $reset_password_code])->first();

            if (!$admin)
            {
                $this->Flash->error('There was a problem trying to find that password reset request. Please check the link and try again.');
                $error = true;
            }
            else
            {
                if ($new_password != $confirm_password)
                {
                    $this->Flash->error('The passwords don\'t match. Please try again.');
                }
                else
                {
                    $admin->admin_password = $new_password;
                    $admin->password_reset_code = $admin->password_reset_expiry_date = null;

                    if ($this->Admins->save($admin))
                    {
                        $this->Flash->success('Your password has been successfully changed. You can now log in.');
                        return $this->redirect(['controller' => 'Dashboard', 'action' => 'login']);
                    }
                    else
                    {
                        $this->Flash->error('There was a problem saving your password. Please try again.');
                    }
                }
            }
        }
        else
        {
            /** @var Admin $admin */
            $admin = $this->Admins->find()->where(['password_reset_code' => $reset_password_code])->first();

            if (!$admin)
            {
                $this->Flash->error('There was a problem trying to find that password reset request. Please check the link and try again.');
                $error = true;
            }
            else
            {
                $now = new \DateTime();

                if ($admin->password_reset_expiry_date > $now)
                {
                    $this->Flash->error('Sorry but the password reset request has now expired. Please request a new password reset.');
                    $error = true;
                }
            }
        }

        $this->set([
            'reset_password_code' => $reset_password_code,
            'title' => sprintf(__('Reset your password | %s'), UtilComponent::APPLICATION_NAME),
            'error' => $error,
        ]);
    }
}