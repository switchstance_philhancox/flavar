<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RecipeUtensilsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RecipeUtensilsTable Test Case
 */
class RecipeUtensilsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RecipeUtensilsTable
     */
    public $RecipeUtensils;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.RecipeUtensils',
        'app.Recipes',
        'app.Utensils'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RecipeUtensils') ? [] : ['className' => RecipeUtensilsTable::class];
        $this->RecipeUtensils = TableRegistry::getTableLocator()->get('RecipeUtensils', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RecipeUtensils);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
