<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller\Api;

use App\Controller\Component\UtilComponent;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Mailer\Email;


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 * @property UtilComponent      $Util
 * @property int $userId
 * @property string $userEmail
 * @property int $userRole
 *
 */
class AppController extends Controller
{
	/**
	 * @var int $currentUserId
	 */
	protected $currentUserId;

    /**
     * @throws \Exception
     */
	public function initialize()
	{
		parent::initialize();
		header('Access-Control-Allow-Origin: *'); //To Allow Cross Origin requests
        header('Access-Control-Allow-Headers: Content-Type, Authorization');
        header('Access-Control-Expose-Headers: X-Auth-Token');

		$this->loadComponent('RequestHandler');
		$this->loadComponent('Flash');
		$this->loadComponent('Cookie');
		$this->loadComponent('Util');
		$this->loadComponent('RestAuth');
		$this->loadComponent('File');
	}

	public function beforeFilter(Event $event)
	{
		if (!in_array($this->getRequest()->getParam('action'), ['info', 'sendPasswordResetCode', 'resetPassword', 'register', 'login', 'logout', 'validateToken']))
		{
			if (!$this->RestAuth->isAuthenticated())
			{
				echo $this->getJsonErrorResponse(403, 'Access denied');
				die;
			} else {
				$this->currentUserId = $this->RestAuth->getAuthenticatedUserId();
			}
		}
	}

	protected function getJsonErrorResponse($http_code, $error_message)
	{
		return json_encode([
			'error' => [
				'code' => $http_code,
				'message' => $error_message
			]
		]);
	}

	/**
	 * Before render callback.
	 *
	 * @param \Cake\Event\Event $event The beforeRender event.
	 *
	 * @return void
	 */
	public function beforeRender(Event $event)
	{

		if (!array_key_exists('_serialize', $this->viewVars) &&
			in_array($this->getResponse()->getType(), ['application/json', 'application/xml'])
		)
		{
			$this->set('_serialize', true);
		}
		$this->set('referer', $this->referer());
	}

	public function getUrl($url)
	{
	    $request = $this->getRequest();
		return $request->scheme() . '://' . $request->host() . $url;
	}

	public function formatValidationErrors($errors) {
		$errors_to_display = [];
		foreach($errors as $field_name => $errors_list) {
			foreach($errors_list as $error) {
				$errors_to_display[] = $error;
			}
		}

		return join('. ', $errors_to_display);
	}

	public function sendAdminNotification($subject, $message) {
        $email = new Email('default');
        $email->setFrom([UtilComponent::APPLICATION_EMAIL => UtilComponent::APPLICATION_NAME])
            ->setTo(UtilComponent::ADMIN_EMAIL)
            ->setSubject('['.UtilComponent::APPLICATION_NAME.'] Admin Notification: '.$subject)
            ->setTemplate('admin_notification')
            ->setEmailFormat('both');

        $email->setViewVars([
            'message' => $message
        ]);

        try {
            $email->send();
            return true;
        } catch(\Exception $e) {
            $this->log($e);
            return false;
        }
    }
}
