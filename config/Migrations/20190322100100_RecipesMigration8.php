<?php
use Migrations\AbstractMigration;

class RecipesMigration8 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $recipes_table = $this->table('recipes');
        $recipes_table
            ->addColumn('can_servings_be_adjusted', 'boolean', ['default' => true, 'after' => 'default_number_of_servings'])
            ->update();
    }
}
