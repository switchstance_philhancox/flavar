<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= isset($title) ? $title : $this->Util->getMetaTitle($this->request) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>vendor/elisyam/css/base/bootstrap.min.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>vendor/elisyam/css/base/elisyam-1.5.min.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>vendor/elisyam/css/datatables/datatables.min.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>vendor/bootstrap-sweetalert/sweetalert.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>vendor/summernote/dist/summernote.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>vendor/codemirror/codemirror.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>assets/css/animate/animate.min.css">
    <link rel="stylesheet" href="<?= $this->Url->build('/', true) ?>css/custom.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css"
          integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <?= $this->Html->meta('icon', $this->Url->build('/', true) . 'img/flavar-favicon.png'); ?>
</head>


<body class="home">

<div class="container-fluid h-100 overflow-y">
    <div class="row flex-row h-100">
        <div class="col-12">
            <div class="password-form mx-auto">
                <div class="text-center">
                    <img src="<?= $this->Url->build('/', true) . 'img/flavar-logo.png' ?>" alt="<?= __('Flavar') ?>" class="img-fluid">
                </div>
                <div class="mt-5">
                    <p class="text-center">
                        <strong>
                        <?= __('Coming soon to a kitchen near you.') ?>
                        </strong>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Begin Vendor Js -->
<script src="<?= $this->Url->build('/', true) ?>vendor/elisyam/js/base/jquery.min.js"></script>
<script src="<?= $this->Url->build('/', true) ?>vendor/elisyam/js/base/core.min.js"></script>
<script src="<?= $this->Url->build('/', true) ?>assets/vendors/js/nicescroll/nicescroll.min.js"></script>
<script src="<?= $this->Url->build('/', true) ?>assets/vendors/js/noty/noty.min.js"></script>
<script src="<?= $this->Url->build('/', true) ?>vendor/elisyam/js/app/app.min.js"></script>

<script src="<?= $this->Url->build('/', true) ?>js/custom.js"></script>
</body>
</html>
