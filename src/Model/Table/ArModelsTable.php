<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArModels Model
 *
 * @method \App\Model\Entity\ArModel get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArModel newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ArModel[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArModel|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArModel|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArModel patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArModel[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArModel findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ArModelsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ar_models');
        $this->setDisplayField('ar_model_id');
        $this->setPrimaryKey('ar_model_id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('ar_model_id')
            ->allowEmptyString('ar_model_id', 'create');

        $validator
            ->scalar('ar_model_file_name')
            ->maxLength('ar_model_file_name', 255)
            ->requirePresence('ar_model_file_name', 'create')
            ->allowEmptyFile('ar_model_file_name', false);

        $validator
            ->scalar('ar_model_zip_file_name')
            ->maxLength('ar_model_zip_file_name', 255)
            ->requirePresence('ar_model_zip_file_name', 'create')
            ->allowEmptyFile('ar_model_zip_file_name', false);

        $validator
            ->scalar('ar_model_url')
            ->maxLength('ar_model_url', 255)
            ->requirePresence('ar_model_url', 'create')
            ->allowEmptyString('ar_model_url', false);

        $validator
            ->nonNegativeInteger('ar_model_file_size')
            ->requirePresence('ar_model_file_size', 'create')
            ->allowEmptyFile('ar_model_file_size', false);

        return $validator;
    }
}
