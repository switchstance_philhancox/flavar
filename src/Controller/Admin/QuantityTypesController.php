<?php
namespace App\Controller\Admin;

use App\Model\Entity\QuantityType;

/**
 * QuantityTypes Controller
 *
 * @property \App\Model\Table\QuantityTypesTable $QuantityTypes
 */
class QuantityTypesController extends AppController
{
	
	public function index()
	{
        $search_term = $this->request->getQuery('search_term');

        $where = [];
        if ($search_term)
        {
            $where = [
                'OR' => [
                    'quantity_type_name LIKE' => '%' . $search_term . '%',
                ]
            ];
        }

        $quantity_types_query = $this->QuantityTypes->find()->where($where);

        $quantity_types = $this->paginate($quantity_types_query);

        $this->set([
            'quantity_types' => $quantity_types,
            'search_term' => $search_term,
        ]);
        $this->set('_serialize', ['quantity_types']);
	}

    /**
    * @return \Cake\Http\Response|null
    */
	public function add()
	{
		$quantity_type = $this->QuantityTypes->newEntity();
		if ($this->request->is('post'))
		{
            $data = $this->request->getData();
            $data['is_deleted'] = false;
			$quantity_type = $this->QuantityTypes->patchEntity($quantity_type, $data);
			if ($this->QuantityTypes->save($quantity_type))
			{
				$this->Flash->success(__('The quantity type has been saved.'));
				return $this->redirect(['action' => 'index']);
			}
			else
			{
				$this->Flash->error(__('The quantity type could not be saved. Please, try again.'));
			}
		}
		
        $this->set([
            'quantity_type' => $quantity_type,
        ]);

	}


    /**
     * @param null $quantity_type_id
     *
     * @return \Cake\Http\Response|null
     */
	public function edit($quantity_type_id = null)
	{
        $quantity_type = $this->QuantityTypes->find()->where(['quantity_type_id' => $quantity_type_id])->first();

        if (!$quantity_type)
        {
            $this->Flash->error('The quantity type could not be found. Please, try again.');
            return $this->redirect(['action' => 'index']);
        }
		 
		if ($this->request->is(['patch', 'post', 'put']))
		{
            $data = $this->request->getData();
            $data['is_deleted'] = false;
			$quantity_type = $this->QuantityTypes->patchEntity($quantity_type, $data);
			if ($this->QuantityTypes->save($quantity_type))
			{
				$this->Flash->success(__('The quantity type has been saved.'));
				return $this->redirect(['action' => 'index']);
			}
			else
			{
                $this->log($quantity_type->getErrors());
				$this->Flash->error(__('The quantity type could not be saved. Please, try again.'));
			}
		}

		
        $this->set([
            'quantity_type' => $quantity_type,
        ]);
        $this->set('_serialize', ['quantity_type']);
	}

    /**
     * @return \Cake\Http\Response|null
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);

        if ($this->request->is('json'))
        {
            $quantity_type_id = $this->request->getData('id');

            /** @var QuantityType $utensil */
            $quantity_type = $this->QuantityTypes->find()->where(['quantity_type_id' => $quantity_type_id])->first();

            if (!$quantity_type)
            {
                $output = [
                    'status' => false,
                    'message' => __('The quantity type could not be found. Please try again.'),
                ];
            }
            else
            {
                if (!$this->QuantityTypes->delete($quantity_type))
                {
                    $this->log($quantity_type->getErrors());
                    $output = [
                        'status' => false,
                        'message' => __('There was a problem trying to delete the quantity type. Please try again.'),
                    ];
                }
                else
                {
                    $output = [
                        'status' => true,
                        'message' => __('The quantity type was successfully deleted.'),
                    ];
                }
            }

            $this->set(compact('output'));
            $this->set('_serialise', ['output']);
        }
        else
        {
            return $this->redirect('index');
        }
    }
}
