<?php
use Migrations\AbstractMigration;

class RecipesMigration3 extends AbstractMigration
{
    public function up()
    {
        $recipe_utensils_table = $this->table('recipe_utensils');
        $recipe_utensils_table
            ->changeColumn('recipe_utensil_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
            ->update();
    }

    public function down()
    {
        $recipe_utensils_table = $this->table('recipe_utensils');
        $recipe_utensils_table
            ->changeColumn('recipe_utensil_id', 'integer', ['limit' => 11, 'signed' => false])
            ->update();
    }
}
