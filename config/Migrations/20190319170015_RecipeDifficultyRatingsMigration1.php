<?php
use Migrations\AbstractMigration;

class RecipeDifficultyRatingsMigration1 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $users_table = $this->table('users', ['id' => false, 'primary_key' => 'user_id']);
        $users_table
            ->addColumn('user_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
            ->addColumn('user_name', 'string', ['limit' => 255])
            ->addColumn('display_name', 'string', ['limit' => 255])
            ->addColumn('email_address', 'string', ['limit' => 255])
            ->addColumn('user_password', 'string', ['limit' => 255])
            ->addColumn('created', 'datetime')
            ->addColumn('modified', 'datetime')
            ->create();

        $recipe_difficulty_ratings_table = $this->table('recipe_difficulty_ratings', ['id' => false, 'primary_key' => 'recipe_difficulty_rating_id']);
        $recipe_difficulty_ratings_table
            ->addColumn('recipe_difficulty_rating_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
            ->addColumn('recipe_id', 'integer', ['limit' => 11, 'signed' => false])
            ->addForeignKey('recipe_id', 'recipes', 'recipe_id', ['update' => 'CASCADE', 'delete' => 'CASCADE'])
            ->addColumn('difficult_rating', 'integer', ['limit' => 11])
            ->addColumn('user_id', 'integer', ['limit' => 11, 'signed' => false])
            ->addForeignKey('user_id', 'users', 'user_id', ['update' => 'CASCADE', 'delete' => 'CASCADE'])
            ->addColumn('created', 'datetime')
            ->addColumn('modified', 'datetime')
            ->create();
    }
}
