<?php

namespace App\Controller\Component;

use Cake\Auth\DefaultPasswordHasher;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

class RestAuthComponent extends Component
{

	public $components = ['Util'];
	private $authenticatedUserId;

	public function isAuthenticated()
	{
		$token = $this->getController()->getRequest()->getHeaderLine('Authorization');
		if($token && strpos($token, 'Bearer') === 0)
		{
		    list(, $token) = explode(' ', $token);

		    try
            {
                $token_object = JWT::decode($token, Security::getSalt(), ['HS256']);
                $user_id = intval($token_object->sub);
                $diff_minutes = round((time() - $token_object->last_check_time) / 60);
                if($diff_minutes >= 15)
                {
                    $usersTable = TableRegistry::getTableLocator()->get('Users');
                    $q = $usersTable->find()
                        ->where([
                            'user_id' => $user_id,
                            'is_live' => true,
                            'is_deleted' => false
                        ]);

                    if($q->count() === 0)
                    {
                        return false;
                    }

                    $new_token = (array) $token_object;
                    $new_token['last_check_time'] = time();
                    header('X-Auth-Token: ' . JWT::encode($new_token, Security::getSalt()));
                }
                else
                {
                    header('X-Auth-Token: ' . $token);
                }

                $this->authenticatedUserId = $user_id;
                return true;
            }
            catch (\Exception $e)
            {
                return false;
            }
		}

		return false;
	}

	public function getAuthenticatedUserId() {
		return $this->authenticatedUserId;
	}

	public function authenticate()
	{
	    $request = $this->getController()->getRequest();
		$email = $request->getData('email');
		$password = $request->getData('password');
		if($email && $password)
		{
			$usersTable = TableRegistry::getTableLocator()->get('Users');
			$q = $usersTable->find()
				->where([
					'email_address' => $email,
                    'is_live' => true,
					'is_deleted' => false
				]);

			if($q->count() > 0)
			{
				$user = $q->first();
				$dfh = new DefaultPasswordHasher();
				if($dfh->check($password, $user->user_password))
				{
					$this->generateAuthToken($user);
					return $user;
				}
			}
		}

		return false;
	}

	public function generateAuthToken($user, $user_id = null) {
	    if($user == null) {
	        if($user_id == null) {
	            return false;
            }
        } else {
	        $user_id = $user->user_id;
        }

		$token = ['sub' => $user_id, 'last_check_time' => time()];
		header('X-Auth-Token: ' . JWT::encode($token, Security::getSalt()));
		return true;
	}

}