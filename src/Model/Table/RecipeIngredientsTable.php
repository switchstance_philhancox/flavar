<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RecipeIngredients Model
 *
 * @property \App\Model\Table\RecipesTable|\Cake\ORM\Association\BelongsTo $Recipes
 *
 * @method \App\Model\Entity\RecipeIngredient get($primaryKey, $options = [])
 * @method \App\Model\Entity\RecipeIngredient newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RecipeIngredient[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RecipeIngredient|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RecipeIngredient|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RecipeIngredient patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RecipeIngredient[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RecipeIngredient findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RecipeIngredientsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('recipe_ingredients');
        $this->setDisplayField('recipe_ingredient_id');
        $this->setPrimaryKey('recipe_ingredient_id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Recipes', [
            'foreignKey' => 'recipe_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('QuantityTypes', [
            'foreignKey' => 'quantity_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('recipe_ingredient_id')
            ->allowEmptyString('recipe_ingredient_id', 'create');

        $validator
            ->decimal('quantity')
            ->requirePresence('quantity', 'create')
            ->allowEmptyString('quantity', false);

        $validator
            ->scalar('quantity_type')
            ->maxLength('quantity_type', 255)
            ->allowEmptyString('quantity_type');

        $validator
            ->scalar('ingredient_name')
            ->maxLength('ingredient_name', 255)
            ->requirePresence('ingredient_name', 'create')
            ->allowEmptyString('ingredient_name', false);

        $validator
            ->decimal('quantity_per_additional_serving')
            ->requirePresence('quantity_per_additional_serving', 'create')
            ->allowEmptyString('quantity_per_additional_serving', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['recipe_id'], 'Recipes'));

        return $rules;
    }
}
