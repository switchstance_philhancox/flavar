<?php
/**
 * @var \App\View\AppView $this
 * @var string $method
 * @var \App\Model\Entity\Recipe $recipe
 * @var App\Model\Entity\RecipeStep $recipe_step
 * @var \App\Model\Entity\RecipeUtensil[] $utensils
 * @var \App\Model\Entity\RecipeStepUtensil[] $recipe_step_utensils
 */
?>

<div class="row mb-5">
    <div class="page-header pb-0">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <span class="c-grey fw-normal"><?= __('Recipes') ?></span><br>
                <a href="<?= $this->Url->build(['controller' => 'Recipes', 'action' => 'view', $recipe->recipe_id]) ?>">
                    <?= $recipe->recipe_title ?></a> > <?= __('Steps') ?>
            </h2>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h2><?= $method == 'add' ? __('Add New Recipe Step') : __('Edit Recipe Step') ?></h2>
            </div>
            <div class="widget-body">
                <?= $this->Form->create($recipe_step); ?>

                <?= $this->Form->control('recipe_step_id') ?>
                <?= $this->Form->control('recipe_id', ['type' => 'hidden', 'value' => $recipe->recipe_id]) ?>
                <?= $this->Form->control('step_description', ['type' => 'textarea']) ?>

                <div class="form-group row d-flex align-items-center mb-5 required">
                    <label class="col-lg-4 form-control-label d-flex justify-content-lg-end" for="has-timer"><?= __('Has Timer') ?></label>
                    <div class="col-lg-1">
                        <div class="custom-control custom-radio styled-radio mb-3">
                            <input value="1" class="custom-control-input has-timer-select" type="radio" name="has_timer" id="has-timer-true" required="required"<?= $recipe_step->has_timer ? ' checked="checked"' : '' ?>>
                            <label class="custom-control-descfeedback" for="has-timer-true"><?= __('Yes') ?></label>
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="custom-control custom-radio styled-radio mb-3">
                            <input value="0" class="custom-control-input has-timer-select" type="radio" name="has_timer" id="has-timer-false" required="required"<?= !$recipe_step->has_timer || $method == 'add' ? ' checked="checked"' : '' ?>>
                            <label class="custom-control-descfeedback" for="has-timer-false"><?= __('No') ?></label>
                        </div>
                    </div>
                </div>

                <div class="time-container<?= !$recipe_step->has_timer ? ' d-none' : '' ?>">

                <?= $this->Form->control('timer_time', ['label' => __('Time'), 'placeholder' => 'HH:MM:SS']) ?>

                <div class="form-group row d-flex align-items-center mb-5 required">
                    <label class="col-lg-4 form-control-label d-flex justify-content-lg-end" for="must-time"><?= __('Must time run out before next step?') ?></label>
                    <div class="col-lg-1">
                        <div class="custom-control custom-radio styled-radio mb-3">
                            <input value="1" class="custom-control-input" type="radio" name="must_complete_time" id="must-complete-time-true" required="required"<?= $recipe_step->must_complete_time ? ' checked="checked"' : '' ?>>
                            <label class="custom-control-descfeedback" for="must-complete-time-true"><?= __('Yes') ?></label>
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="custom-control custom-radio styled-radio mb-3">
                            <input value="0" class="custom-control-input" type="radio" name="must_complete_time" id="must-complete-time-false" required="required"<?= !$recipe_step->must_complete_time || $method == 'add' ? ' checked="checked"' : '' ?>>
                            <label class="custom-control-descfeedback" for="must-complete-time-false"><?= __('No') ?></label>
                        </div>
                    </div>
                </div>

                </div>

                <div class="form-group row d-flex align-items-center mb-5 required">
                    <label class="col-lg-4 form-control-label d-flex justify-content-lg-end" for="is-mise-en-place"><?= __('Is this mise-en-place?') ?></label>
                    <div class="col-lg-1">
                        <div class="custom-control custom-radio styled-radio mb-3">
                            <input value="1" class="custom-control-input" type="radio" name="is_mise_en_place" id="is-mep-true" required="required"<?= $recipe_step->is_mise_en_place ? ' checked="checked"' : '' ?>>
                            <label class="custom-control-descfeedback" for="is-mep-true"><?= __('Yes') ?></label>
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="custom-control custom-radio styled-radio mb-3">
                            <input value="0" class="custom-control-input" type="radio" name="is_mise_en_place" id="is-mep-false" required="required"<?= !$recipe_step->is_mise_en_place || $method == 'add' ? ' checked="checked"' : '' ?>>
                            <label class="custom-control-descfeedback" for="is-mep-false"><?= __('No') ?></label>
                        </div>
                    </div>
                </div>

                <div class="form-group row d-flex align-items-center mb-5 required">
                    <label class="col-lg-4 form-control-label d-flex justify-content-lg-end"><?= __('Utensil?') ?></label>
                    <input type="hidden" name="recipe_utensil_id" value="<?= $recipe_step->recipe_utensil_id ? $recipe_step->recipe_utensil_id : '' ?>">
                    <?php if (count($utensils) > 0) : ?>
                        <div class="col-lg-3">
                            <?php if ($method == 'add' || $recipe_step->recipe_utensil_id == null) : ?>
                                <span id="recipe-utensil-step-name"><?= __('No utensil selected') ?></span>
                            <?php else : ?>
                                <span id="recipe-utensil-step-name"><span style="color: #333"><?= $recipe_step->recipe_utensil->internal_name ?></span> (<?= $recipe_step->recipe_utensil->utensil->utensil_name ?>)</span>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-3">
                            <a class="c-primary" id="recipe-step-utensil-action-label" href="#" data-target="#select-utensil-modal" data-toggle="modal"><?= $recipe_step->recipe_utensil ? __('Edit Utensil') : __('Add Utensil') ?></a>
                            <a class="<?= $method == 'add' || $recipe_step->recipe_utensil == null ? 'd-none ' : '' ?>c-red ml-4" id="recipe-step-utensil-remove-label" href="#"><?= __('Remove Utensil') ?></a>
                        </div>
                    <?php else : ?>
                        <div class="col-lg-6">
                            <p><?= __('You must add some utensils to the recipe in order to attach them to recipe steps.') ?></p>
                        </div>
                    <?php endif; ?>

                </div>

                <div id="select-utensil-modal" class="modal fade">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"><?= __('Recipe Step Utensil') ?></h4>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">×</span>
                                    <span class="sr-only">close</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <div class="form-group row d-flex align-items-center mb-5 required">
                                    <label class="col-lg-4 form-control-label d-flex justify-content-lg-end" for="recipe-step-utensil-select"><?= __('Select utensil') ?></label>
                                    <div class="col-lg-8">
                                        <select class="form-control recipe-step-utensil-select recipe-step-utensil-selector" id="recipe-step-utensil-select">
                                            <option value="" data-utensil-name=""><?= __('-- Select a utensil --') ?></option>
                                            <?php foreach ($utensils as $utensil) : ?>
                                            <option data-utensil-name="<?= $utensil->internal_name ?>" value="<?= $utensil->recipe_utensil_id ?>"><?= $utensil->internal_name ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <a class="btn btn-default btn-square" data-dismiss="modal"><?= __('Cancel') ?></a>
                                <a class="btn btn-primary btn-square add-utensil-to-step" href="#" type="submit"><?= __('Add Utensil to Step') ?></a>
                            </div>
                        </div>
                    </div>
                </div>

                <?= $this->Form->control('video_link', ['type' => 'url', 'placeholder' => __('Must start with http:// or https://')]) ?>
                <?= $this->Form->control('tip_description', ['type' => 'textarea', 'label' => __('Tip')]) ?>

                <div class="text-right">
                    <input class="btn btn-primary btn-square" type="submit" value="<?= __('Save Record') ?>">
                </div>

                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>