
	public function index()
	{
        $search_term = $this->request->getQuery('search_term');

        $where = [];
        if ($search_term)
        {
            $where = [
                'OR' => [
                ]
            ];
        }

        $<?= $pluralName ?>_query = $this-><?= $currentModelName ?>->find()->where($where);

        $<?= $pluralName ?> = $this->paginate($<?= $pluralName ?>_query);

        $this->set([
            '<?= $pluralName ?>' => $<?= $pluralName ?>,
            'search_term' => $search_term,
        ]);
        $this->set('_serialize', ['<?= $pluralName ?>']);
	}
