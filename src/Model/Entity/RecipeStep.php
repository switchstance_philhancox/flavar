<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RecipeStep Entity
 *
 * @property int $recipe_step_id
 * @property int $recipe_id
 * @property int|null $order_number
 * @property string $step_description
 * @property bool $has_timer
 * @property string $timer_time
 * @property bool $must_complete_time
 * @property bool $is_mise_en_place
 * @property int|null $utensil_id
 * @property int|null $recipe_utensil_id
 * @property string|null $video_link
 * @property string|null $tip_description
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Recipe $recipe
 * @property \App\Model\Entity\Utensil $utensil
 * @property RecipeUtensil $recipe_utensil
 */
class RecipeStep extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];
}
