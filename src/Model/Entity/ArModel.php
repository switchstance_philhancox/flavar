<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Routing\Router;

/**
 * ArModel Entity
 *
 * @property int $ar_model_id
 * @property string $ar_model_file_name
 * @property string $ar_model_zip_file_name
 * @property string $ar_model_url
 * @property int $ar_model_file_size
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Recipe[] $recipes
 */
class ArModel extends Entity
{

    protected $_virtual = ['ar_model_full_url'];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ar_model_file_name' => true,
        'ar_model_zip_file_name' => true,
        'ar_model_url' => true,
        'ar_model_file_size' => true,
        'created' => true,
        'modified' => true,
        'recipes' => true
    ];

    public function _getArModelFullUrl() {
        $request = Router::getRequest();
        return $request->scheme() . '://' . $request->host() . '/' . $this->ar_model_url;
    }
}
