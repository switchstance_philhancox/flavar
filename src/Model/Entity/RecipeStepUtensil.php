<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RecipeStepUtensil Entity
 *
 * @property int $recipe_step_utensil_id
 * @property int $utensil_id
 * @property string $step_utensil_name
 *
 * @property \App\Model\Entity\Utensil $utensil
 */
class RecipeStepUtensil extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];
}
