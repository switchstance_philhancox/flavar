<?php
namespace App\Controller\Admin;

use App\Model\Entity\Recipe;
use App\Model\Table\TagsTable;
use Cake\ORM\Query;

/**
 * Recipes Controller
 *
 * @property \App\Model\Table\RecipesTable $Recipes
 * @property TagsTable $Tags
 */
class RecipesController extends AppController
{
    /**
    * @param null $recipe_id
    *
    * @return \Cake\Http\Response|null
    */
    public function view($recipe_id = null)
    {
        $recipe = $this->Recipes->find()
            ->where(['Recipes.recipe_id' => $recipe_id])
            ->contain([
                'Admins',
                'ArModels',
                'Categories',
                'RecipeIngredients' => [
                    'QuantityTypes'
                ],
                'RecipeUtensils' => [
                    'Utensils',
                ],
                'RecipePlatingDescriptions' => [
                    'Images',
                ],
                'RecipeSteps' => function (Query $query) {
                    return $query->orderAsc('order_number')->contain(['RecipeUtensils' => ['Utensils' => ['Images']]]);
                },
                'RecipeImages' => [
                    'Images',
                ],
                'RecipeTags' => [
                    'Tags',
                ],
                'Images',
            ])
            ->first();

        if (!$recipe)
        {
            $this->Flash->error('The record could not be found. Please try again.');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }

        $this->set([
            'recipe' => $recipe
        ]);
    }

    /**
     * @return \Cake\Http\Response|null
     */
	public function add()
	{
        $this->loadModel('Tags');

		$recipe = $this->Recipes->newEntity();
		if ($this->request->is('post'))
		{
            $data = $this->request->getData();
            if(empty($data)) {
                $this->Flash->error('Error occurred while adding new recipe. Please make sure uploaded file size doesn\'t exceed server limits.');
                return $this->redirect(['controller' => 'Recipes', 'action' => 'add']);
            }

            $data['is_deleted'] = false;
            $data['admin_id'] = $this->admin_id;

            if ($data['image_file']['name'] != null)
            {
                $image_id = $this->File->uploadFile($data['image_file'], 'uploads' . DS . 'recipes', ['admin_id' => $this->admin_id]);

                if (is_int($image_id))
                {
                    $data['featured_image_id'] = $image_id;
                }
            }

            if ($data['model_file']['name'] != null)
            {
                $ar_model_data = $this->File->uploadFile($data['model_file'], 'uploads' . DS . 'models', [
                    'file_type' => $this->File::FILE_TYPE_AR_MODEL,
                    'allowed_extensions' => ['zip'],
                    'max_file_size_mb' => 20
                ]);

                if ($ar_model_data)
                {
                    $data['ar_model'] = $ar_model_data;
                }
            }

            if (isset($data['tags']) && count($data['tags']) > 0)
            {
                foreach ($data['tags'] as $tag_name)
                {
                    $data['recipe_tags'][] = [
                        'tag_id' => $tag_name,
                    ];
                }
            }

			$recipe = $this->Recipes->patchEntity($recipe, $data, ['associated' => ['RecipeTags', 'ArModels']]);
			if ($this->Recipes->save($recipe, ['associated' => ['RecipeTags', 'ArModels']]))
			{
				$this->Flash->success(__('The recipe has been saved.'));
                return $this->redirect(['controller' => 'Recipes', 'action' => 'view', $recipe->recipe_id]);
			}
			else
			{
			    $this->log($recipe->getErrors());
				$this->Flash->error(__('The recipe could not be saved. Please, try again.'));
			}
		}

        $categories = $this->Recipes->Categories->find('list', ['limit' => 200]);
        $tags = $this->Tags->find()->orderAsc('tag_name')->toList();

        $this->set([
            'categories' => $categories,
            'recipe' => $recipe,
            'tags' => $tags,
        ]);

	}


	/**
    * @param null $recipe_id
    *
    * @return \Cake\Http\Response|null
    */
	public function edit($recipe_id = null)
	{
	    $this->loadModel('Tags');

        $recipe = $this->Recipes->find()->where(['recipe_id' => $recipe_id])->contain(['RecipeTags', 'RecipeSteps', 'Images', 'ArModels'])->first();

        if (!$recipe)
        {
            $this->Flash->error('The $recipe could not be found. Please, try again.');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
		 
		if ($this->request->is(['patch', 'post', 'put']))
		{
            $data = $this->request->getData();

            if (isset($data['remove_image_file']) && $data['remove_image_file'] == true)
            {
                $data['featured_image_id'] = null;
            }

            if ($data['image_file']['name'] != null)
            {
                $image_id = $this->File->uploadFile($data['image_file'], 'uploads' . DS . 'recipes', ['admin_id' => $this->admin_id]);

                if (is_int($image_id))
                {
                    $data['featured_image_id'] = $image_id;
                }
            }

            if (isset($data['remove_model_file']) && $data['remove_model_file'] == true)
            {
                $data['ar_model_id'] = null;
            }

            if ($data['model_file']['name'] != null)
            {

                $ar_model_data = $this->File->uploadFile($data['model_file'], 'uploads' . DS . 'models', [
                    'file_type' => $this->File::FILE_TYPE_AR_MODEL,
                    'allowed_extensions' => ['zip'],
                    'max_file_size_mb' => 20
                ]);

                if ($ar_model_data)
                {
                    $data['ar_model'] = $ar_model_data;
                }
            }

            if (isset($data['tags']) && count($data['tags']) > 0)
            {
                foreach ($data['tags'] as $tag_name)
                {
                    $data['recipe_tags'][] = [
                        'recipe_id' => $recipe_id,
                        'tag_id' => $tag_name,
                    ];
                }
            }

			$recipe = $this->Recipes->patchEntity($recipe, $data, ['associated' => ['RecipeTags', 'ArModels']]);

			if ($this->Recipes->save($recipe, ['associated' => ['RecipeTags', 'ArModels']]))
			{
				$this->Flash->success(__('The recipe has been saved.'));
				return $this->redirect(['controller' => 'Recipes', 'action' => 'view', $recipe_id]);
			}
			else
			{
                $this->log($recipe->getErrors());
				$this->Flash->error(__('The recipe could not be saved. Please, try again.'));
			}
		}

        $images = $this->Recipes->Images->find('list', ['limit' => 200]);
        $categories = $this->Recipes->Categories->find('list', ['limit' => 200]);
        $tags = $this->Tags->find()->orderAsc('tag_name')->toList();
		
        $this->set([
            'categories' => $categories,
            'recipe' => $recipe,
            'tags' => $tags,
        ]);
        $this->set('_serialize', ['recipes']);
	}

    /**
     * @return \Cake\Http\Response|null
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);

        if ($this->request->is('json'))
        {
            $recipe_id = $this->request->getData('id');

            /** @var Recipe $recipe */
            $recipe = $this->Recipes->find()->where(['recipe_id' => $recipe_id])->first();

            if (!$recipe)
            {
                $output = [
                    'status' => false,
                    'message' => __('The recipe could not be found. Please try again.'),
                ];
            }
            else
            {
                $recipe->is_deleted = true;
                if (!$this->Recipes->save($recipe))
                {
                    $this->log($recipe->getErrors());
                    $output = [
                        'status' => false,
                        'message' => __('There was a problem trying to delete the recipe. Please try again.'),
                    ];
                }
                else
                {
                    $output = [
                        'status' => true,
                        'message' => __('The recipe was successfully deleted.'),
                    ];
                }
            }

            $this->set(compact('output'));
            $this->set('_serialise', ['output']);
        }
        else
        {
            return $this->redirect('index');
        }
    }
}
