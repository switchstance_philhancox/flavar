<?php
use Migrations\AbstractMigration;

class RecipesMigration2 extends AbstractMigration
{
    public function up()
    {
        $recipe_ingredients_table = $this->table('recipe_ingredients');
        $recipe_ingredients_table
            ->changeColumn('quantity', 'float', ['precision' => 10, 'scale' => 4])
            ->changeColumn('quantity_per_additional_serving', 'float', ['precision' => 10, 'scale' => 4])
            ->update();
    }

    public function down()
    {
        $recipe_ingredients_table = $this->table('recipe_ingredients');
        $recipe_ingredients_table
            ->changeColumn('quantity', 'integer', ['limit' => 11])
            ->changeColumn('quantity_per_additional_serving', 'integer', ['limit' => 11])
            ->update();
    }
}
