/**
 * ADD REMOVE ROWS
 * Should work globally if you create a container with two rows. One row should be hidden with class 'd-none' and all instances
 * where you need a number e.g. for arrays and IDs replaced with {{counter}}, and then another, none hidden row to act as the
 * first row. The remove button in this row should be hidden with class d-none. Add remove rows can be found in Element/Arr
 * for examples. Ensure you check the files where those elements are placed for more examples of how they work.
 *
 * NOTE: Array items with a key of {{counter}} should be unset as part of the save method in the controller before saving
 * or you're gonna have a bad time.
 */


/**
 * Adds a new row including its own container. Replaces all instances of {{counter}} with the correct key to ensure
 * IDs are unique and that the array to be submitted is correct.
 *
 * @param container_id
 */
function addRow(container_id) {
    let row_html = $(container_id + ' .arr-row').first().clone(true, true).get(0).outerHTML;
    let row_key = $(container_id + ' .arr-row').last().data('key');
    let new_row_key = (row_key + 1);
    row_html = row_html.split('{{counter}}').join(new_row_key);
    row_html = row_html.replace(' d-none', '');
    $(container_id).append(row_html);

}

/**
 * Well... it removes the row...
 *
 * @param button
 */
function removeRow(button) {
    button.closest('tr').remove();
}

/**
 * Upon clicking the relevant "add row" button, adds a new row if the maximum hasn't been reached. Ensures the "remove row"
 * button appears once a minimum number of rows is achieved.
 */
$(document.body).on('click', '.arr-add-row', function () {
    let container_id = '#' + $(this).data('container-id');
    let maximum_rows = $(this).data('maximum-rows');
    let minimum_rows = $(this).data('minimum-rows') === undefined ? 3 : $(this).data('minimum-rows');

   if (maximum_rows !== undefined && $(container_id + ' tr').length >= maximum_rows) {
       return false;
   } else {
       addRow(container_id);
   }

    if ($(container_id + ' tr').length === minimum_rows) {
        $(container_id + ' .arr-remove-row').addClass('d-none');
    } else {
        $(container_id + ' .arr-remove-row').removeClass('d-none');
    }
});

/**
 * Upon clicking the relevant "remove row" button, removes the row as long as there are more than the minimum
 * number of allowed rows. Ensures that if the minimum number of rows is reached, the "remove row" button is
 * hidden so you cannot delete the only surviving row.
 */
$(document.body).on('click', '.arr-remove-row', function () {
    let container_id = '#' + $(this).data('container-id');
    let minimum_rows = $(this).data('minimum-rows') === undefined ? 3 : $(this).data('minimum-rows');
    if($(container_id + ' tr').length === minimum_rows) {
        //alert('Can't remove row.');
        $(container_id + ' .arr-remove-row').addClass('d-none');
    } else if($(container_id + ' tr').length - 1 === minimum_rows) {
        $(container_id + ' .arr-remove-row').addClass('d-none');
        removeRow($(this));
    } else {
        removeRow($(this));
    }
});
