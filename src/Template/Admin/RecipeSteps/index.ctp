<?php
/**
 * @var \App\View\AppView $this
 * @var App\Model\Entity\RecipeStep[]|\Cake\Collection\CollectionInterface $recipeSteps */
?>

<!-- Begin Page Header-->
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title"><?= __('Recipe Steps') ?></h2>
            <div>
                <a class="btn btn-square btn-outline-primary" href="<?= $this->Url->build(['controller' => 'Recipe Step', 'action' => 'add']) ?>">
                    <?= __('Add New Recipe Steps') ?>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header -->


<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <!--
            <div class="widget-header bordered no-actions d-flex align-items-center">
            </div>
            -->
            <div class="widget-body">
                <?php if (count($recipeSteps) > 0) : ?>
                <div class="table-responsive">
                    <div id="sorting-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div id="sorting-table_filter" class="dataTables_filter">
                                    <form method="get" action="#">
                                        <label><?= __('Search:') ?><input type="search" class="form-control form-control-sm" placeholder="" name="search_term" aria-controls="sorting-table" value="<?= $search_term ?>"></label>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <table class="table mb-0 dataTable table-hover">
                                    <thead>
                                    <tr>
                                        
                                        <?php
                                        if ($this->request->getQuery('sort') == 'recipe_step_id') :
                                            if ($this->request->getQuery('direction') == 'asc') :
                                                $sort_class = 'sorting_asc';
                                            elseif ($this->request->getQuery('direction') == 'desc') :
                                                $sort_class = 'sorting_desc';
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                        else :
                                            $sort_class = 'both';
                                        endif;
                                        ?>
                                        <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('recipe_step_id') ?></th>
                                        
                                        <?php
                                        if ($this->request->getQuery('sort') == 'recipe_id') :
                                            if ($this->request->getQuery('direction') == 'asc') :
                                                $sort_class = 'sorting_asc';
                                            elseif ($this->request->getQuery('direction') == 'desc') :
                                                $sort_class = 'sorting_desc';
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                        else :
                                            $sort_class = 'both';
                                        endif;
                                        ?>
                                        <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('recipe_id') ?></th>
                                        
                                        <?php
                                        if ($this->request->getQuery('sort') == 'order_number') :
                                            if ($this->request->getQuery('direction') == 'asc') :
                                                $sort_class = 'sorting_asc';
                                            elseif ($this->request->getQuery('direction') == 'desc') :
                                                $sort_class = 'sorting_desc';
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                        else :
                                            $sort_class = 'both';
                                        endif;
                                        ?>
                                        <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('order_number') ?></th>
                                        
                                        <?php
                                        if ($this->request->getQuery('sort') == 'step_description') :
                                            if ($this->request->getQuery('direction') == 'asc') :
                                                $sort_class = 'sorting_asc';
                                            elseif ($this->request->getQuery('direction') == 'desc') :
                                                $sort_class = 'sorting_desc';
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                        else :
                                            $sort_class = 'both';
                                        endif;
                                        ?>
                                        <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('step_description') ?></th>
                                        
                                        <?php
                                        if ($this->request->getQuery('sort') == 'has_timer') :
                                            if ($this->request->getQuery('direction') == 'asc') :
                                                $sort_class = 'sorting_asc';
                                            elseif ($this->request->getQuery('direction') == 'desc') :
                                                $sort_class = 'sorting_desc';
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                        else :
                                            $sort_class = 'both';
                                        endif;
                                        ?>
                                        <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('has_timer') ?></th>
                                        
                                        <?php
                                        if ($this->request->getQuery('sort') == 'timer_time') :
                                            if ($this->request->getQuery('direction') == 'asc') :
                                                $sort_class = 'sorting_asc';
                                            elseif ($this->request->getQuery('direction') == 'desc') :
                                                $sort_class = 'sorting_desc';
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                        else :
                                            $sort_class = 'both';
                                        endif;
                                        ?>
                                        <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('timer_time') ?></th>
                                        
                                        <?php
                                        if ($this->request->getQuery('sort') == 'must_complete_time') :
                                            if ($this->request->getQuery('direction') == 'asc') :
                                                $sort_class = 'sorting_asc';
                                            elseif ($this->request->getQuery('direction') == 'desc') :
                                                $sort_class = 'sorting_desc';
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                        else :
                                            $sort_class = 'both';
                                        endif;
                                        ?>
                                        <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('must_complete_time') ?></th>
                                        
                                        <?php
                                        if ($this->request->getQuery('sort') == 'is_mise_en_place') :
                                            if ($this->request->getQuery('direction') == 'asc') :
                                                $sort_class = 'sorting_asc';
                                            elseif ($this->request->getQuery('direction') == 'desc') :
                                                $sort_class = 'sorting_desc';
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                        else :
                                            $sort_class = 'both';
                                        endif;
                                        ?>
                                        <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('is_mise_en_place') ?></th>
                                        
                                        <?php
                                        if ($this->request->getQuery('sort') == 'utensil_id') :
                                            if ($this->request->getQuery('direction') == 'asc') :
                                                $sort_class = 'sorting_asc';
                                            elseif ($this->request->getQuery('direction') == 'desc') :
                                                $sort_class = 'sorting_desc';
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                        else :
                                            $sort_class = 'both';
                                        endif;
                                        ?>
                                        <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('utensil_id') ?></th>
                                        
                                        <?php
                                        if ($this->request->getQuery('sort') == 'video_link') :
                                            if ($this->request->getQuery('direction') == 'asc') :
                                                $sort_class = 'sorting_asc';
                                            elseif ($this->request->getQuery('direction') == 'desc') :
                                                $sort_class = 'sorting_desc';
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                        else :
                                            $sort_class = 'both';
                                        endif;
                                        ?>
                                        <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('video_link') ?></th>
                                        
                                        <?php
                                        if ($this->request->getQuery('sort') == 'tip_description') :
                                            if ($this->request->getQuery('direction') == 'asc') :
                                                $sort_class = 'sorting_asc';
                                            elseif ($this->request->getQuery('direction') == 'desc') :
                                                $sort_class = 'sorting_desc';
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                        else :
                                            $sort_class = 'both';
                                        endif;
                                        ?>
                                        <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('tip_description') ?></th>
                                        
                                        <?php
                                        if ($this->request->getQuery('sort') == 'created') :
                                            if ($this->request->getQuery('direction') == 'asc') :
                                                $sort_class = 'sorting_asc';
                                            elseif ($this->request->getQuery('direction') == 'desc') :
                                                $sort_class = 'sorting_desc';
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                        else :
                                            $sort_class = 'both';
                                        endif;
                                        ?>
                                        <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('created') ?></th>
                                        
                                        <?php
                                        if ($this->request->getQuery('sort') == 'modified') :
                                            if ($this->request->getQuery('direction') == 'asc') :
                                                $sort_class = 'sorting_asc';
                                            elseif ($this->request->getQuery('direction') == 'desc') :
                                                $sort_class = 'sorting_desc';
                                            else :
                                                $sort_class = 'both';
                                            endif;
                                        else :
                                            $sort_class = 'both';
                                        endif;
                                        ?>
                                        <th scope="col" class="<?= $sort_class ?> sorting"><?= $this->Paginator->sort('modified') ?></th>
                                                                                <th scope="col" class="actions"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($recipeSteps as $key => $recipeStep): ?>
                                    <tr class="entity-row-<?= $key ?>">
                                                                                <td><?= $this->Number->format($recipeStep->recipe_step_id) ?></td>
                                                                                <td><?= $recipeStep->has('recipe') ? $this->Html->link($recipeStep->recipe->recipe_id, ['controller' => 'Recipes', 'action' => 'view', $recipeStep->recipe->recipe_id]) : '' ?></td>
                                                                                <td><?= $this->Number->format($recipeStep->order_number) ?></td>
                                                                                <td><?= h($recipeStep->step_description) ?></td>
                                                                                <td><?= h($recipeStep->has_timer) ?></td>
                                                                                <td><?= h($recipeStep->timer_time) ?></td>
                                                                                <td><?= h($recipeStep->must_complete_time) ?></td>
                                                                                <td><?= h($recipeStep->is_mise_en_place) ?></td>
                                                                                <td><?= $recipeStep->has('utensil') ? $this->Html->link($recipeStep->utensil->utensil_id, ['controller' => 'Utensils', 'action' => 'view', $recipeStep->utensil->utensil_id]) : '' ?></td>
                                                                                <td><?= h($recipeStep->video_link) ?></td>
                                                                                <td><?= h($recipeStep->tip_description) ?></td>
                                                                                <td><?= h($recipeStep->created) ?></td>
                                                                                <td><?= h($recipeStep->modified) ?></td>
                                                                                <td class="actions">
                                            <?= $this->Html->link(__('View'), ['action' => 'view', $recipeStep->recipe_step_id], ['class' => 'btn btn-primary btn-square btn-sm']) ?>
                                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $recipeStep->recipe_step_id], ['class' => 'btn btn-info btn-square btn-sm']) ?>
                                            <btn class="delete-entity btn btn-sm btn-square btn-danger" data-key="<?= $key ?>" data-csrf="<?= $this->request->getParam('_csrfToken') ?>" data-id="<?= $recipeStep->recipe_step_id ?>" data-delete-url="<?= $this->Url->build(['action' => 'delete', '_ext' => 'json']); ?>"><?= __('Delete') ?></btn>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                <div class="dataTables_info" role="status" aria-live="polite">
                                    <?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_paginate paging_simple_numbers">
                                    <ul class="pagination">
                                        <?= $this->Paginator->first() ?>
                                        <?= $this->Paginator->prev() ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next() ?>
                                        <?= $this->Paginator->last() ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php else : ?>
                    <h3 class="text-center"><?= __('There are no records yet. Click here to add one.') ?></h3>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>