<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RecipeUtensils Model
 *
 * @property \App\Model\Table\RecipesTable|\Cake\ORM\Association\BelongsTo $Recipes
 * @property \App\Model\Table\UtensilsTable|\Cake\ORM\Association\BelongsTo $Utensils
 *
 * @method \App\Model\Entity\RecipeUtensil get($primaryKey, $options = [])
 * @method \App\Model\Entity\RecipeUtensil newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RecipeUtensil[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RecipeUtensil|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RecipeUtensil|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RecipeUtensil patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RecipeUtensil[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RecipeUtensil findOrCreate($search, callable $callback = null, $options = [])
 */
class RecipeUtensilsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('recipe_utensils');
        $this->setDisplayField('recipe_utensil_id');
        $this->setPrimaryKey('recipe_utensil_id');

        $this->belongsTo('Recipes', [
            'foreignKey' => 'recipe_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Utensils', [
            'foreignKey' => 'utensil_id',
            'joinType' => 'LEFT',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('recipe_utensil_id')
            ->allowEmptyString('recipe_utensil_id', 'create');

        $validator
            ->integer('quantity')
            ->requirePresence('quantity', 'create')
            ->allowEmptyString('quantity', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['recipe_id'], 'Recipes'));
        $rules->add($rules->existsIn(['utensil_id'], 'Utensils'));

        return $rules;
    }
}
