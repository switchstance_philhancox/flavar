<?php
namespace App\Controller\Admin;

use App\Model\Entity\Utensil;

/**
 * Utensils Controller
 *
 * @property \App\Model\Table\UtensilsTable $Utensils
 */
class UtensilsController extends AppController
{
	
	public function index()
	{
        $search_term = $this->request->getQuery('search_term');

        $where = [];
        if ($search_term)
        {
            $where = [
                'OR' => [
                    'utensil_name LIKE ' => '%' . $search_term . '%'
                ]
            ];
        }

        $utensils_query = $this->Utensils->find()->where($where)->contain(['Images']);

        $utensils = $this->paginate($utensils_query);

        $this->set([
            'utensils' => $utensils,
            'search_term' => $search_term,
        ]);
        $this->set('_serialize', ['utensils']);
	}

    /**
    * @return \Cake\Http\Response|null
    */
	public function add()
	{
		$utensil = $this->Utensils->newEntity();
		if ($this->request->is('post'))
		{
            $data = $this->request->getData();

            if ($data['image_file']['name'] != null)
            {
                $image_id = $this->File->uploadFile($data['image_file'], 'uploads' . DS . 'utensils', ['admin_id' => $this->admin_id]);

                if (is_int($image_id))
                {
                    $data['image_id'] = $image_id;
                }
            }

			$utensil = $this->Utensils->patchEntity($utensil, $data);
			if ($this->Utensils->save($utensil))
			{
                $this->Flash->success(__('The utensil has been saved.'));

                return $this->redirect(['action' => 'index']);
			}
			else
			{
				$this->Flash->error(__('The utensil could not be saved. Please, try again.'));
			}
		}

        $this->set([
            'utensil' => $utensil,
        ]);

	}


	/**
    * @param null $utensil_id
    *
    * @return \Cake\Http\Response|null
    */
	public function edit($utensil_id = null)
	{
        $utensil = $this->Utensils->find()->where(['utensil_id' => $utensil_id])->contain(['Images'])->first();

        if (!$utensil)
        {
            $this->Flash->error('The $utensil could not be found. Please, try again.');
            return $this->redirect(['action' => 'index']);
        }
		 
		if ($this->request->is(['patch', 'post', 'put']))
		{
            $data = $this->request->getData();

            if (isset($data['remove_image_file']) && $data['remove_image_file'] == true)
            {
                $data['image_id'] = null;
            }

            if ($data['image_file']['name'] != null)
            {
                $image_id = $this->File->uploadFile($data['image_file'], 'uploads' . DS . 'utensils', ['admin_id' => $this->admin_id]);

                if (is_int($image_id))
                {
                    $data['image_id'] = $image_id;
                }
            }

			$utensil = $this->Utensils->patchEntity($utensil, $data);
			if ($this->Utensils->save($utensil))
			{
				$this->Flash->success(__('The utensil has been saved.'));
				return $this->redirect(['action' => 'index']);
			}
			else
			{
                $this->log($utensil->getErrors());
				$this->Flash->error(__('The utensil could not be saved. Please, try again.'));
			}
		}

        $this->set([
            'utensil' => $utensil,
        ]);
        $this->set('_serialize', ['utensils']);
	}

    /**
     * @return \Cake\Http\Response|null
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);

        if ($this->request->is('json'))
        {
            $utensil_id = $this->request->getData('id');

            /** @var Utensil $utensil */
            $utensil = $this->Utensils->find()->where(['utensil_id' => $utensil_id])->first();

            if (!$utensil)
            {
                $output = [
                    'status' => false,
                    'message' => __('The utensil could not be found. Please try again.'),
                ];
            }
            else
            {
                if (!$this->Utensils->delete($utensil))
                {
                    $this->log($utensil->getErrors());
                    $output = [
                        'status' => false,
                        'message' => __('There was a problem trying to delete the utensil. Please try again.'),
                    ];
                }
                else
                {
                    $output = [
                        'status' => true,
                        'message' => __('The utensil was successfully deleted.'),
                    ];
                }
            }

            $this->set(compact('output'));
            $this->set('_serialise', ['output']);
        }
        else
        {
            return $this->redirect('index');
        }
    }
}
