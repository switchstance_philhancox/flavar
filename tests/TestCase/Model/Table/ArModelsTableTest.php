<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ArModelsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ArModelsTable Test Case
 */
class ArModelsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ArModelsTable
     */
    public $ArModels;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ArModels',
        'app.Recipes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ArModels') ? [] : ['className' => ArModelsTable::class];
        $this->ArModels = TableRegistry::getTableLocator()->get('ArModels', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ArModels);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
