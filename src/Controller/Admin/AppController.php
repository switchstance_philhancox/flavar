<?php
namespace App\Controller\Admin;

use App\Controller\Component\FileComponent;
use App\Controller\Component\UtilComponent;
use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * @property FileComponent $File
 * @property UtilComponent $Util
 *
 * Class AppController
 * @package App\Controller\Admin
 */
class AppController extends Controller
{
    protected $admin_id;

    /**
     * @throws \Exception
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('File');
        $this->loadComponent('Flash');
        $this->loadComponent('Util');
        $this->loadComponent('Auth', [
            'authenticate'  => [
                'Form' => [
                    'userModel' => 'Admins',
                    'fields' => [
                        'username' => 'email_address',
                        'password' => 'admin_password',
                    ],
                ],
            ],
            'authError' => 'Please log in.',
            'loginAction'   => [
                'controller' => 'Dashboard',
                'action'     => 'login',
            ],
            'loginRedirect' => '/admin',
            'storage' => [
                'className' => 'Session',
                'key' => 'Auth.Admin',
            ],
        ]);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }

    /**
     * @param Event $event
     *
     * @return \Cake\Http\Response|null|void
     */
    public function beforeFilter(Event $event)
    {
        $this->setUserVariables();
    }

    /**
     * @param Event $event
     *
     * @return \Cake\Http\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->getType(), ['application/json', 'application/xml'])
        )
        {
            $this->set('_serialize', true);
        }
        $this->set('referer', $this->referer());
    }

    private function setUserVariables()
    {
        $admin = $this->Auth->user();
        if (!is_null($admin))
        {
            $this->admin_id = $admin['admin_id'];

            $this->set([
                'admin_id' => $this->admin_id,
            ]);
        }
    }
}