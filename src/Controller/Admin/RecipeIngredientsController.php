<?php
namespace App\Controller\Admin;

use App\Model\Table\QuantityTypesTable;
use App\Model\Table\RecipesTable;

/**
 * RecipeIngredients Controller
 *
 * @property QuantityTypesTable $QuantityTypes
 * @property RecipesTable $Recipes
 * @property \App\Model\Table\RecipeIngredientsTable $RecipeIngredients
 */
class RecipeIngredientsController extends AppController
{
    /**
     * @param null $recipe_id
     *
     * @return \Cake\Http\Response|null
     */
	public function edit($recipe_id = null)
	{
	    $this->loadModel('QuantityTypes');
	    $this->loadModel('Recipes');

	    $recipe = $this->Recipes->find()->where(['recipe_id' => $recipe_id])->first();

	    if (!$recipe)
        {
            $this->Flash->error('You cannot add ingredients to a recipe that doesn\'t exist.');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }

        $recipe_ingredients = $this->RecipeIngredients->find()->where(['recipe_id' => $recipe_id])->contain(['QuantityTypes'])->all();
		 
		if ($this->request->is(['patch', 'post', 'put']))
		{
            $data = $this->request->getData();

            unset($data['recipe_ingredients']['{{counter}}']);

            $recipe = $this->Recipes->patchEntity($recipe, $data, ['associated' => 'RecipeIngredients']);

            if (!$this->Recipes->save($recipe, ['associated' => 'RecipeIngredients']))
            {
                $this->Flash->error('There was a problem saving the ingredients list. Please try again.');
            }
            else
            {
                $this->Flash->success('The ingredients list was saved successfully.');
                return $this->redirect(['controller' => 'Recipes', 'action' => 'view', $recipe_id]);
            }
		}

		if (count($recipe_ingredients) == 0)
        {
            $recipe_ingredients = [];
            $recipe_ingredients[] = $this->RecipeIngredients->newEntity();
        }

		$new_recipe_ingredient = $this->RecipeIngredients->newEntity();

        $quantity_types = $this->QuantityTypes->find()->where(['is_deleted IS FALSE'])->all();

        $this->set([
            'new_recipe_ingredient' => $new_recipe_ingredient,
            'quantity_types' => $quantity_types,
            'recipe' => $recipe,
            'recipe_ingredients' => $recipe_ingredients,
        ]);

        $this->set('_serialize', ['recipe_ingredients']);
	}
}
