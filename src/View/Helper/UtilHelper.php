<?php
namespace App\View\Helper;

use App\Controller\Component\UtilComponent;
use App\Model\Entity\Recipe;
use App\Model\Entity\RecipeUtensil;
use Cake\View\Helper;
use Cake\Log\Log;
use Cake\Network\Request;
use Psr\Log\LogLevel;

/**
 * Class UtilHelper
 * @package App\View\Helper
 */
class UtilHelper extends Helper
{
    public $helpers = ['Form'];

    /**
     * @param $message
     */
    public function log($message)
    {
        Log::write(LogLevel::ERROR, $message);
    }

    /**
     * Return a tick or cross icon for boolean displays
     *
     * @param $true_or_false
     * @param $options
     *
     * @return string
     */
    public function getBooleanIcon($true_or_false, $options = [])
    {
        $options['icon_provider'] = isset($options['icon_provider']) ? $options['icon_provider'] : 'fa';
        $options['true_icon'] = isset($options['true_icon']) ? $options['true_icon'] : 'fa-check';
        $options['false_icon'] = isset($options['false_icon']) ? $options['false_icon'] : 'fa-times';
        $options['true_colour'] = isset($options['true_colour']) ? $options['true_colour'] : 'c-green';
        $options['false_colour'] = isset($options['false_colour']) ? $options['false_colour'] : 'c-red';
        $options['return_null_value'] = isset($options['return_null_value']) ? $options['return_null_value'] : false;

        $true_return = '<span class="' . $options['icon_provider'] . ' ' . $options['true_icon'] . ' ' . $options['true_colour'] . '"></span>';
        $false_return = '<span class="' . $options['icon_provider'] . ' ' . $options['false_icon'] . ' ' . $options['false_colour'] . '"></span>';


        if ($options['return_null_value'] !== false)
        {
            return $true_or_false === null ? $options['return_null_value'] : ($true_or_false ? $true_return : $false_return);
        }

        return $true_or_false ? $true_return : $false_return;
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    public function getMetaTitle(Request $request)
    {
        $controller_name = \Cake\Utility\Inflector::humanize(
            \Cake\Utility\Inflector::tableize($request->getParam('controller'))
        );

        $method_name = $request->getParam('action') == 'index'
            ? sprintf(__('All %s'), \Cake\Utility\Inflector::humanize(
                \Cake\Utility\Inflector::tableize($request->getParam('controller'))
            ))
            : ucfirst($request->getParam('action')) . ' ' . \Cake\Utility\Inflector::humanize(
                \Cake\Utility\Inflector::singularize(
                    \Cake\Utility\Inflector::tableize($request->getParam('controller'))
                )
            );

        if ($method_name == null && $controller_name == null) :
            $title = UtilComponent::APPLICATION_NAME;
        elseif ($method_name == null && $controller_name !== null) :
            $title = $controller_name . ' | ' . UtilComponent::APPLICATION_NAME;
        else :
            $title = $method_name . ' | ' . $controller_name . ' | ' . UtilComponent::APPLICATION_NAME;
        endif;

        return $title;
    }

    /**
     * @return string
     */
    public function getCurrentUrl()
    {
        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }

    /**
     * @param $subscriber_level
     *
     * @return null|string
     */
    public function returnSubscriberLevel($subscriber_level)
    {
        switch ($subscriber_level)
        {
            case Recipe::SUBSCRIBER_LEVEL_FREE :
                return __('Free');
            case Recipe::SUBSCRIBER_LEVEL_PREMIUM :
                return __('Premium');
            default :
                return '';
        }
    }

    /**
     * @param $difficulty
     *
     * @return null|string
     */
    public function returnDifficulty($difficulty)
    {
        switch ($difficulty)
        {
            case Recipe::DIFFICULTY_LEVEL_EASY :
                return __('Easy');
            case Recipe::DIFFICULTY_LEVEL_MEDIUM :
                return __('Medium');
            case Recipe::DIFFICULTY_LEVEL_HARD :
                return __('Hard');
            default :
                return '';
        }
    }

    /**
     * @param $is_live
     *
     * @return null|string
     */
    public function returnStatus($is_live)
    {
        return $is_live ? __('Live') :  __('Draft');
    }

    /**
     * @param $recipe_default_servings
     * @param $ingredient_quantity
     * @param $ingredient_extra_quantity
     * @param $quantity_name
     * @param $ingredient_name
     *
     * @return string
     */
    public function getIngredientLine($recipe_default_servings, $ingredient_quantity, $ingredient_extra_quantity, $quantity_name, $ingredient_name)
    {
        $extra_people = ($recipe_default_servings - 1);

        $total_quantity = $extra_people > 0 ? $ingredient_quantity + ($ingredient_extra_quantity * $extra_people) : $ingredient_quantity;

        if ($quantity_name == '-')
        {
            return $total_quantity . ' x ' . $ingredient_name;
        }
        else
        {
            return $total_quantity . $quantity_name . ' ' . $ingredient_name;
        }
    }

    public function formatFileSize($file_size_bytes, $decimals = 2)
    {
        $size = ['B','kB','MB','GB'];
        $factor = floor((strlen($file_size_bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $file_size_bytes / pow(1024, $factor)) . @$size[$factor];
    }

    /**
     * @param RecipeUtensil[] $recipe_utensils
     * @return array $recipe_utensil_list
     */
    public function getRecipeUtensilList($recipe_utensils)
    {
        $recipe_utensil_list = [];

        foreach ($recipe_utensils as $recipe_utensil)
        {
            $recipe_utensil_list[$recipe_utensil->utensil_id] = [
                'quantity' => isset($recipe_utensil_list[$recipe_utensil->utensil_id]) ? ($recipe_utensil_list[$recipe_utensil->utensil_id]['quantity'] + 1) : 1,
                'name' => $recipe_utensil->utensil->utensil_name,
            ];
        }

        return $recipe_utensil_list;
    }

}