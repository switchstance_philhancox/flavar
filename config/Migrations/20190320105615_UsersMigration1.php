<?php
use Migrations\AbstractMigration;

class UsersMigration1 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table
            ->addColumn('is_live', 'boolean', ['default' => false, 'after' => 'user_password'])
            ->addColumn('is_deleted', 'boolean', ['default' => false, 'after' => 'is_live'])
            ->update();
    }
}
