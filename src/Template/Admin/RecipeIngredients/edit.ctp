<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\QuantityType[] $quantity_types
 * @var \App\Model\Entity\Recipe $recipe
 * @var \App\Model\Entity\RecipeIngredient[] $recipe_ingredients
 * @var \App\Model\Entity\RecipeIngredient $new_recipe_ingredient
 */
?>

<div class="row">
    <div class="page-header pb-0">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                <span class="c-grey fw-normal"><?= __('Recipes') ?></span><br>
                <a href="<?= $this->Url->build(['controller' => 'Recipes', 'action' => 'view', $recipe->recipe_id]) ?>">
                    <?= $recipe->recipe_title ?></a> > <?= __('Ingredients') ?>
            </h2>
        </div>
    </div>
    <div class="col-md-12">
        <?php if ($recipe->can_servings_be_adjusted) : ?>
            <p><?= __('Each serving is for one person. It will be recalculated for the recipe based on the recipe\'s number of servings and your extra serving details.') ?></p>
        <?php else : ?>
            <p><?= sprintf(__('The ingredients for this recipe can not be adjusted. Please add the ingredients required for %d people.'), $recipe->default_number_of_servings) ?></p>
        <?php endif; ?>
    </div>
</div>

<div class="widget">
    <div class="widget-body">
        <form method="post" action="<?= $this->Url->build(['controller' => 'RecipeIngredients', 'action' => 'edit', $recipe->recipe_id]) ?>">
        <table class="table table-condensed table-bordered">
            <thead>
            <?php if ($recipe->can_servings_be_adjusted) : ?>
            <tr>
                <th colspan="3"></th>
                <th colspan="2"><?= __('For each additional serving, add:') ?></th>
                <th></th>
            </tr>
            <?php endif; ?>
            <tr>
                <th><?= __('Quantity') ?></th>
                <th><?= __('Quantity Type') ?></th>
                <th><?= __('Name') ?></th>
                <?php if ($recipe->can_servings_be_adjusted) : ?>
                <th><?= __('Quantity Number') ?></th>
                <th><?= __('Quantity Type') ?></th>
                <?php endif; ?>
                <th><?= __('') ?></th>
            </tr>
            </thead>
            <tbody id="recipe-ingredients-container">
                <?= $this->element('Arr' . DS . 'recipe-ingredient-row', ['key' => '{{counter}}', 'recipe_ingredient' => $new_recipe_ingredient]) ?>
            <?php foreach ($recipe_ingredients as $key => $recipe_ingredient) : ?>
                <?= $this->element('Arr' . DS . 'recipe-ingredient-row', ['key' => $key, 'recipe_ingredient' => $recipe_ingredient]) ?>
            <?php endforeach; ?>
            </tbody><tfoot>
            <tr>
                <td colspan="6">
                    <a class="btn btn-secondary btn-square btn-sm arr-add-row"
                       data-container-id="recipe-ingredients-container" data-minimum-rows="2">
                        <?= __('Add another ingredient') ?>
                    </a>
                </td>
            </tr>
            </tfoot>
        </table>

            <div class="row">
                <div class="col-md-12">
                    <input type="submit" value="<?= __('Save ingredients') ?>" class="pull-right btn btn-lg btn-primary btn-square">
                </div>
            </div>
        </form>
    </div>
</div>
