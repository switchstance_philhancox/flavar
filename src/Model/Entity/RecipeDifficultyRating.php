<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RecipeDifficultyRating Entity
 *
 * @property int $recipe_difficulty_rating_id
 * @property int $recipe_id
 * @property int $difficult_rating
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Recipe $recipe
 * @property \App\Model\Entity\User $user
 */
class RecipeDifficultyRating extends Entity
{

    const RATING_EASY = 1;
    const RATING_MEDIUM = 2;
    const RATING_DIFFICULT = 3;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];
}
