<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Recipes Model
 *
 * @property \App\Model\Table\AdminsTable|\Cake\ORM\Association\BelongsTo $Admins
 * @property \App\Model\Table\ImagesTable|\Cake\ORM\Association\BelongsTo $Images
 * @property \App\Model\Table\CategoriesTable|\Cake\ORM\Association\BelongsTo $Categories
 *
 * @method \App\Model\Entity\Recipe get($primaryKey, $options = [])
 * @method \App\Model\Entity\Recipe newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Recipe[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Recipe|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Recipe|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Recipe patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Recipe[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Recipe findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RecipesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('recipes');
        $this->setDisplayField('recipe_id');
        $this->setPrimaryKey('recipe_id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Admins', [
            'foreignKey' => 'admin_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Images', [
            'foreignKey' => 'featured_image_id'
        ]);
        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id'
        ]);
        $this->hasMany('RecipeIngredients', [
            'foreignKey' => 'recipe_id',
            'saveStrategy' => 'replace',
        ]);
        $this->hasMany('RecipeUtensils', [
            'foreignKey' => 'recipe_id',
            'saveStrategy' => 'replace',
        ]);
        $this->belongsTo('RecipePlatingDescriptions', [
            'foreignKey' => 'recipe_plating_description_id',
        ]);
        $this->hasMany('RecipeSteps', [
            'foreignKey' => 'recipe_id',
        ]);
        $this->hasMany('RecipeImages', [
            'foreignKey' => 'recipe_id',
        ]);
        $this->belongsTo('ArModels', [
            'foreignKey' => 'ar_model_id'
        ]);
        $this->hasMany('RecipeDifficultyRatings', [
            'foreignKey' => 'recipe_id',
        ]);
        $this->hasMany('RecipeRatings', [
            'foreignKey' => 'recipe_id',
        ]);
        $this->hasMany('RecipeTags', [
            'foreignKey' => 'recipe_id',
            'saveStrategy' => 'replace',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('recipe_id')
            ->allowEmptyString('recipe_id', 'create');

        $validator
            ->scalar('recipe_title')
            ->maxLength('recipe_title', 255)
            ->requirePresence('recipe_title', 'create')
            ->allowEmptyString('recipe_title', false);

        $validator
            ->boolean('is_live')
            ->requirePresence('is_live', 'create')
            ->allowEmptyString('is_live', false);

        $validator
            ->boolean('is_deleted')
            ->requirePresence('is_deleted', 'create')
            ->allowEmptyString('is_deleted', false);

        $validator
            ->integer('subscriber_level')
            ->requirePresence('subscriber_level', 'create')
            ->allowEmptyString('subscriber_level', false);

        $validator
            ->scalar('description')
            ->maxLength('description', 2500)
            ->allowEmptyString('description');

        $validator
            ->integer('difficulty_level')
            ->requirePresence('difficulty_level', 'create')
            ->allowEmptyString('difficulty_level', false);

        $validator
            ->integer('default_number_of_servings')
            ->requirePresence('default_number_of_servings', 'create')
            ->allowEmptyString('default_number_of_servings', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['admin_id'], 'Admins'));
        $rules->add($rules->existsIn(['featured_image_id'], 'Images'));
        $rules->add($rules->existsIn(['category_id'], 'Categories'));

        return $rules;
    }
}
