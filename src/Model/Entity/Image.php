<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Routing\Router;

/**
 * Image Entity
 *
 * @property int $image_id
 * @property string $image_name
 * @property string $image_size
 * @property string $image_url
 * @property string $alt_text
 * @property int $admin_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $updated
 *
 * @property \App\Model\Entity\Admin $admin
 */
class Image extends Entity
{
    protected $_virtual = ['image_full_url'];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];

    public function _getImageFullUrl() {
        $request = Router::getRequest();
        return $request->scheme() . '://' . $request->host() . '/' . $this->image_url;
    }


}
