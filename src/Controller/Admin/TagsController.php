<?php
namespace App\Controller\Admin;

use App\Model\Entity\Tag;

/**
 * Tags Controller
 *
 * @property \App\Model\Table\TagsTable $Tags
 */
class TagsController extends AppController
{
	
	public function index()
	{
        $search_term = $this->request->getQuery('search_term');

        $where = [];
        if ($search_term)
        {
            $where = [
                'OR' => [
                    'tag_name LIKE ' => '%' . $search_term . '%'
                ]
            ];
        }

        $tags_query = $this->Tags->find()->where($where);

        $tags = $this->paginate($tags_query);

        $this->set([
            'tags' => $tags,
            'search_term' => $search_term,
        ]);
        $this->set('_serialize', ['tags']);
	}

    /**
    * @param null $tag_id
    *
    * @return \Cake\Http\Response|null
    */
    public function view($tag_id = null)
    {
        $tag = $this->Tags->find()
            ->where(['Tags.tag_id' => $tag_id])
            ->first();

        if (!$tag)
        {
            $this->Flash->error('The record could not be found. Please try again.');
            return $this->redirect(['action' => 'index']);
        }

        $this->set([
            'tag' => $tag
        ]);
    }

    /**
    * @return \Cake\Http\Response|null
    */
	public function add()
	{
		$tag = $this->Tags->newEntity();
		if ($this->request->is('post'))
		{
            $data = $this->request->getData();
			$tag = $this->Tags->patchEntity($tag, $data);
			if ($this->Tags->save($tag))
			{
				$this->Flash->success(__('The tag has been saved.'));
				return $this->redirect(['action' => 'index']);
			}
			else
			{
				$this->Flash->error(__('The tag could not be saved. Please, try again.'));
			}
		}

		
        $this->set([
            'tag' => $tag,
        ]);
	}


	/**
    * @param null $tag_id
    *
    * @return \Cake\Http\Response|null
    */
	public function edit($tag_id = null)
	{
        $tag = $this->Tags->find()->where(['tag_id' => $tag_id])->first();

        if (!$tag)
        {
            $this->Flash->error('The $tag could not be found. Please, try again.');
            return $this->redirect(['action' => 'index']);
        }
		 
		if ($this->request->is(['patch', 'post', 'put']))
		{
            $data = $this->request->getData();
			$tag = $this->Tags->patchEntity($tag, $data);
			if ($this->Tags->save($tag))
			{
				$this->Flash->success(__('The tag has been saved.'));
				return $this->redirect(['action' => 'index']);
			}
			else
			{
                $this->log($tag->getErrors());
				$this->Flash->error(__('The tag could not be saved. Please, try again.'));
			}
		}

		
        $this->set([
            'tag' => $tag,
        ]);
        $this->set('_serialize', ['tags']);
	}

    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);

        if ($this->request->is('json'))
        {
            $tag_id = $this->request->getData('id');

            /** @var Tag $tag */
            $tag = $this->Tags->find()->where(['tag_id' => $tag_id])->first();

            if (!$tag)
            {
                $output = [
                    'status' => false,
                    'message' => __('The tag could not be found. Please try again.'),
                ];
            }
            else
            {
                if ($this->Tags->delete($tag))
                {
                    $output = [
                        'status' => true,
                    ];
                }
                else
                {
                    $this->log($tag->getErrors());
                    $output = [
                        'status' => false,
                        'message' => __('There was a problem trying to delete the tag. Please try again.'),
                    ];
                }
            }

            $this->set(compact('output'));
            $this->set('_serialise', ['output']);
        }
        else
        {
            return $this->redirect('index');
        }
    }
}
