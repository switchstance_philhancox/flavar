<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\QuantityType $quantityType
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Quantity Type'), ['action' => 'edit', $quantityType->quantity_type_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Quantity Type'), ['action' => 'delete', $quantityType->quantity_type_id], ['confirm' => __('Are you sure you want to delete # {0}?', $quantityType->quantity_type_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Quantity Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Quantity Type'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="quantityTypes view large-9 medium-8 columns content">
    <h3><?= h($quantityType->quantity_type_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Quantity Type Name') ?></th>
            <td><?= h($quantityType->quantity_type_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Quantity Type Id') ?></th>
            <td><?= $this->Number->format($quantityType->quantity_type_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($quantityType->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($quantityType->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Deleted') ?></th>
            <td><?= $quantityType->is_deleted ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
