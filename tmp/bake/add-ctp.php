<?php
$fields = collection($fields)
->filter(function($field) use ($schema) {
return !in_array($schema->columnType($field), ['binary', 'text']);
});

if (isset($modelObject) && $modelObject->behaviors()->has('Tree')) {
$fields = $fields->reject(function ($field) {
return $field === 'lft' || $field === 'rght';
});
}

if (!empty($indexColumns)) {
$fields = $fields->take($indexColumns);
}

?>

<!-- Begin Page Header-->
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title"><CakePHPBakeOpenTag= __('<?= $pluralHumanName ?>') CakePHPBakeCloseTag></h2>
        </div>
    </div>
</div>
<!-- End Page Header -->




	<CakePHPBakeOpenTag= $this->element('Forms'.DS.'<?= str_replace(' ', '', $pluralHumanName) ?>'.DS.'<?= strtolower(str_replace(' ', '-', $pluralHumanName)) ?>-form', ['method' => 'add']) CakePHPBakeCloseTag>

	<CakePHPBakeOpenTagphp
	/*********************************************************
	 *
	 * If you prefer, the below should be placed in an element
	 * file as described above so it can be shared between
	 * add/edit view.
	 *
	 * Then delete the form below up until "end of element"
	 *
	 *********************************************************/
	CakePHPBakeCloseTag>

<CakePHPBakeOpenTagphp
/**
 * @var \<?= $namespace ?>\View\AppView $this
 * @var string $method
 * @var <?= $entityClass ?> $<?= lcfirst(str_replace(' ', '', $singularHumanName)) ?>
 */
CakePHPBakeCloseTag>

<div class="row">
    <div class="col-xl-12">
        <!-- Default -->
        <div class="widget has-shadow">
            <div class="widget-header bordered no-actions d-flex align-items-center">
                <h2><CakePHPBakeOpenTag= $method == 'add' ? __('Add New <?= $singularHumanName ?>') : __('Edit <?= $singularHumanName ?>') CakePHPBakeCloseTag></h2>
            </div>
            <div class="widget-body">
                <CakePHPBakeOpenTag= $this->Form->create($<?= lcfirst(str_replace(' ', '', $singularHumanName)) ?>); CakePHPBakeCloseTag>

                <?php foreach ($fields as $field): ?>
                <CakePHPBakeOpenTag= $this->Form->control('<?= $field ?>') CakePHPBakeCloseTag>
                <?php endforeach; ?>

                <div class="text-right">
                    <input class="btn btn-primary btn-square" type="submit" value="<CakePHPBakeOpenTag= __('Save Record') CakePHPBakeCloseTag>">
                </div>

                <CakePHPBakeOpenTag= $this->Form->end() CakePHPBakeCloseTag>
            </div>
        </div>
    </div>
</div>

<CakePHPBakeOpenTagphp
/*********************************************************
 * End of element
 *********************************************************/

