<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Recipe Entity
 *
 * @property int $recipe_id
 * @property string $recipe_title
 * @property bool $is_live
 * @property bool $is_deleted
 * @property int $admin_id
 * @property int $subscriber_level
 * @property string|null $description
 * @property string|null $total_time_estimate
 * @property int|null $featured_image_id
 * @property int $difficulty_level
 * @property int $default_number_of_servings
 * @property boolean $can_servings_be_adjusted
 * @property int|null $category_id
 * @property integer|null $recipe_plating_description_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Admin $admin
 * @property \App\Model\Entity\Image $image
 * @property \App\Model\Entity\Category $category
 * @property RecipeIngredient[] $recipe_ingredients
 * @property RecipePlatingDescription $recipe_plating_description
 * @property ArModel $ar_model
 * @property RecipeStep[] $recipe_steps
 * @property RecipeUtensil[] $recipe_utensils
 * @property RecipeImage[] $recipe_images
 * @property RecipeTag[] $recipe_tags
 */
class Recipe extends Entity
{
    const SUBSCRIBER_LEVEL_FREE = 1;
    const SUBSCRIBER_LEVEL_PREMIUM = 2;

    const DIFFICULTY_LEVEL_EASY = 1;
    const DIFFICULTY_LEVEL_MEDIUM = 2;
    const DIFFICULTY_LEVEL_HARD = 3;

    protected $_virtual = ['difficulty'];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];

    public function _getDifficulty()
    {
        switch($this->difficulty_level)
        {
            case Recipe::DIFFICULTY_LEVEL_EASY:
                return 'Easy';
            case Recipe::DIFFICULTY_LEVEL_MEDIUM:
                return 'Medium';
            case Recipe::DIFFICULTY_LEVEL_HARD:
                return 'Hard';
            default:
                return null;
        }
    }
}
