
<!-- Begin Page Header-->
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title"><?= __('Recipe Images') ?></h2>
        </div>
    </div>
</div>
<!-- End Page Header -->

<?= $this->element('Forms'.DS.'RecipeImages'.DS.'recipe-images-form', ['method' => 'add']) ?>
