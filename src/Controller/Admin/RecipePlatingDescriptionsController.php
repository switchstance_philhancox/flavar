<?php
namespace App\Controller\Admin;

use App\Model\Entity\Recipe;
use App\Model\Table\RecipesTable;

/**
 * RecipePlatingDescriptions Controller
 *
 * @property \App\Model\Table\RecipePlatingDescriptionsTable $RecipePlatingDescriptions
 * @property RecipesTable $Recipes
 */
class RecipePlatingDescriptionsController extends AppController
{
    /**
     * @param null $recipe_id
     *
     * @return \Cake\Http\Response|null
     */
	public function edit($recipe_id = null)
	{
        $this->loadModel('Recipes');

        /** @var Recipe $recipe */
        $recipe = $this->Recipes->find()->where(['recipe_id' => $recipe_id])->first();

        if (!$recipe)
        {
            $this->Flash->error('You cannot add plating descriptions to a recipe that doesn\'t exist.');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }

        $recipe_plating_description = $this->RecipePlatingDescriptions
            ->find()
            ->where(['recipe_plating_description_id' => $recipe->recipe_plating_description_id])
            ->contain(['Images'])
            ->first();

        if (!$recipe_plating_description)
        {
            $recipe_plating_description = $this->RecipePlatingDescriptions->newEntity();
        }
		 
		if ($this->request->is(['patch', 'post', 'put']))
		{
            $data = $this->request->getData();
            if (isset($data['remove_image_file']) && $data['remove_image_file'] == true)
            {
                $data['plating_image_id'] = null;
            }

            if ($data['image_file']['name'] != null)
            {
                $image_id = $this->File->uploadFile($data['image_file'], 'uploads' . DS . 'recipes', ['admin_id' => $this->admin_id]);

                if (is_int($image_id))
                {
                    $data['plating_image_id'] = $image_id;
                }
            }

            $recipe_plating_description = $this->RecipePlatingDescriptions->patchEntity($recipe_plating_description, $data);
			if ($this->RecipePlatingDescriptions->save($recipe_plating_description))
			{
                $recipe->recipe_plating_description_id = $recipe_plating_description->recipe_plating_description_id;

                if (!$this->Recipes->save($recipe))
                {
                    $this->log($recipe->getErrors());
                }

				$this->Flash->success(__('The recipe plating description has been saved.'));
				return $this->redirect(['controller' => 'Recipes', 'action' => 'view', $recipe_id]);
			}
			else
			{
                $this->log($recipe_plating_description->getErrors());
				$this->Flash->error(__('The recipe plating description could not be saved. Please, try again.'));
			}
		}
		
        $this->set([
            'recipe_plating_description' => $recipe_plating_description,
        ]);
        $this->set('_serialize', ['recipePlatingDescriptions']);
	}
}
