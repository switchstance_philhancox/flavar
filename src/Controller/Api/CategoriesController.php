<?php

namespace App\Controller\Api;

use App\Model\Entity\RecipeDifficultyRating;
use Cake\ORM\Query;

/**
 * Categories Controller
 *
 * @property \App\Model\Table\CategoriesTable           $Categories
 */
class CategoriesController extends AppController
{

    public function index()
    {
        $q = $this->Categories->find()
            ->select([
                'Categories.category_id',
                'Categories.category_name'
            ]);

        $categories = [];

        foreach($q as $row)
        {
            $row->recipes = $this->Categories->Recipes->find()
                ->select([
                    'Recipes.recipe_id',
                    'Recipes.category_id',
                    'Recipes.recipe_title',
                    'Recipes.subscriber_level',
                    'Recipes.difficulty_level',
                    'Images.image_url'
                ])->where([
                    'Recipes.category_id' => $row->category_id,
                    'Recipes.is_live' => true,
                    'Recipes.is_deleted' => false
                ])->contain(['Images'])
                ->order(['Recipes.recipe_id ASC'])
                ->limit(6)
                ->toArray();

            if(!empty($row['recipes']))
            {
                $categories[] = $row;
            }
        }

        echo json_encode([
            'categories' => $categories
        ]);
        die;
    }


}
