<?php
use Migrations\AbstractMigration;

class RecipesMigration1 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $images_table = $this->table('images', ['id' => false, 'primary_key' => 'image_id']);
        $images_table
            ->addColumn('image_id', 'integer', ['signed' => false, 'limit' => 11, 'identity' => true])
            ->addColumn('image_name', 'string', ['limit' => 255])
            ->addColumn('image_size', 'string', ['limit' => 255])
            ->addColumn('image_url', 'string', ['limit' => 255])
            ->addColumn('alt_text', 'string', ['limit' => 255])
            ->addColumn('admin_id', 'integer', ['limit' => 11, 'signed' => false])
            ->addForeignKey('admin_id', 'admins', 'admin_id', ['update' => 'CASCADE', 'delete' => 'RESTRICT'])
            ->addColumn('created', 'datetime')
            ->addColumn('updated', 'datetime')
            ->create();

        $categories_table = $this->table('categories', ['id' => false, 'primary_key' => 'category_id']);
        $categories_table
            ->addColumn('category_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
            ->addColumn('category_name', 'string', ['limit' => 255])
            ->addColumn('created', 'datetime')
            ->addColumn('updated', 'datetime')
            ->create();

        $recipe_plating_descriptions_table = $this->table('recipe_plating_descriptions', ['id' => false, 'primary_key' => 'recipe_plating_description_id']);
        $recipe_plating_descriptions_table
            ->addColumn('recipe_plating_description_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
            ->addColumn('plating_image_id', 'integer', ['limit' => 11, 'signed' => false, 'null' => true])
            ->addForeignKey('plating_image_id', 'images', 'image_id', ['update' => 'CASCADE', 'delete' => 'CASCADE'])
            ->addColumn('plating_description', 'string', ['limit' => 2000, 'null' => true])
            ->create();

        $recipes_table = $this->table('recipes', ['id' => false, 'primary_key' => 'recipe_id']);
        $recipes_table
            ->addColumn('recipe_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
            ->addColumn('recipe_title', 'string', ['limit' => 255])
            ->addColumn('is_live', 'boolean', ['default' => false])
            ->addColumn('is_deleted', 'boolean', ['default' => false])
            ->addColumn('admin_id', 'integer', ['limit' => 11, 'signed' => false])
            ->addForeignKey('admin_id', 'admins', 'admin_id', ['update' => 'CASCADE', 'delete' => 'RESTRICT'])
            ->addColumn('subscriber_level', 'integer', ['limit' => 11])
            ->addColumn('description', 'string', ['limit' => 2500, 'null' => true])
            ->addColumn('ar_model', 'string', ['limit' => 255, 'null' => true])
            ->addColumn('featured_image_id', 'integer', ['limit' => 11, 'signed' => false, 'null' => true])
            ->addForeignKey('featured_image_id', 'images', 'image_id', ['update' => 'CASCADE', 'delete' => 'SET_NULL'])
            ->addColumn('difficulty_level', 'integer', ['limit' => 11])
            ->addColumn('default_number_of_servings', 'integer', ['limit' => 11])
            ->addColumn('category_id', 'integer', ['limit' => 11, 'signed' => false, 'null' => true])
            ->addForeignKey('category_id', 'categories', 'category_id', ['update' => 'CASCADE', 'delete' => 'SET_NULL'])
            ->addColumn('recipe_plating_description_id', 'integer', ['limit' => 11, 'signed' => false, 'null' => true])
            ->addForeignKey('recipe_plating_description_id', 'recipe_plating_descriptions', 'recipe_plating_description_id', ['update' => 'CASCADE', 'delete' => 'CASCADE'])
            ->addColumn('created', 'datetime')
            ->addColumn('modified', 'datetime')
            ->create();

        $recipe_images_table = $this->table('recipe_images', ['id' => false, 'primary_key' => 'recipe_image_id']);
        $recipe_images_table
            ->addColumn('recipe_image_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
            ->addColumn('recipe_id', 'integer', ['limit' => 11, 'signed' => false])
            ->addForeignKey('recipe_id', 'recipes', 'recipe_id', ['update' => 'CASCADE', 'delete' => 'CASCADE'])
            ->addColumn('image_id', 'integer', ['limit' => 11, 'signed' => false])
            ->addForeignKey('image_id', 'images', 'image_id', ['update' => 'CASCADE', 'delete' => 'CASCADE'])
            ->create();

        $tags_table = $this->table('tags', ['id' => false, 'primary_key' => 'tag_id']);
        $tags_table
            ->addColumn('tag_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
            ->addColumn('tag_name', 'string', ['limit' => 255])
            ->addColumn('created', 'datetime')
            ->addColumn('modified', 'datetime')
            ->create();

        $recipe_tags_table = $this->table('recipe_tags', ['id' => false, 'primary_key' => 'recipe_tag_id']);
        $recipe_tags_table
            ->addColumn('recipe_tag_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
            ->addColumn('recipe_id', 'integer', ['limit' => 11, 'signed' => false])
            ->addForeignKey('recipe_id', 'recipes', 'recipe_id', ['update' => 'CASCADE', 'delete' => 'CASCADE'])
            ->addColumn('tag_id', 'integer', ['limit' => 11, 'signed' => false])
            ->addForeignKey('tag_id', 'tags', 'tag_id', ['update' => 'CASCADE', 'delete' => 'CASCADE'])
            ->create();

        $quantity_types_table = $this->table('quantity_types', ['id' => false, 'primary_key' => 'quantity_type_id']);
        $quantity_types_table
            ->addColumn('quantity_type_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
            ->addColumn('quantity_type_name', 'string', ['limit' => 255])
            ->addColumn('is_deleted', 'boolean', ['default' => false])
            ->addColumn('created', 'datetime')
            ->addColumn('modified', 'datetime')
            ->create();

        $recipe_ingredients_table = $this->table('recipe_ingredients', ['id' => false, 'primary_key' => 'recipe_ingredient_id']);
        $recipe_ingredients_table
            ->addColumn('recipe_ingredient_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
            ->addColumn('recipe_id', 'integer', ['limit' => 11, 'signed' => false])
            ->addForeignKey('recipe_id', 'recipes', 'recipe_id', ['update' => 'CASCADE', 'delete' => 'CASCADE'])
            ->addColumn('quantity', 'integer', ['limit' => 11])
            ->addColumn('quantity_type_id', 'integer', ['limit' => 11, 'signed' => false])
            ->addForeignKey('quantity_type_id', 'quantity_types', 'quantity_type_id', ['update' => 'CASCADE', 'delete' => 'CASCADE'])
            ->addColumn('ingredient_name', 'string', ['limit' => 255])
            ->addColumn('quantity_per_additional_serving', 'integer', ['limit' => 11])
            ->addColumn('created', 'datetime')
            ->addColumn('modified', 'datetime')
            ->create();

        $utensils_table = $this->table('utensils', ['id' => false, 'primary_key' => 'utensil_id']);
        $utensils_table
            ->addColumn('utensil_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
            ->addColumn('utensil_name', 'string', ['limit' => 255])
            ->addColumn('image_id', 'integer', ['limit' => 11, 'signed' => false, 'null' => true])
            ->addForeignKey('image_id', 'images', 'image_id', ['update' => 'CASCADE', 'delete' => 'SET_NULL'])
            ->addColumn('created', 'datetime')
            ->addColumn('modified', 'datetime')
            ->create();

        $recipe_utensils_table = $this->table('recipe_utensils', ['id' => false, 'primary_key' => 'recipe_utensil_id']);
        $recipe_utensils_table
            ->addColumn('recipe_utensil_id', 'integer', ['limit' => 11, 'signed' => false])
            ->addColumn('recipe_id', 'integer', ['limit' => 11, 'signed' => false])
            ->addForeignKey('recipe_id', 'recipes', 'recipe_id', ['update' => 'CASCADE', 'delete' => 'CASCADE'])
            ->addColumn('quantity', 'integer', ['limit' => 11])
            ->addColumn('utensil_id', 'integer', ['limit' => 11, 'signed' => false])
            ->addForeignKey('utensil_id', 'utensils', 'utensil_id', ['update' => 'CASCADE', 'delete' => 'CASCADE'])
            ->create();

        $recipe_steps_table = $this->table('recipe_steps', ['id' => false, 'primary_key' => 'recipe_step_id']);
        $recipe_steps_table
            ->addColumn('recipe_step_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
            ->addColumn('recipe_id', 'integer', ['limit' => 11, 'signed' => false])
            ->addForeignKey('recipe_id', 'recipes', 'recipe_id', ['update' => 'CASCADE', 'delete' => 'CASCADE'])
            ->addColumn('order_number', 'integer', ['limit' => 11, 'null' => true])
            ->addColumn('step_description', 'string', ['limit' => 2500])
            ->addColumn('has_timer', 'boolean', ['default' => false])
            ->addColumn('timer_time', 'string', ['limit' => 5, 'null' => false])
            ->addColumn('must_complete_time', 'boolean', ['default' => false])
            ->addColumn('is_mise_en_place', 'boolean', ['default' => false])
            ->addColumn('utensil_id', 'integer', ['limit' => 11, 'signed' => false, 'null' => true])
            ->addForeignKey('utensil_id', 'utensils', 'utensil_id', ['update' => 'CASCADE', 'delete' => 'SET_NULL'])
            ->addColumn('video_link', 'string', ['limit' => 2500, 'null' => true])
            ->addColumn('tip_description', 'string', ['limit' => 2500, 'null' => true])
            ->addColumn('created', 'datetime')
            ->addColumn('modified', 'datetime')
            ->create();
    }
}
