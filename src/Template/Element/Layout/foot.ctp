<!-- Begin Vendor Js -->
<script src="<?= $this->Url->build('/', true) ?>vendor/elisyam/js/base/jquery.min.js"></script>
<script src="<?= $this->Url->build('/', true) ?>vendor/elisyam/js/base/core.min.js"></script>
<script src="<?= $this->Url->build('/', true) ?>assets/vendors/js/nicescroll/nicescroll.min.js"></script>
<script src="<?= $this->Url->build('/', true) ?>assets/vendors/js/noty/noty.min.js"></script>
<script src="<?= $this->Url->build('/', true) ?>vendor/elisyam/js/app/app.min.js"></script>
<script src="<?= $this->Url->build('/', true) ?>vendor/sweetalert/js/sweetalert.js"></script>
<script src="<?= $this->Url->build('/', true) ?>vendor/bootstrap-select/js/bootstrap-select.js"></script>

<script src="<?= $this->Url->build('/', true) ?>js/jquery-ui.js"></script>
<script src="<?= $this->Url->build('/', true) ?>js/admin.js"></script>
<script src="<?= $this->Url->build('/', true) ?>js/add-remove-row.js"></script>
<script src="<?= $this->Url->build('/', true) ?>js/jquery.mask.js"></script>