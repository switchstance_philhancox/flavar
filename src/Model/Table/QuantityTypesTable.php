<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * QuantityTypes Model
 *
 * @method \App\Model\Entity\QuantityType get($primaryKey, $options = [])
 * @method \App\Model\Entity\QuantityType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\QuantityType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\QuantityType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\QuantityType|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\QuantityType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\QuantityType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\QuantityType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class QuantityTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('quantity_types');
        $this->setDisplayField('quantity_type_id');
        $this->setPrimaryKey('quantity_type_id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('quantity_type_id')
            ->allowEmptyString('quantity_type_id', 'create');

        $validator
            ->scalar('quantity_type_name')
            ->maxLength('quantity_type_name', 255)
            ->requirePresence('quantity_type_name', 'create')
            ->allowEmptyString('quantity_type_name', false);

        $validator
            ->boolean('is_deleted')
            ->requirePresence('is_deleted', 'create')
            ->allowEmptyString('is_deleted', false);

        return $validator;
    }
}
