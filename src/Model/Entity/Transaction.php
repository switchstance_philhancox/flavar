<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Transaction Entity
 *
 * @property int $transaction_id
 * @property int $store_transaction_id
 * @property int $user_id
 * @property bool $is_apple
 * @property string $receipt_data
 * @property \Cake\I18n\FrozenTime $purchase_date
 * @property \Cake\I18n\FrozenTime $expiry_date
 *
 * @property \App\Model\Entity\StoreTransaction $store_transaction
 * @property \App\Model\Entity\User $user
 */
class Transaction extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'store_transaction_id' => true,
        'user_id' => true,
        'is_apple' => true,
        'receipt_data' => true,
        'purchase_date' => true,
        'expiry_date' => true,
        'store_transaction' => true,
        'user' => true
    ];
}
