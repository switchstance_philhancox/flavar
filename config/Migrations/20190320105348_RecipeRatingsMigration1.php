<?php
use Migrations\AbstractMigration;

class RecipeRatingsMigration1 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('recipe_ratings', ['id' => false, 'primary_key' => 'recipe_rating_id']);
        $table
            ->addColumn('recipe_rating_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
            ->addColumn('recipe_id', 'integer', ['limit' => 11, 'signed' => false])
            ->addForeignKey('recipe_id', 'recipes', 'recipe_id', ['update' => 'CASCADE', 'delete' => 'CASCADE'])
            ->addColumn('rating', 'integer', ['limit' => 11])
            ->addColumn('user_id', 'integer', ['limit' => 11, 'signed' => false])
            ->addForeignKey('user_id', 'users', 'user_id', ['update' => 'CASCADE', 'delete' => 'CASCADE'])
            ->addColumn('created', 'datetime')
            ->addColumn('modified', 'datetime')
            ->create();
    }
}
