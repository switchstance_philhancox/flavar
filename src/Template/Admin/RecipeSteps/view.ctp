<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RecipeStep $recipeStep
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Recipe Step'), ['action' => 'edit', $recipeStep->recipe_step_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Recipe Step'), ['action' => 'delete', $recipeStep->recipe_step_id], ['confirm' => __('Are you sure you want to delete # {0}?', $recipeStep->recipe_step_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Recipe Steps'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recipe Step'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Recipes'), ['controller' => 'Recipes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recipe'), ['controller' => 'Recipes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Utensils'), ['controller' => 'Utensils', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Utensil'), ['controller' => 'Utensils', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="recipeSteps view large-9 medium-8 columns content">
    <h3><?= h($recipeStep->recipe_step_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Recipe') ?></th>
            <td><?= $recipeStep->has('recipe') ? $this->Html->link($recipeStep->recipe->recipe_id, ['controller' => 'Recipes', 'action' => 'view', $recipeStep->recipe->recipe_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step Description') ?></th>
            <td><?= h($recipeStep->step_description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Timer Time') ?></th>
            <td><?= h($recipeStep->timer_time) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Utensil') ?></th>
            <td><?= $recipeStep->has('utensil') ? $this->Html->link($recipeStep->utensil->utensil_id, ['controller' => 'Utensils', 'action' => 'view', $recipeStep->utensil->utensil_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Video Link') ?></th>
            <td><?= h($recipeStep->video_link) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tip Description') ?></th>
            <td><?= h($recipeStep->tip_description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Recipe Step Id') ?></th>
            <td><?= $this->Number->format($recipeStep->recipe_step_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Number') ?></th>
            <td><?= $this->Number->format($recipeStep->order_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($recipeStep->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($recipeStep->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Has Timer') ?></th>
            <td><?= $recipeStep->has_timer ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Must Complete Time') ?></th>
            <td><?= $recipeStep->must_complete_time ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Mise En Place') ?></th>
            <td><?= $recipeStep->is_mise_en_place ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
