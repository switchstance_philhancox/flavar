<?php
use Migrations\AbstractMigration;

class RecipeStepsMigration1 extends AbstractMigration
{

    public function up()
    {
        $recipe_utensils_table = $this->table('recipe_utensils');
        $recipe_utensils_table
            ->addColumn('internal_name', 'string', ['limit' => 255, 'null' => true])
            ->update();

        $recipe_steps_table = $this->table('recipe_steps');
        $recipe_steps_table
            ->addColumn('recipe_utensil_id', 'integer', ['limit' => 11, 'signed' => false, 'null' => true, 'after' => 'utensil_id'])
            ->addForeignKey('recipe_utensil_id', 'recipe_utensils', 'recipe_utensil_id', ['update' =>' CASCADE', 'delete' => 'SET_NULL'])
            ->update();
    }

    public function down()
    {
        $recipe_utensils_table = $this->table('recipe_utensils');
        $recipe_utensils_table
            ->removeColumn('internal_name')
            ->update();

        $recipe_steps_table = $this->table('recipe_steps');
        $recipe_steps_table
            ->dropForeignKey('recipe_utensil_id')
            ->removeColumn('recipe_utensil_id')
            ->update();
    }
}
