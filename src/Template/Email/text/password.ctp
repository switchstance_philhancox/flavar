Hello <?= $user->forename; ?>!

You've just requested password reset code that will allow you to reset the password to your Flavar account. Here is the code you should paste into the app: <?= $code; ?>. Please note the code is valid only for next 24 hours.

If you haven't requested password reset code then please ignore this email.

Kind regards,
Flavar Team