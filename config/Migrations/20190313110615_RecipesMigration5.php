<?php
use Migrations\AbstractMigration;

class RecipesMigration5 extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('recipes');
        $table
            ->removeColumn('ar_model')
            ->update();
    }

    public function down()
    {
        $table = $this->table('recipes');
        $table
            ->addColumn('ar_model', 'string', ['limit' => 255, 'null' => true, 'after' => 'description'])
            ->update();
    }
}
